<div class="parent">
	<div class="doubleFaceSliderWrapper desktop-only">
		<h1 class="doubleFaceSliderTitle">OUT AND ABOUT</h1>
		<div class="arrow rightArrow"></div>
		<div class="arrow leftArrow"></div>
		<div class="doubleFaceSlider royalSlider desktop-only rsMinW">
			<div class="rsContent">
				<div class="content-wrapper">
					<h2 class="slide-title">ZEITUNA BAY</h2>
					<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, facere.Lorem ipsum dolor</p>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
				</div>
				<div class="imgWrapperContainer">					
					<div class="imgWrapper">
						<img class="rsImg" src="images/_carousel.jpg">
					</div>
				</div>
			</div>
			<div class="rsContent">
				<div class="content-wrapper">
					<h2 class="slide-title">ZEITUNA BAY</h2>
					<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, sunt?</p>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
				</div>
				<div class="imgWrapperContainer">					
					<div class="imgWrapper">
						<img class="rsImg" src="images/_beirut.jpg">
					</div>
				</div>
			</div>
			<div class="rsContent">
				<div class="content-wrapper">
					<h2 class="slide-title">ZEITUNA BAY</h2>
					<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, minima.</p>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
				</div>
				<div class="imgWrapperContainer">					
					<div class="imgWrapper">
						<img class="rsImg" src="images/_out-of-hotel.jpg">
					</div>
				</div>
			</div>

			<div class="rsContent">
				<div class="content-wrapper">
					<h2 class="slide-title">ZEITUNA BAY</h2>
					<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, delectus!</p>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
				</div>
				<div class="imgWrapperContainer">					
					<div class="imgWrapper">
						<img class="rsImg" src="images/_beirut.jpg">
					</div>
				</div>
			</div>

			<div class="rsContent">
				<div class="content-wrapper">
					<h2 class="slide-title">ZEITUNA BAY</h2>
					<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, magni!</p>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
				</div>
				<div class="imgWrapperContainer">					
					<div class="imgWrapper">
						<img class="rsImg" src="images/_out-of-hotel.jpg">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="doubleFaceSliderWrapper mobile-only">
		<h1 class="doubleFaceSliderTitle">OUT AND ABOUT</h1>
		<div class="arrow rightArrow"></div>
		<div class="arrow leftArrow"></div>
		<div class="doubleFaceSlider royalSlider mobile-only rsMinW">
			<div class="rsContent">
				<h2 class="slide-title">ZEITUNA BAY</h2>
				<div class="mobileImgWrapper">
					<a class="rsImg" href="images/_carousel.jpg"></a>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
				</div>
				<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, facere.</p>
			</div>
			<div class="rsContent">
				<h2 class="slide-title">ZEITUNA BAY</h2>
				<div class="mobileImgWrapper">
					<a class="rsImg" href="images/_beirut.jpg"></a>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
				</div>
				<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, sunt?</p>
			</div>
			<div class="rsContent">
				<h2 class="slide-title">ZEITUNA BAY</h2>
				<div class="mobileImgWrapper">
					<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
				</div>
				<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, minima.</p>
			</div>
			<div class="rsContent">
				<h2 class="slide-title">ZEITUNA BAY</h2>
				<div class="mobileImgWrapper">
					<a class="rsImg" href="images/_beirut.jpg"></a>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
				</div>
				<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, delectus!</p>
			</div>
			<div class="rsContent">
				<h2 class="slide-title">ZEITUNA BAY</h2>
				<div class="mobileImgWrapper">
					<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
					<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
				</div>
				<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, magni!</p>
			</div>
		</div>
	</div>
</div>