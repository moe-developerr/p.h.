<nav id="ws-nav">
	<a href="tel:+9611369100" class="phone">PHONE: +961-1-369100</a>
	<a href="mailto:info@phoeniciabeirut.com" class="email">EMAIL: info@phoeniciabeirut.com</a>
	
	<div class="cross-wrapper" id="nav-close-btn">
		<div class="cross"></div>
	</div>

	<ul class="ws-nav-list clear-fix">
		<li class="ws-nav__list-item">
			<a href="#">THE HOTEL</a>
		</li>
		<li class="drop-down-btn ws-nav__list-item _activeLink">
			<div>ROOMS &AMP; SUITES</div>
			<ul class="drop-down">
				<li class="_activeLink"><a class="drop-down__anchor" href="#">ALL</a></li>
				<li><a class="drop-down__anchor" href="#">ROOMS</a></li>
				<li><a class="drop-down__anchor" href="#">SUITES</a></li>
				<li><a class="drop-down__anchor" href="#">PHOENICIA CLUB</a></li>
				<li><a class="drop-down__anchor" href="#">PHOENICIA RESIDENCE</a></li>
			</ul>
		</li>
		<li class="drop-down-btn ws-nav__list-item">
			<div>DINING</div>
			<ul class="drop-down">
				<li><a class="drop-down__anchor" href="#">RESTAURANTS &amp; BARS</a></li>
				<li><a class="drop-down__anchor" href="#">THE COLLECTION</a></li>
				<li><a class="drop-down__anchor" href="#">PHOENICIA GOURMET</a></li>
				<li><a class="drop-down__anchor" href="#">PRIVATE DINING</a></li>
			</ul>
		</li>
		<li class="drop-down-btn ws-nav__list-item">
			<div>SPA &AMP; WELLNESS</div>
			<ul class="drop-down">
				<li><a class="drop-down__anchor" href="#">SPA</a></li>
				<li><a class="drop-down__anchor" href="#">WELLNESS</a></li>
				<li><a class="drop-down__anchor" href="#">FITNESS</a></li>
			</ul>
		</li>
		<li class="drop-down-btn ws-nav__list-item">
			<div>MEETINGS &AMP; EVENTS</div>
			<ul class="drop-down">
				<li><a class="drop-down__anchor" href="#">MEETING &amp; CONFERENCES</a></li>
				<li><a class="drop-down__anchor" href="#">WEDDINGS</a></li>
				<li><a class="drop-down__anchor" href="#">SOCIAL EVENTS</a></li>
				<li><a class="drop-down__anchor" href="#">CAPACITY / CHARTS</a></li>
				<li><a class="drop-down__anchor" href="#">RFP</a></li>
			</ul>
		</li>
		<li>
			<div>ART AT PHOENICIA</div>
		</li>
		<li class="drop-down-btn ws-nav__list-item">
			<a href="#">GALLERY</a>
			<ul class="drop-down">
				<li><a class="drop-down__anchor" href="#">PHOTOS</a></li>
				<li><a class="drop-down__anchor" href="#">VIDEOS</a></li>
			</ul>
		</li>
		<li class="ws-nav__list-item">
			<a href="#">YOUNG PHOENICIANS</a>
		</li>
		<li class="ws-nav__list-item">
			<a href="#">OFFERS</a>
		</li>
		<li class="mobile-only ws-nav__list-item">
			<a href="#">NEWS</a>
		</li>
		<li class="mobile-only ws-nav__list-item">
			<a href="#">BOUTIQUES</a>
		</li>
		<li class="mobile-only ws-nav__list-item">
			<a href="#">CAREERS</a>
		</li>
		<li class="mobile-only ws-nav__list-item">
			<a href="#">PRIVACY</a>
		</li>
		<li class="mobile-only ws-nav__list-item">
			<a href="#">SITE MAP</a>
		</li>
	</ul>

	<div class="social-icons clear-fix">
		<a href="#" class="fa fa-linkedin"></a>
		<a href="#" class="fa fa-youtube"></a>
		<a href="#" class="fa fa-instagram"></a>
		<a href="#" class="fa fa-pinterest"></a>
		<a href="#" class="fa fa-google-plus"></a>
		<a href="#" class="fa fa-facebook"></a>
		<a href="#" class="fa fa-twitter"></a>
	</div>
</nav>
<div class="background"></div>
<script>
var desktopNavModule = (function (jswindow) {
	// cache DOM
	var $wsNav 		= $('#ws-nav');
	var $dropDown   = $('.drop-down');
	var $activeLink = $wsNav.find('.drop-down-btn.activeLink');

	var showCurrentPageModule = (function () {		
		function showCurrentPage() {
			$activeLink.mouseover().children('div').click();
		}

		return {
			showCurrentPage: showCurrentPage
		}
	})();

	var navDropDownsModule = (function () {
		// cache DOM
		var $thisDropDown 			   			 = undefined;
		var $thisDropDownListItem 	   			 = undefined;
		var $theseDropDownListItems	   			 = undefined;
		var $thisDropDownParentLi	   			 = undefined;
		var totalWidthOfTheseListItems 			 = 0;
		var thisDropDownWidth 		   			 = 0;
		var parentLiLeftPadding 	   			 = '';
		var parentLiLeftOffset 		   			 = 0;
		var partOfUlInsideWindow       			 = 0;
		var partOfUlOutsideWindow      			 = 0;
		var currentWindowWidth		   			 = 0;
		var twoPointFivePercentCurrentWindowWidth = 0;


		function repositionDropDowns() {
			if(jswindow.innerWidth >= 1000) {
				// set ul visibility hidden and display block
				$dropDown.css({visibility: 'hidden', display: 'block'});
				
				// 1- check each drop down
				$dropDown.each(_positionDropDowns);
		
				// set ul visibility and display to empty to keep the css hover active
				$dropDown.css({visibility: '', display: ''});
			}
		}

		function _positionDropDowns() {
			$thisDropDown 		   	   = $(this);
			$theseDropDownListItems	   = $thisDropDown.children('li');
			$thisDropDownParentLi  	   = $thisDropDown.parent('li');
			totalWidthOfTheseListItems = 0;
			
			// 2- get the parent li left padding
			parentLiLeftPadding = '15px';

			/* 
				3- increase the size of the drop down in order to give
				   space for list items with space seperated words (IMPORTANT!)
			*/
			$thisDropDown.width(5000);

			// 4- then loop over each li of this ul and give it a padding
			$theseDropDownListItems.each(function () {
				$thisDropDownListItem = $(this);
				$thisDropDownListItem.css('padding', '0 ' + parentLiLeftPadding)

				// 5- calculate the total width of this/these li(s)
				totalWidthOfTheseListItems += $thisDropDownListItem.innerWidth();
			});
			// 6- set this width to the drop down
			$thisDropDown.width(totalWidthOfTheseListItems + 1);
			// 7- get the width of this drop down
			thisDropDownWidth     = $thisDropDown.width();
			// 8- get the current window width
			currentWindowWidth    = jswindow.innerWidth;
			// 9- get twoPointFivePercentCurrentWindowWidth
			twoPointFivePercentCurrentWindowWidth = currentWindowWidth * 0.025;
			// 10- get the left offset of the parent li
			parentLiLeftOffset    = $thisDropDownParentLi.offset().left;
			// 11- subtract the window width from the left offset
			partOfUlInsideWindow  = currentWindowWidth - parentLiLeftOffset;
			// 12- subtract the drop down width from the partOfUlInsideWindow
			partOfUlOutsideWindow = thisDropDownWidth  - partOfUlInsideWindow;
			// 13- if the calculated value is positive then shift the ul in the negative direction by this calculated amount
			if(partOfUlOutsideWindow > 0) $thisDropDown.css('left', -partOfUlOutsideWindow - twoPointFivePercentCurrentWindowWidth);
		}

		return {
			repositionDropDowns: repositionDropDowns
		}
	})();

	var navListItemsModule = (function (timer) {
		// cache DOM
		var $wsNavList 					 	  = $wsNav.children('.ws-nav-list');
		var $wsNavListItems 			 	  = $wsNavList.children('li:not(.mobile-only)');
		var $background 					  = $('.background');
		var isResizingFromNonDesktopToDesktop = 0;
		var isResizingFromDesktopToNonDesktop = 0;
		var executeOnceOver1100 		  	  = 0;
		var executeOnceBetween1000And1099 	  = 0;		
		var navListWidth 				  	  = 0;
		var windowWidth					  	  = 0;
		
		// check listeners
		checkListeners();
		
		// init
		WebFont.load({
					    custom: {
					      families: ['Gotham']
					    },
					    active: function() {	
						 						repositionNavListItems();
						 						navDropDownsModule.repositionDropDowns();
												$wsNav.animate({opacity: 0.95}, 500, function () {
													modifyNavOpacity();
													showCurrentPageModule.showCurrentPage();
												});
											}
					});

		function _attachListeners() {
			$wsNav.on('mouseenter', '.drop-down-btn', _showDropDown)
					.on('mouseleave', '.drop-down-btn', _hideDropDown);
		}

		function _detachListeners() {
			$wsNav.off('mouseenter mouseleave');
		}

		function _showDropDown() {
			$(this).children('.drop-down').stop().fadeIn(300);
			$background.stop().show(0).animate({"height":1, "opacity":0},0).animate({"height":45, "opacity":0.95},200);
		}

		function _hideDropDown() {
			$dropDown.stop().fadeOut(300);
			$background.stop().animate({"height":1, "opacity":0},200);
			$activeLink.removeClass('activeLink');
		}

		function modifyNavOpacity() {
			if(jswindow.innerWidth >= 1000) $wsNav.css('opacity', 0.95);
			else $wsNav.css('opacity', 1);
		}

		function checkListeners() {
			if(jswindow.innerWidth >= 1000) {
				isResizingFromNonDesktopToDesktop++;
				if(isResizingFromNonDesktopToDesktop === 1) {
					_attachListeners();
					isResizingFromDesktopToNonDesktop = 0;
				}
			} else {
				isResizingFromDesktopToNonDesktop++;
				if(isResizingFromDesktopToNonDesktop === 1) {
					_detachListeners();
					isResizingFromNonDesktopToDesktop = 0;
				}
			}
		}

		function repositionNavListItems() {
			if(jswindow.innerWidth >= 1000) {
				_getNavListWidthOnceOnDesktopViewport();
				var navWidth 			   = _getNavWidth();
				var ulWidth  			   = navListWidth;
				var paddingForEachListItem = _getPaddingForEachListItem(navWidth, ulWidth);

				paddingForEachListItem = _removeDiscripency(paddingForEachListItem);

				_setPaddingForListItems(paddingForEachListItem);
			}
		}

		function _getNavWidth() {
			return $wsNav.width();
		}

		function _getNavListWidthOnceOnDesktopViewport(parameter) {
			windowWidth = jswindow.innerWidth;

			if(windowWidth >= 1100) {
				executeOnceOver1100++;
				
				if(executeOnceOver1100 <= 2) {
					executeOnceBetween1000And1099 = 0;
					_stripPaddingFromListItems();
					navListWidth = $wsNavList.width();
				}
			} else if(windowWidth >= 1000 && windowWidth <= 1099) {
				executeOnceBetween1000And1099++;

				if(executeOnceBetween1000And1099 <= 2) {
					executeOnceOver1100 = 0;
					_stripPaddingFromListItems();
					navListWidth = $wsNavList.width();
				}
			}
		}

		function _getPaddingForEachListItem(navWidth, ulWidth) {
			return (navWidth - ulWidth) / $wsNavListItems.length;
		}

		function _removeDiscripency(paddingForEachListItem) {
			return Math.floor((paddingForEachListItem*10)/10) - 0.1;
		}

		function _setPaddingForListItems(paddingForEachListItem) {
			$wsNavListItems.css('padding', '0 ' + paddingForEachListItem/2 + 'px');
		}

		function _stripPaddingFromListItems() {
			$wsNavListItems.each(function () { $(this).css('padding', 0); });
		}

		return {
			repositionNavListItems: repositionNavListItems, // this is used in windowResizeListenersModule
			checkListeners: checkListeners,
			modifyNavOpacity: modifyNavOpacity
		}
	})(undefined);

	return {
		repositionDropDowns: navDropDownsModule.repositionDropDowns,
		repositionNavListItems: navListItemsModule.repositionNavListItems,
		checkNavListItemsListeners: navListItemsModule.checkListeners,
		modifyNavOpacity: navListItemsModule.modifyNavOpacity
	}
})(window);
</script>