<header id="ws-header">
	<div id="three-bars-wrapper">
		<div id="three-bars"></div>
	</div>
</header>
<div id="header-components">
	<!-- header logo -->
	<div id="header-logo">
		<a href="#">
			<img src="images/header-logo.png" alt="Phoenicia logo">
		</a>
	</div>

	<!-- contact details -->
	<div id="contact-details">
		<div class="contact-details-email"><a href="mailto:info@phoeniciabeirut.com" class="mailto">EMAIL: info@phoeniciabeirut.com</a></div>
		<div class="contact-details-phone">PHONE:+961-1-369100</div>
		<div class="contact-details-address">ADDRESS: Minet El Hosn, Beirut, Lebanon, PO Box 11/846</div>
		<a href="#" class="map">MAP</a>
		<span class="close-btn" id="contact-details-close-btn"></span>
	</div>

	<!-- contact details overlay NOTE: DON'T CHANGE THE POSITION OF THIS ELEMENT IN HTML DUE TO Z-INDEX PRECEDENCE -->
	<div class="contactDetailsOverlay"></div>

	<!-- contact us -->
	<div id="contact-us">
		<a href="#" class="anchor">CONTACT US</a>
	</div>
	
	<!-- book now desktop -->
	<div class="book-now-desktop-wrapper">
		<a href="#" class="anchor book-now-desktop">BOOK AT PHOENICIA</a>
		<span class="close-btn" id="book-now-close-btn"></span>
	</div>
	
	<!-- book now mobile -->
	<div id="book-now-mobile">
		BOOK <span class="mobile-410">AT PHOENICIA</span>
	</div>
	
	<!-- calendar -->
	<div id="calendar">
		<form action="" method="post" id="calendar-form">
			<fieldset class="check-dates">
				<legend class="calendar__legend calendar__legend--margin-bottom">YOUR STAY DATES</legend>
				<div class="arrival clear-fix">
					<label>ARRIVAL</label>
					<input type="text" class="date-field arrivalDate" readonly="true">
					<button type="button" class="calendar-icon"><i class="fa fa-calendar"></i></button>
				</div>
				<div class="departure clear-fix">
					<label>DEPARTURE</label>
					<input type="text" class="date-field departureDate" readonly="true">
					<button type="button" class="calendar-icon"><i class="fa fa-calendar"></i></button>
				</div>
			</fieldset>
			<fieldset class="reservation">
				<legend class="calendar__legend calendar__legend--margin">ROOMS &amp; GUESTS</legend>
				<div class="clear-fix">
					<label>ROOMS</label>
					<label>ADULTS</label>
					<label>CHILDREN</label>	
				</div>
				<div class="reservation-count clear-fix">
					<div class="counter-container">
						<input type="text" name="" class="reservation-count__input-field" id="rooms" value="1">
						<div class="up-arrow"></div>
						<div class="down-arrow"></div>
					</div>
					<div class="counter-container">
						<input type="text" name="" class="reservation-count__input-field" id="adults" value="1">
						<div class="up-arrow"></div>
						<div class="down-arrow"></div>
					</div>
					<div class="counter-container">
						<input type="text" name="" class="reservation-count__input-field" id="children" value="1">
						<div class="up-arrow"></div>
						<div class="down-arrow"></div>
					</div>				
				</div>
			</fieldset>
			<input class="calendar__submit-btn" type="submit" value="CHECK AVAILABILITY">
		</form>
	</div>
</div>