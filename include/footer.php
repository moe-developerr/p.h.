<footer id="ws-footer" class="clear-fix">
	<a href="#" class="social-wall-btn">PHOENICIA SOCIAL WALL</a>
	<span class="social-icons clear-fix">
		<span class="ws-footer__stay-connected mobile-415">STAY CONNECTED</span>
		<a href="#" class="fa fa-linkedin"></a>
		<a href="#" class="fa fa-youtube"></a>
		<a href="#" class="fa fa-instagram"></a>
		<a href="#" class="fa fa-pinterest"></a>
		<a href="#" class="fa fa-google-plus"></a>
		<a href="#" class="fa fa-facebook"></a>
		<a href="#" class="fa fa-twitter"></a>
	</span>
	
	<div class="scroll-down scroll-down-arrow">
		SCROLL DOWN
	</div>
	
	<ul class="footer-list desktop-only">
		<li><a href="#" class="footer-list-link">BEIRUT</a></li>
		<li><a href="#" class="footer-list-link">NEWS</a></li>
		<li><a href="#" class="footer-list-link">BOUTIQUES</a></li>
		<li><a href="#" class="footer-list-link">CAREERS</a></li>
		<li><a href="#" class="footer-list-link">PRIVACY</a></li>
		<li><a href="#" class="footer-list-link">SITE MAP</a></li>
	</ul>
	<ul class="footer-list2 desktop-only">
		<li><a href="#" class="footer-list2-link">AMBASSADOR</a></li>
		<li><a href="#" class="footer-list2-link">IHG Rewards Club</a></li>
		<li><a href="#" class="footer-list2-link">AR</a></li>
	</ul>
	
	
	<div class="clear-fix">
		<a href="#" class="footer-links mobile-only">AMBASSADOR</a>
		<a href="#" class="footer-links mobile-only">IHG Rewards Club</a>
		<a href="http://InterContinental.com" class="intercontinental-link">InterContinental.com</a>
		<a href="http://InterContinental.com" class="footer-links mobile-only">AR</a>
	</div>
	
	<div class="footer-bottom clear-fix">
		<div id="footer-logo">
			<a href="#">
				<img src="images/footer-logo.png" alt="Phoenicia logo">
			</a>
		</div>
	
		<div class="copyrights">&copy;2015 Phoenicia Hotel. All rights reserved.</div>
	</div>
	
	<a href="http://InterContinental.com" class="desktop-int-link">InterContinental.com</a>
</footer>
<script src="js/vendor/datetimepicker.full.min.js"></script>
<script src="js/modularMain.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">