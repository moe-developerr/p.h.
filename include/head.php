<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<title>Phoenicia Homepage</title>

<!-- Stylesheets -->
<link rel="stylesheet" href="css/vendor/reset.min.css">
<link rel="stylesheet" href="css/vendor/datetimepicker.css">
<link rel="stylesheet" href="css/typo.css">
<link rel="stylesheet" href="css/common.css">
<!-- <script src="js/vendor/webfontloader.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
<script src="js/vendor/jquery-1.11.3.min.js"></script>
<script>
	if(navigator.userAgent.indexOf('MSIE 8.0') !== -1) location.replace('http://localhost/phoenicia/root/not-supported.php');
</script>