<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<link rel="stylesheet" href="css/pages-content/beirut.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
	
		<main class="MW1200 clear-fix">
			<h1 id="page-title">BEIRUT</h1>
			
			<div class="slider-wrapper">
				<div class="arrow rightArrow"></div>
				<div class="arrow leftArrow"></div>
				<div class="imageSlider rsMinW">
					<div class="slide" data-area="oceana">
						<a href="images/_accomodation.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, inventore.</div>
					</div>
					<div class="slide" data-area="sioufi gardens">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, eaque.</div>
					</div>
					<div class="slide" data-area="national museum">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, iste!</div>
					</div>
					<div class="slide" data-area="verdun">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="hamra">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="sanayeh gardens">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="st george cathedral of beirut">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="phoenicia">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="mohamad el-amine mosque">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="robert mouawad museum">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="beirut souks">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="jeita grotto">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="edde sands">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="byblos">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
					<div class="slide" data-area="cedars">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
				</div>
			</div>

			<ul class="dots-line">
				<li style="left: 5%;"  class="dot" data-area="oceana"></li>
				<li style="left: 10%;" class="dot" data-area="sioufi gardens"></li>
				<li style="left: 20%;" class="dot" data-area="national museum"></li>
				<li style="left: 28%;" class="dot" data-area="verdun"></li>
				<li style="left: 32%;" class="dot" data-area="hamra"></li>
				<li style="left: 37%;" class="dot medium-dot" data-area="sanayeh gardens"></li>
				<li style="left: 40%;" class="dot" data-area="st george cathedral of beirut"></li>
				<li style="left: 45%;" class="dot big-dot" data-area="phoenicia"></li>
				<li style="left: 53%;" class="dot" data-area="mohamad el-amine mosque"></li>
				<li style="left: 57%;" class="dot" data-area="robert mouawad museum"></li>
				<li style="left: 63%;" class="dot" data-area="beirut souks"></li>
				<li style="left: 75%;" class="dot" data-area="jeita grotto"></li>
				<li style="left: 80%;" class="dot" data-area="edde sands"></li>
				<li style="left: 90%;" class="dot" data-area="byblos"></li>
				<li style="left: 95%;" class="dot" data-area="cedars"></li>
			</ul>

			<ul class="area-names">
				<li style="left: 5%;" class="dot-name"  data-area="oceana"> Oceana</li>
				<li style="left: 10%;" class="dot-name"  data-area="sioufi gardens">Sioufi gardens</li>
				<li style="left: 20%;" class="dot-name"  data-area="national museum">National museum</li>
				<li style="left: 28%;" class="dot-name"  data-area="verdun">Verdun</li>
				<li style="left: 32%;" class="dot-name"  data-area="hamra">Hamra</li>
				<li style="left: 37%;" class="dot-name"  data-area="sanayeh gardens">Sanayeh gardens</li>
				<li style="left: 40%;" class="dot-name"  data-area="st george cathedral of beirut">St george cathedral of beirut</li>
				<li style="left: 45%;" class="big-dot dot-name"  data-area="phoenicia">Phoenicia</li>
				<li style="left: 53%;" class="dot-name"  data-area="mohamad el-amine mosque">Mohamad el-amine mosque</li>
				<li style="left: 57%;" class="dot-name"  data-area="robert mouawad museum">Robert mouawad museum</li>
				<li style="left: 63%;" class="dot-name"  data-area="beirut souks">Beirut souks</li>
				<li style="left: 75%;" class="dot-name"  data-area="jeita grotto">Jeita grotto</li>
				<li style="left: 80%;" class="dot-name"  data-area="edde sands">Edde sands</li>
				<li style="left: 90%;" class="dot-name"  data-area="byblos">Byblos</li>
				<li style="left: 95%;" class="dot-name"  data-area="cedars">Cedars</li>
			</ul>
				

			<ul class="areas">
				<li class="area" data-area="oceana">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Oceana</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="sioufi gardens">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Sioufi Gardens</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="national museum">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">National Museum</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="verdun">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Verdun</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="hamra">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Hamra</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="sanayeh gardens">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Sanayeh Gardens</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="phoenicia">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Phoenicia</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="st george cathedral of beirut">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">St George Cathedral of Beirut</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="mohamad el-amine mosque">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Mohamad El-Amine Mosque</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="robert mouawad museum">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Robert Mouawad Museum</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="beirut souks">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Beirut Souks</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="jeita grotto">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Jeita Grotto</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="edde sands">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Edde Sands</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="byblos">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Byblos</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
				<li class="area" data-area="cedars">
					<div class="time-taken-to-reach">
						<time>00:15</time> minutes by car
					</div>
					<h2 class="area-name">Cedars</h2>
					<p class="area-description">This pedestrian area guarantees a unique shopping experience bringing you the trendiest brands.</p>
				</li>
			</ul>

			<section class="beirut">
				
				<div class="weather-time clear-fix">
					<h2 class="beirutCity">NOW IN BEIRUT</h2>
					<time class="beirutTime"><span id="dynamicClock">03:15 pm</span> <span class="smaller">+2 GMT</span></time>
					<span class="beirutWeather">
						<img src="images/weather.png" class="cloud">
						<div class="degrees"> 27 °<sup>C</sup></div>
						<span class="smaller">Sunny - Beach time</span>
					</span>
				</div>

				<ul class="beirut">
					<li class="table">
						<ul class="tr">
							<li class="beirut-category">
								<h3 class="beirut-category-title">TIME:</h3>
								<p class="beirut-category-text">Lebanese time is G.M.T. +2 hours in winter (October to March) and +3 hours in summer (April to September) when daylight saving time is observed.</p>
							</li>
							<li class="beirut-category">
								<h3 class="beirut-category-title">POPULATION:</h3>
								<p class="beirut-category-text">As per figures collected in 2009, the population of Lebanon is approximately 4 million people.</p>
							</li>
						</ul>
					</li>
					<li class="table">
						<ul class="tr">
							<li class="beirut-category">
								<h3 class="beirut-category-title">AREA:</h3>
								<p class="beirut-category-text">Lebanon is a small country of only 10,452 sq km; from north to south it extends 217 km and from east to west it spans 80 km at its widest point.</p>
							</li>
							<li class="beirut-category">
								<h3 class="beirut-category-title">LOCAL WEATHER:</h3>
								<p class="beirut-category-text">Lebanon has a Meditteranean climate with mild Winters and long warm Summers.</p>
							</li>
						</ul>
					</li>
					<li class="table">
						<ul class="tr">
							<li class="beirut-category">
								<h3 class="beirut-category-title">LANGUAGE:</h3>
								<p class="beirut-category-text">Arabic is the official language of Lebanon. French is the second commonly used language, however English is widely spoken.</p>
							</li>
							<li class="beirut-category">
								<h3 class="beirut-category-title">CURRENCY:</h3>
								<p class="beirut-category-text">The official Lebanese currency is the Lebanese pound or lira (LL) although U.S. dollars are used widely throughout the country. The US$/LL exchange rate is relatively stable at US$1=LL1,500.</p>
							</li>
						</ul>
					</li>
				</ul>
			</section>
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>

	<script src="js/vendor/jquery.royalslider.min.js"></script>
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script>
		var dotsModule = (function ($window, timer) {
			// cache DOM
			var $sliderWrapper 		= $('.slider-wrapper');
			var $imageSlider   		= $sliderWrapper.children('.imageSlider');
			var $slides  		    = $imageSlider.children('.slide');
			var $thisArrow          = undefined;
			var imageSliderInstance = undefined;
			var $dotsLine 	  	   = $('.dots-line');
			var $dots 	  	  	   = $dotsLine.children('.dot');
			var $areaNames         = $('.area-names');
			var $dotNames          = $areaNames.children('.dot-name');
			var $area              = $('.area');
			var numberOfDots  	   = $dots.length;
			var numberOfDotsShown  = 0;
			var numberOfNamesShown = 0;

			// attach listeners
			_attachListeners();

			// init
			_init();

			function _attachListeners() {
				$window.on('load', _showDotsSequentially);
				$sliderWrapper.on('click', '.arrow', _activateArrows);
				$dotsLine.on('click', '.dot', _showSelectedArea)
						 .on('mouseenter', '.dot:not(.big-dot)', _showAreaName)
						 .on('mouseleave', '.dot:not(.big-dot)', _hideAreaName);
			}

			function _init() {
				imageSliderInstance = $imageSlider.royalSlider({
					autoScaleSlider: true,
					numImagesToPreload: 8,
					imageScaleMode: 'fill',
					loop: true,
					controlNavigation: 'none',
					keyboardNavEnabled: true,
					transitionSpeed: 1000
				}).data('royalSlider');
				imageSliderInstance.ev.on('rsAfterSlideChange', _changeDotAndText);
			}

			function _changeDotAndText() {
				var slideAreaName = imageSliderInstance.slidesJQ[imageSliderInstance.currSlideId].children('.slide').data('area');
				_showSelectedArea(slideAreaName);
			}

			function _activateArrows() {
				$thisArrow = $(this);

				if($thisArrow.hasClass('leftArrow')) imageSliderInstance.prev();
				else imageSliderInstance.next();
			}

			function _showAreaName() {
				$dotNames.filter('[data-area="' + $(this).data('area') + '"]').show(0);
			}

			function _hideAreaName() {
				$dotNames.filter('[data-area="' + $(this).data('area') + '"]').hide(0);
			}

			function _showInitialSlideAndArea() {
				var areaToShow = $dots.filter('.medium-dot').data('area');

				$area.filter('[data-area="' + areaToShow + '"]').fadeIn(800);
				_changeSlide(areaToShow);
			}

			function _showSelectedArea(AreaName) {
				var dotClicked = $(this); // dot clicked is an array (array of dots or array holding window object (if moved by hand))
				var isSliderDraggedByHand = (dotClicked[0] === window) ? true : false;
				var dotToHighlight = $dots.filter('[data-area="' + AreaName + '"]');

				// if slider is moved by hand, take the area name from me
				// else if you pressed on the dot, take the area name from the dot itself
				var areaClicked = isSliderDraggedByHand ? AreaName : dotClicked.data('area');
				
				// show area text
				$area.stop().hide(0).filter('[data-area="' + areaClicked + '"]').fadeIn(800);

				_changeDotSize(isSliderDraggedByHand ? dotToHighlight : dotClicked);
				_changeSlide(areaClicked);
			}

			function _changeSlide(areaClicked) {
				imageSliderInstance.goTo($slides.index($('[data-area="' + areaClicked + '"]')));
			}

			function _changeDotSize(dotClicked) {
				dotClicked.addClass('medium-dot').siblings().removeClass('medium-dot');
			}

			function _showDotsSequentially() {
				timer = setInterval(_showDot, 300);
			}

			function _showDot() {
				if(numberOfDotsShown < numberOfDots) {
					$dots.eq(numberOfDotsShown++).fadeIn(800);
					$dotNames.eq(numberOfNamesShown).animate({opacity: 1}, 300);
					$dotNames.eq(numberOfNamesShown++).hide(0);
				} else {
					clearInterval(timer);
					_showInitialSlideAndArea();
					$dotNames.filter('.big-dot').fadeIn(800);
				}
			}
		})($(window));
		
	</script>
</body>
</html>