<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<link rel="stylesheet" href="css/vendor/flickity.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<link rel="stylesheet" href="css/pages-content/accomodation-inside.css">
</head>
<body>

	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">
		<main class="MW1200 center-content clear-fix">
			<h1 id="page-title">PRESEDENTIAL SUITE</h1>
			<div class="accomodation-inside">

				<div class="slider-wrapper">
					<div class="arrow rightArrow"></div>
					<div class="arrow leftArrow"></div>
					<div class="imageSlider rsMinW">
						<div class="slide">
							<a href="images/_accomodation.jpg" class="rsImg"></a>
							<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, inventore.</div>
						</div>
						<div class="slide">
							<a href="images/_beirut.jpg" class="rsImg"></a>
							<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, eaque.</div>
						</div>
						<div class="slide">
							<a href="images/_beirut.jpg" class="rsImg"></a>
							<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, iste!</div>
						</div>
						<div class="slide">
							<a href="images/_beirut.jpg" class="rsImg"></a>
							<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
						</div>
					</div>
				</div>
				
				<div class="content-container clear-fix">
					<div class="social-icons-inner clear-fix">
						<a href="" class="fa fa-google-plus"></a>
						<a href="" class="fa fa-linkedin"></a>
						<a href="" class="fa fa-facebook"></a>
						<a href="" class="fa fa-twitter"></a>
						<span>SHARE ON</span>
					</div>
					
					<h1 class="content-container__heading">AMENITIES &amp; SERVICES</h1>
					
					<ul class="content-container__list1 with-arrows">
						<li class="content-container__list1__row">
							Assorted fresh fruit basket and sweets in all rooms and suites. Two bottles of mineral water in all rooms
						</li>
						<li class="content-container__list1__row">
							Complimentary daily local newspaper Iron board and iron available in all rooms and suites
						</li>
						<li class="content-container__list1__row">
							LCD Screens with VDA system in all rooms and suites Full Breakfast Buffet is available in Mosaic Restaurant
						</li>
						<li class="content-container__list1__row">
							IP Telephony Access to SPA Phoenicia for paid available treatments
						</li>
						<li class="content-container__list1__row">
							Round the clock access to the Hotel’s fitness room
						</li>
						<li class="content-container__list1__row">
							Complimentary access to indoor and outdoor pools
						</li>
						<li class="content-container__list1__row">
							Complimentary scheduled shuttle transfer to Beirut International Airport Complimentary shuttle transfer to Downtown Beirut
						</li>
						<li class="content-container__list1__row">
							Complimentary shuttle transfer to ABC department store
						</li>
						<li class="content-container__list1__row">
							Complimentary WIFI in public areas
						</li>
						<li class="content-container__list1__row">
							Complimentary WIFI in the room
						</li>
						<li class="content-container__list1__row">
							Meet and Assist service inside the arrival area, after the General Security, is available at the additional charge of US per guest
						</li>
					</ul>

					<ul class="content-container__list2">
						<li class="content-container__list2__row">
							<div class="content-container__list2__title">SIZE</div>
							<div class="content-container__list2__text">50sqm/538sqft</div>
						</li>
						<li class="content-container__list2__row">
							<div class="content-container__list2__title">BEDDING</div>
							<div class="content-container__list2__text">
								King or Queen bed
								Down comforters and pillows
								Custom imported linens
							</div>
						</li>
						<li class="content-container__list2__row">
							<div class="content-container__list2__title">FEATURES</div>
							<div class="content-container__list2__text">
								Separate living room
								Extensive wardrobe space
							</div>
						</li>
						<li class="content-container__list2__row">
							<div class="content-container__list2__title">BATH</div>
							<div class="content-container__list2__text">
								Bathrooms with enclosed shower
								Custom-made bathroom amenities
							</div>
						</li>
						<li class="content-container__list2__row">
							<div class="content-container__list2__title">TECH</div>
							<div class="content-container__list2__text">
								46" flat screen HDTVs with full 
								cable access, high definition channels 
								and movies on-demand
							</div>
						</li>
					</ul>
				</div>

				<div class="recommended-rooms clear-fix">
					<h1 class="recommended-rooms__title">MORE OPTIONS</h1>
					<div class="moreOptions js-flickity" data-flickity-options='{"lazyLoad": 2, "cellAlign": "center", "pageDots": false, "wrapAround": true, "arrowShape": {"x0": 10, "x1": 40, "y1": 50, "x2": 45, "y2": 50, "x3": 15} }'>
						<div class="slides">
							<a href="#" class="room">
								<img src="images/_accomodation.jpg" alt="room">
								<h4 class="room-type">REGULAR ROOM</h4>
								<div class="clear-fix room-details">
									<div class="feature-title">SIZE</div>
									<div class="feature-details">40sqm/430sqft</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">BEDDING</div>
									<div class="feature-details">King bed Pillows imported linens</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">FEATURES</div>
									<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
								</div>
							</a>
						</div>
						<div class="slides">
							<a href="#" class="room">
								<img src="images/_accomodation.jpg" alt="room">
								<h4 class="room-type">REGULAR ROOM</h4>
								<div class="clear-fix room-details">
									<div class="feature-title">SIZE</div>
									<div class="feature-details">40sqm/430sqft</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">BEDDING</div>
									<div class="feature-details">King bed Pillows imported linens</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">FEATURES</div>
									<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
								</div>
							</a>
						</div>
						<div class="slides">
							<a href="#" class="room">
								<img src="images/_accomodation.jpg" alt="room">
								<h4 class="room-type">REGULAR ROOM</h4>
								<div class="clear-fix room-details">
									<div class="feature-title">SIZE</div>
									<div class="feature-details">40sqm/430sqft</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">BEDDING</div>
									<div class="feature-details">King bed Pillows imported linens</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">FEATURES</div>
									<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
								</div>
							</a>
						</div>
						<div class="slides">
							<a href="#" class="room">
								<img src="images/_accomodation.jpg" alt="room">
								<h4 class="room-type">REGULAR ROOM</h4>
								<div class="clear-fix room-details">
									<div class="feature-title">SIZE</div>
									<div class="feature-details">40sqm/430sqft</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">BEDDING</div>
									<div class="feature-details">King bed Pillows imported linens</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">FEATURES</div>
									<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
								</div>
							</a>
						</div>
						<div class="slides">
							<a href="#" class="room">
								<img src="images/_accomodation.jpg" alt="room">
								<h4 class="room-type">REGULAR ROOM</h4>
								<div class="clear-fix room-details">
									<div class="feature-title">SIZE</div>
									<div class="feature-details">40sqm/430sqft</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">BEDDING</div>
									<div class="feature-details">King bed Pillows imported linens</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">FEATURES</div>
									<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
								</div>
							</a>
						</div>
						<div class="slides">
							<a href="#" class="room">
								<img src="images/_accomodation.jpg" alt="room">
								<h4 class="room-type">REGULAR ROOM</h4>
								<div class="clear-fix room-details">
									<div class="feature-title">SIZE</div>
									<div class="feature-details">40sqm/430sqft</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">BEDDING</div>
									<div class="feature-details">King bed Pillows imported linens</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">FEATURES</div>
									<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
								</div>
							</a>
						</div>
						<div class="slides">
							<a href="#" class="room">
								<img src="images/_accomodation.jpg" alt="room">
								<h4 class="room-type">REGULAR ROOM</h4>
								<div class="clear-fix room-details">
									<div class="feature-title">SIZE</div>
									<div class="feature-details">40sqm/430sqft</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">BEDDING</div>
									<div class="feature-details">King bed Pillows imported linens</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">FEATURES</div>
									<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
								</div>
							</a>
						</div>
						<div class="slides">
							<a href="#" class="room">
								<img src="images/_accomodation.jpg" alt="room">
								<h4 class="room-type">REGULAR ROOM</h4>
								<div class="clear-fix room-details">
									<div class="feature-title">SIZE</div>
									<div class="feature-details">40sqm/430sqft</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">BEDDING</div>
									<div class="feature-details">King bed Pillows imported linens</div>
								</div>
								<div class="clear-fix room-details">
									<div class="feature-title">FEATURES</div>
									<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
								</div>
							</a>
						</div>
					</div>
				</div>	<!-- Recommended Rooms -->
			</div>
		</main>
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
	
	<script src="js/vendor/flickity.pkgd.min.js"></script>
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script src="js/vendor/jquery.royalslider.min.js"></script>
	<script>
		var imageSliderModule = (function () {
			// cache DOM
			var $sliderWrapper 		= $('.slider-wrapper');
			var $imageSlider   		= $sliderWrapper.children('.imageSlider');
			var $thisArrow          = undefined;
			var imageSliderInstance = undefined;

			// attach listeners
			_attachListeners();

			// init
			_init();

			function _attachListeners() {
				$sliderWrapper.on('click', '.arrow', _activateArrows);
			}

			function _init() {
				imageSliderInstance = $imageSlider.royalSlider({
					autoScaleSlider: true,
					imageScaleMode: 'fill',
					loop: true,
					controlNavigation: 'none',
					keyboardNavEnabled: true,
					transitionSpeed: 1000,
					autoplay: {
						enabled: true,
						pauseOnHover: true,
						stopAtAction: false,
						delay: 3000
					}
				}).data('royalSlider');
			}

			function _activateArrows() {
				$thisArrow = $(this);

				if($thisArrow.hasClass('leftArrow')) imageSliderInstance.prev();
				else imageSliderInstance.next();
			}
		})();
	</script>
</body>
</html>