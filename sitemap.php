<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/sitemap.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>
	
	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">
		
		<main class="MW1200 clear-fix">

			<h1 id="page-title">SITE MAP</h1>
			
			<div class="table">
				<div class="tr">
					<div class="tc mainLink"><a href="#">HOME</a></div>
					<div class="tc"></div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">THE HOTEL</a></div>
					<div class="tc"></div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">ROOMS &amp; SUITES</a></div>
					<div class="tc">
						<ul>
							<li><a href="#" class="subLink">All</a></li>
							<li><a href="#" class="subLink">Rooms</a></li>
							<li><a href="#" class="subLink">Suites</a></li>
							<li><a href="#" class="subLink">Phoenicia Club</a></li>
							<li><a href="#" class="subLink">Phoenicia Residence</a></li>
						</ul>
					</div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">DINING</a></div>
					<div class="tc">
						<ul>
							<li><a href="#" class="subLink">Restaurants &amp; Bars</a></li>
							<li><a href="#" class="subLink">Phoenicia Gourmet</a></li>
							<li><a href="#" class="subLink">Private Dining</a></li>
						</ul>
					</div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">SPA &amp; WELLNESS</a></div>
					<div class="tc">
						<ul>
							<li><a href="#" class="subLink">SPA</a></li>
							<li><a href="#" class="subLink">Wellness</a></li>
							<li><a href="#" class="subLink">Kids Club</a></li>
						</ul>
					</div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">MEETINGS &amp; EVENTS</a></div>
					<div class="tc">
						<ul>
							<li><a href="#" class="subLink">Social Events</a></li>
							<li><a href="#" class="subLink">Weddings</a></li>
							<li><a href="#" class="subLink">Meeting &amp; Conference</a></li>
							<li><a href="#" class="subLink">Capacity / Charts</a></li>
							<li><a href="#" class="subLink">RFP</a></li>
						</ul>
					</div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">ARTS &amp; Collections</a></div>
					<div class="tc">
						<ul>
							<li><a href="#" class="subLink">Art at Phoenicia</a></li>
							<li><a href="#" class="subLink">Coganc at Phoenicia</a></li>
							<li><a href="#" class="subLink">Whisky at Phoenicia</a></li>
							<li><a href="#" class="subLink">Wine at Phoenicia</a></li>
						</ul>
					</div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">GALLERY</a></div>
					<div class="tc">
						<ul>
							<li><a href="#" class="subLink">Photos</a></li>
							<li><a href="#" class="subLink">Videos</a></li>
							<li><a href="#" class="subLink">Virtual Tours</a></li>
						</ul>
					</div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">OFFERS</a></div>
					<div class="tc"></div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">BEIRUT</a></div>
					<div class="tc"></div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">NEWS</a></div>
					<div class="tc"></div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">BOUTIQUES</a></div>
					<div class="tc"></div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">CAREERS</a></div>
					<div class="tc"></div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">PRIVACY</a></div>
					<div class="tc"></div>
				</div>
				<div class="tr">
					<div class="tc mainLink"><a href="#">SITE MAP</a></div>
					<div class="tc"></div>
				</div>
			</div>

		</main>

	</div>	<!-- Ws Wrapper -->
	
	<?php include 'include/footer.php'; ?>
	
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>