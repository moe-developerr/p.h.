<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/accomodation.css">
</head>
<body>

	<div class="opacity-layer"></div>

	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>
	
	<div id="ws-wrapper">
		<main class="MW1200 center-content clear-fix">
			<h1 id="page-title">ROOMS</h1>
			<div class="inline-block">
				<section class="rooms inline-block clear-fix">
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
				</section>

				<section class="rooms inline-block clear-fix">
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">DOUBLE ROOM</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR SUITE</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
				</section>

				<section class="suites inline-block clear-fix">
					<h1 class="section-title">SUITES</h1>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR ROOM</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR ROOM</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR ROOM</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
					<a href="#" class="room bgpink">
						<div class="room__img"></div>
						<h2 class="room-type">REGULAR ROOM</h2>
						<div class="clear-fix room-details">
							<div class="feature-title">SIZE</div>
							<div class="feature-details">40sqm/430sqft</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">BEDDING</div>
							<div class="feature-details">King bed Pillows imported linens</div>
						</div>
						<div class="clear-fix room-details">
							<div class="feature-title">FEATURES</div>
							<div class="feature-details">Bathrooms with enclosed rainforest shower</div>
						</div>
					</a>
				</section>
				
				<section class="others inline-block clear-fix">
					<a href="#" class="others__link">
						<figure>
							<img src="images/_accomodation2.jpg" alt="">
							<figcaption>SUITES</figcaption>
						</figure>
					</a>
					<a href="#" class="others__link">
						<figure>
							<img src="images/_accomodation2.jpg" alt="">
							<figcaption>SUITES</figcaption>
						</figure>
					</a>
					<a href="#" class="others__link">
						<figure>
							<img src="images/_accomodation2.jpg" alt="">
							<figcaption>SUITES</figcaption>
						</figure>
					</a>
				</section>
				
				<section class="others inline-block clear-fix">
					<a href="#" class="others__link">
						<figure>
							<img src="images/_accomodation2.jpg" alt="">
							<figcaption>SUITES</figcaption>
						</figure>
					</a>
					<a href="#" class="others__link">
						<figure>
							<img src="images/_accomodation2.jpg" alt="">
							<figcaption>SUITES</figcaption>
						</figure>
					</a>
					<a href="#" class="others__link">
						<figure>
							<img src="images/_accomodation2.jpg" alt="">
							<figcaption>SUITES</figcaption>
						</figure>
					</a>
				</section>
				
			</div>
		</main>
	</div> <!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>

	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script>
		
	</script>
</body>
</html>