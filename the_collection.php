<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/collection.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
	
		<main class="MW1200 clear-fix center-content">
			<h1 id="page-title">THE COLLECTION</h1>
			
			<p class="page-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae numquam commodi nisi laudantium. Repudiandae sunt ut corrupti vitae consequuntur eum atque necessitatibus expedita provident nulla, eveniet nisi odit? Et, necessitatibus?</p>
			
			<ul class="collections inline-block">
				<li class="collection">
					<a href="#">
						<h2 class="collection-title">COGNAC <br> AT PHOENICIA</h2>
						<img src="images/_collection.jpg" alt="">
						<div class="layer"></div>
						<div class="layer-text">The Phoenicia collection boasts well over 50 bottles of the world’s rarest and oldest Cognacs and Armagnacs. The following highlight a few samples of our exclusive selection.</div>
					</a>
				</li>
				<li class="collection second">
					<a href="#">
						<h2 class="collection-title">WINE <br> AT PHOENICIA</h2>
						<img src="images/_collection.jpg" alt="">
						<div class="layer"></div>
						<div class="layer-text">The Phoenicia collection boasts well over 50 bottles of the world’s rarest and oldest Cognacs and Armagnacs. The following highlight a few samples of our exclusive selection.</div>
					</a>
				</li>
				<li class="collection">
					<a href="#">
						<h2 class="collection-title">WHISKY <br> AT PHOENICIA</h2>
						<img src="images/_collection.jpg" alt="">
						<div class="layer"></div>
						<div class="layer-text">The Phoenicia collection boasts well over 50 bottles of the world’s rarest and oldest Cognacs and Armagnacs. The following highlight a few samples of our exclusive selection.</div>
					</a>
				</li>
			</ul>
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>

	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>