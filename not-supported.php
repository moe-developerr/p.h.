<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Phoenicia Beirut - Browser Not Supported</title>
	<link rel="stylesheet" href="css/vendor/reset.min.css">
	<link rel="stylesheet" href="css/typo.css">

    <? 
    	$_base = $_SERVER["HTTP_HOST"]. $_SERVER["PHP_SELF"]; 
		$_base = explode("/", $_base);
		$base="http:/";
		for ($i=0; $i<(count($_base)-1); $i++) {
			$base = $base."/".$_base[$i];
		}
		$base = $base."/";
		$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	?>
<base href="<?=$base?>">

	<style>
		div {
			width: 50%;
			margin: 0 auto;
			text-align: center;
			margin-top: 50px;
		}
		h1,
		a {
			color: #49176D;
		}
		h1 {
			text-transform: uppercase;
			font-weight: 600;			
			margin-bottom: 50px;
			margin-top: 50px;
			font-size: 27px;
			font-size: 2.7rem;
		}
		p,
		a {
			font-size: 15px;
			font-size: 1.5rem;
		}
		p {
			margin-bottom: 20px;
		}
		a {
			text-decoration:none;
		}
		a:hover {
			text-decoration: underline;
		}
	</style>
</head>
<body>
	<div>
		<img src="images/header-logo.png" alt="Browser Not Supported">
		<h1>YOUR BROWSER IS NOT SUPPORTED</h1>
		<p>
			Phoenicia Beirut supports <a href="https://www.google.com/intl/en/chrome/browser/desktop/index.html">Chrome</a>, <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a>, <a href="http://www.apple.com/safari/">Safari</a>, <a href="www.opera.com/download">Opera</a>, and <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">Internet Explorer 9</a> and above.
		</p>
		<p>Please upgrade your browser to open Phoenicia Beirut Website. Thank You.</p>
	</div>
</body>
</html>