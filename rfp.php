<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/rfp.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>
	
	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
		
		<main class="clear-fix">

			<div class="rfp center-content">
				<h1 id="page-title">REQUEST FOR PROPOSAL</h1>
			
					<div id="page-img">
						<div class="desktop-only whiteColor">
							<br>
							<br>
							<h1 class="whiteColor">UNWIND AND REJUVENATE</h1>
							<br>
							<br>
							<br>
							<br>
							<br>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, fugit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, asperiores.</p>
							<p>Sit amet, consectetur adipisicing elit. Inventore, autem?Dolor velit distinctio saepe fugiat adipisci accusamus officiis hic quas.</p>
							<br>
							<br>
							<br>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, fugit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, asperiores.</p>
							<p>Sit amet, consectetur adipisicing elit. Inventore, autem?Dolor velit distinctio saepe fugiat adipisci accusamus officiis hic quas.</p>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<p>Opens everyday from 7:00 am to 10:00 pm</p>
							<p>Mezzaine Level, <a href="tel:009611357030" class="boldHoverText whiteColor">+961 (0)1 357030</a></p>
						</div>
					</div>
				
				<div class="table">
					<form class="desktop-row" action="" method="POST" id="rfp-form">
						<div class="row-cell clear-fix">
							<h2 class="leftAligned">CONTACT DETAILS</h2>
							<div class="select-wrapper-parent name-prefix clear-fix">
								<div class="select-wrapper clear-fix">
									<select name="honorific" id="honorific" class="required" placeholder="honorific">
										<option value="Mr" selected>Mr.</option>
										<option value="Ms">Ms.</option>
										<option value="Miss">Miss</option>
										<option value="Mrs">Mrs.</option>
									</select>
								</div>
							</div>
							<input type="text" name="firstName" id="first-name" placeholder="First Name" class="required">
							<input type="text" name="lastName" id="last-name" placeholder="Last Name" class="required">
							<input type="text" name="address" id="address" placeholder="Address" class="required">
							<input type="tel" name="phone" id="phone" placeholder="Phone">
							<input type="tel" name="mobile" id="mobile" placeholder="Mobile">
							<input type="email" name="email" id="email" placeholder="Email">
						</div>
						<div class="spacing"></div>
						<div class="row-cell clear-fix">
							<h2 class="leftAligned">EVENT DETAILS</h2>
							<div class="select-wrapper-parent clear-fix">
								<div class="select-wrapper" class="required" placeholder="Event Type">
									<select name="eventType" id="event-type">
										<option value="" selected disabled>Choose your event type</option>
										<option value="">Silver</option>
										<option value="">Gold</option>
									</select>
								</div>
							</div>
							<div class="event-date-wrapper">
								<input type="text" name="eventDate" id="event-date" placeholder="Event Date" class="required">
								<i class="fa fa-calendar"></i>
							</div>
							<div class="event-date-wrapper">
								<input type="text" name="alternateEventDate" id="alternate-event-date" placeholder="Alternative Date If Available">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" name="estimateAttendees" id="estimated-attendees" placeholder="Estimated Number of Attendees">
							<div id="guest-rooms" placeholder="Guest Rooms">
								Are Guest Rooms Required
								<input type="radio" name="guest-rooms" id="yes" value="Yes"><label for="yes">Yes</label>
								<input type="radio" name="guest-rooms" id="no" value="No"><label for="no">No</label>
							</div>
						</div>
						<div class="spacing"></div>
						<div class="row-cell clear-fix">
							<h2 class="leftAligned">ADDITIONAL DETAILS</h2>
							<textarea name="notes" id="notes" cols="30" rows="10" placeholder="Notes"></textarea>
							<div id="reply">
								Reply Reference
								<input type="radio" name="reply" id="reply-email"><label for="reply-email">Email</label>
								<input type="radio" name="reply" id="reply-mobile"><label for="reply-mobile">Mobile</label>
								<input type="radio" name="reply" id="reply-phone"><label for="reply-phone">Phone</label>
							</div>
						</div>
					</form>
				</div>
				<button class="submit-button">SUBMIT</button>
			</div> <!-- RFP -->

		</main>

	</div>
	
	<?php include 'include/footer.php'; ?>

	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script>
		var isPlaceHolderSupportedModule = (function () {
			// cache DOM
			var placeholder = '';

			// attach listeners
			_attachListeners();

			//init
			_fixPlaceholdersInThisPage();

			function _attachListeners() {
				if(!_isPlaceholderSupported()) {
					$('.rfp-form').on('click', 'input textarea', _removePlaceholder)
								  .on('blur', 'input textarea', _addPlaceholder);	
				}
			}
			
			function _isPlaceholderSupported() {
				return 'placeholder' in document.createElement('input');
			}

			function _fixPlaceholdersInThisPage() {
				if(!_isPlaceholderSupported()) {
					$('input, textarea').each(_extractPlaceholderValueAndInsertItBack)
				}
			}

			function _extractPlaceholderValueAndInsertItBack() {
				var $thisField = $(this);
				var placeholderValue = $thisField.attr('placeholder');
				$thisField.val(placeholderValue);
			}

			function _removePlaceholder() {
				var $thisField = $(this);
				
				placeholder = $thisField.val();
				$thisField.val('');
			}

			function _addPlaceholder() {
				var $thisField = $(this);
				var $thisFieldValue = $thisField.val();

				if($thisFieldValue === '' || $thisFieldValue === placeholder) $thisField.val(placeholder);
			}
		})();
		!function eventDates() {
			var inputFieldNumber = 0;

			$('#event-date, #alternate-event-date').each(function () {
				$(this).datetimepicker({
					timepicker: false,
					format:'d/m/Y',
					minDate: 0, // you can't select dates prior today
					value: ((inputFieldNumber === 0) ? '0' : '+1970/01/02') // if first input field set its date to today, else to tomorrow
				});
				inputFieldNumber++;
			});
		}();

		!function validateForm() {
			var $guestRooms 		= $('input[name=guest-rooms]'),
				$submitButton 		= $('.submit-button'),
				$phone 				= $('#phone'),
				$rfpForm 			= $('#rfp-form');

				$submitButton.on('click', function () {
					$('.required').each(function () {
						var $thisRequiredField = $(this);
						if($thisRequiredField.val() === '') {
							$submitButton.text('Fill In ' + $thisRequiredField.attr('placeholder'));
							return false;
						} else if(!$guestRooms.filter(':checked').val()) {
							$submitButton.text('Fill In Guest Rooms');
							return false;
						} else if(!$phone.val().match(/^[0-9]+$/)) {
							$submitButton.text('Only Numbers Allowed In Phone Number');
							return false;
						} else {
							$submitButton.text('SUBMIT');
						}
					});

					if($submitButton.text() === 'SUBMIT') {
						$.ajax({
							url: $rfpForm.attr('action'),
							data: $rfpForm.serialize(),
							success: function () {
								$submitButton.text('Your form is submitted');
							}
						});
					}
				});
		}();
	</script>
</body>
</html>