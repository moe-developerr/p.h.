<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<link rel="stylesheet" href="css/pages-content/directions.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
	
		<main class="MW1200 clear-fix">
			<h1 id="page-title">DIRECTIONS</h1>
			<div id="map"></div>
			<script>
				var googleMapModule = (function () {
					// init
					google.maps.event.addDomListener(window, 'load', _initializeGoogleMap);

					function _initializeGoogleMap() {
						var mapCanvas = document.getElementById("map");
						var myLatLng = {lat: 33.901139, lng: 35.492890};
						var map = new google.maps.Map(mapCanvas, {
						    zoom: 13,
							scrollwheel: true,
							disableDefaultUI: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP,
						    center: myLatLng
						});

				  		var marker = new google.maps.Marker({
				   			position: myLatLng,
				   			map: map,
				    		icon: "images/logoPin.png",
				      		title:"PHOENICIA BEIRUT"
				  		});
					}
				})();
			</script>
			<div class="content-container clear-fix">
				<div class="social-icons-inner clear-fix">
					<a href="#" class="fa fa-google-plus"></a>
					<a href="#" class="fa fa-linkedin"></a>
					<a href="#" class="fa fa-facebook"></a>
					<a href="#" class="fa fa-twitter"></a>
					<span>SHARE ON</span>
				</div>
				<h1 class="content-container__heading">DIRECTION</h1>
				<div class="direction-instructions">
					<p>From the airport, take the highway towards the city centre.</p>
					<p>Upon arriving to downtown, make a left towards Martyrs' Square.</p>
					<br>
					<br>
					<p>At the end of the road, turn left onto Weygand Street.</p>
					<p>Pass Al Nahar building on the right. Continue straight to the end of the street and make a right towards the sea.</p>
					<br>
					<br>
					<p>Keep going till end of the way, St Georges to the right side, HSBC to the left, then take the first left just by the Palm Beach Hotel.</p>
					<br>
					<br>
					<p>Again, once out of the exit, access the security check point and Phoenicia Hotel is to the right side.</p>
				</div>

				<div class="address-details">
					<p>Address: Minet El Hosn, Beirut, Lebanon,</p>
					<p>PO Box 11/846</p>
					<a href="tel:009611369110">Tel: +961 (0)1 369110</a>
					<br>
					<a href="tel:009611361615">Fax: +961 (0)1 361615</a>
					<br>
					<br>
					<br>
					<p>Beirut Rafic Hariri International Airport (BEY)</p>
					<p>Travel time 20 minutes</p>
					<p>Distance 10 kilometers</p>
					<p>6.3 miles</p>
				</div>
				
			</div>
	
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
	
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>