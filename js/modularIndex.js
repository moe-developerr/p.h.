var IndexPageModule = (function ($window, jswindow, mySwiper) {
	// cache DOM
	var $contactUs 				 = $('#contact-us');

	var headerModule = (function (scrollTimer) {
		// cache DOM
		var $calendar 	    	     	      = $('#calendar');
		var $nav      	    	     	      = $('#ws-nav');
		var $header   	    	     	      = $('#ws-header');
		var $contactDetails 	     	      = $('#contact-details');
		var $contactDetailsAddress   	      = $contactDetails.children('.contact-details-address');
		var $map 				     	      = $contactDetails.children('.map');
		var $contactDetailsCloseBtn  	      = $contactDetails.children('#contact-details-close-btn');
		var $logo 				     	      = $('#header-logo');
		var $background						  = $('.background');
		var $bookNowWrapper		     	      = $('.book-now-desktop-wrapper');
		var $swiperContainer 				  = $('.swiper-container');
		var $bookNow   			     	      = $bookNowWrapper.children('.book-now-desktop');
		var $contactDetailsOverlay  		  = $contactDetails.next('.contactDetailsOverlay');
		var headerStatus	         	      = 'maximized';
		var isResizingFromNonDesktopToDesktop = 0;
		var isResizingFromDesktopToTablet     = 0;
		var isResizingFromTabletToMobile      = 0;
		var $activeSlide 		    	      = {};

		// attach listeners
		_attachScrollEventIfSwiperNotSupported();

		function _attachScrollEventIfSwiperNotSupported() {
			if(!(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener)) {
				$window.on('scroll', _runAfterScrollFallback);
			}
		}

		function toggleHeaderIfSwiperNotSupported() {
			//This is used in case the window is resized from non desktop to desktop
			if(!(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener) && jswindow.innerWidth >= 1000) {
				if($window.scrollTop() !== 0) _minimizeHeader();
				else if($window.scrollTop() === 0) _maximizeHeader();
			}
		}

		function _runAfterScrollFallback() {
			clearTimeout(scrollTimer);

			scrollTimer = setTimeout(function () {
				toggleHeaderSize();
			}, 100);
		}

		function toggleHeaderSize() {
			if(jswindow.innerWidth >= 1000) {
				if(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener) {
					$activeSlide = $('.swiper-slide.swiper-slide-active');

					//if section is not 0 and header is maximized, minimize header
					if($activeSlide.data('slide-number') != 0 && headerStatus === 'maximized') _minimizeHeader();
					
					else if($activeSlide.data('slide-number') == 0 && headerStatus === 'minimized') _maximizeHeader();

				} else {
					if($window.scrollTop() !== 0 && headerStatus === 'maximized') _minimizeHeader();

					else if($window.scrollTop() === 0 && headerStatus === 'minimized') _maximizeHeader();
				}
			}
		}

		function _minimizeHeader() {
			_initDesktopHeaderDimensions(45, {top: '5.8px', width: '160px', height: '33.402px'}, {height: '35px'}, {height: '35px', top: 5, paddingTop: 12.5, '-webkit-transform': 'translateY(0)','-ms-transform': 'translateY(0)',transform: 'translateY(0)'}, {top: 45}, {top: 40}, {height: '35px', top: 5, '-webkit-transform': 'translateY(0)','-ms-transform': 'translateY(0)',transform: 'translateY(0)'}, {top: 5, '-webkit-transform': 'translateY(0)','-ms-transform': 'translateY(0)',transform: 'translateY(0)'}, {float: 'left', clear: 'none'}, {top: 7}, {top: 6}, 90, 90, {height: 45});
			headerStatus = 'minimized';
		}

		function _maximizeHeader() {
			_initDesktopHeaderDimensions(75, {top: '14.5px', width: '220px', height: '45.9278px'}, {height: '35px'}, {height: '56px', top: 37, paddingTop: 10, '-webkit-transform': 'translateY(-50%)','-ms-transform': 'translateY(-50%)',transform: 'translateY(-50%)'}, {top: 75}, {top: 55}, {height: '35px', top: 37, '-webkit-transform': 'translateY(-50%)','-ms-transform': 'translateY(-50%)',transform: 'translateY(-50%)'}, {top: 37, '-webkit-transform': 'translateY(-50%)','-ms-transform': 'translateY(-50%)',transform: 'translateY(-50%)'}, {float: 'none', clear: 'left'}, {top: 18}, {top: 17}, 75, 120, {height: 75});
			headerStatus = 'maximized';
		}

		function _initDesktopHeaderDimensions(headerHeight, logoDimensions, bookNowHeight, contactDetailsCSS, NavPosition, calendarTopPosition, contactUsCSS, bookNowWrapperCSS, contactDetailsAddressFloat, contactDetailsCloseBtnTopPosition, mapTopPosition, swiperContainerPaddingTop, backgroundTopPosition, contactDetailsOverlayHeight) {
        	$header.height(headerHeight);
        	$logo.css(logoDimensions);
        	$bookNow.css(bookNowHeight);
        	$contactDetails.css(contactDetailsCSS);
        	$nav.css(NavPosition);
        	$calendar.css(calendarTopPosition);
			$contactUs.css(contactUsCSS);
			$bookNowWrapper.css(bookNowWrapperCSS);
			$contactDetailsAddress.css(contactDetailsAddressFloat);
			$contactDetailsCloseBtn.css(contactDetailsCloseBtnTopPosition);
			$map.css(mapTopPosition);
			$swiperContainer.css('padding-top', swiperContainerPaddingTop);
			$background.css('top', backgroundTopPosition)
			$contactDetailsOverlay.css(contactDetailsOverlayHeight);
    	}

    	function _initNonDesktopHeader() {
    		if(jswindow.innerWidth >= 768) {
				_initTabletHeader();
    		} else {
    			_initMobileHeader();
    		}
    	}

    	function _initTabletHeader() { // run once on resize
    		isResizingFromDesktopToTablet++;
			isResizingFromTabletToMobile = 0;
			if(isResizingFromDesktopToTablet === 1) {
				_initNonDesktopHeaderStyle(
					{width: 180, height: 37.5773, top: 26},
					{top: 0},
					76,
					0,
					{'padding-top': 76}
				);
			}
    	}

    	function _initMobileHeader() { // run once on resize
    		isResizingFromDesktopToTablet = 0;
			isResizingFromTabletToMobile++;
			if(isResizingFromTabletToMobile === 1) {
				_initNonDesktopHeaderStyle(
					{width: 160, height: 33.402, top: 26},
					{top: 0},
					76,
					0,
					{'padding-top': 76}
				);
			}
    	}

    	function _initNonDesktopHeaderStyle($logoDimensions, $navTopPosition, $headerHeight, $contactUsHideTime, $swiperContainerPaddingTop) {
    		$logo.css($logoDimensions);
			$nav.css($navTopPosition);
			$header.height($headerHeight);
			$contactUs.hide($contactUsHideTime);
			$swiperContainer.css($swiperContainerPaddingTop);
    	}

    	function _initDesktopHeader() { // run once on resize
    		isResizingFromNonDesktopToDesktop++;
    		if(!(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener)) {
    			if(isResizingFromNonDesktopToDesktop === 1 && $window.scrollTop() === 0) {
    				_maximizeHeader();
    			}
    		}
    		else {
    			if(isResizingFromNonDesktopToDesktop === 1 && $('.swiper-slide.swiper-slide-active').data('slide-number') == 0) {
    				_maximizeHeader();
	    		}	
    		}
    	}

    	function fixHeaderSize() {
    		if(jswindow.innerWidth >= 1000) {
    			isResizingFromDesktopToTablet = isResizingFromTabletToMobile = 0;
    			_initDesktopHeader();
    		} else {
    			isResizingFromNonDesktopToDesktop = 0;
    			_initNonDesktopHeader();
    		}
    	}

    	return {
    		toggleHeaderSize: toggleHeaderSize,
    		fixHeaderSize: fixHeaderSize,
    		toggleHeaderIfSwiperNotSupported: toggleHeaderIfSwiperNotSupported
    	}
	})(undefined);

	var swiperModule = (function () {
		// cache DOM
		var $swiperWrapper = $('.swiper-wrapper');
		var $scrollDown    = $('.scroll-down');
		
		// init on window load

		function loadEitherSwiperOrRelyOnNativeScrollbar() {
			if(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener) {
				$.getScript('https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.2.5/js/swiper.min.js', function () {
					checkSwiperStatus();
				});
			}
		}


		function _initSwiper() {
			if(!mySwiper) {
				mySwiper = new Swiper ('.swiper-container', {
							 	direction: 'vertical',
				    		    mousewheelControl: true,	        
				    		    scrollbar: '.swiper-scrollbar',
				    		    scrollbarHide: false,
				    		    scrollbarDraggable: true,
				    		    scrollbarSnapOnRelease: true,
				    		    keyboardControl: true,
				    		    speed: 300,
				    		    // disables swiping
				    		    onlyExternal: true,
				    		    onInit: function (swiper) {
									desktopNavModule.repositionNavListItems();
						 			desktopNavModule.repositionDropDowns();
				    		    },
				    		    onTransitionStart: function (mySwiper) {
				    		    	headerModule.toggleHeaderSize();
				    		    	_toggleScrollDownBtn();
				    		    },
				    		    onReachBeginning: function (mySwiper) {
				    		    	$scrollDown.show(0);
				    		    },
				    		    onReachEnd: function (mySwiper) {
				    		    	$scrollDown.hide(0);
				    		    }
							});
				/*******************************************************************************
				*	1. Wrapper is displayed as block first and then turned into flex box 
				*	2. Why? In order to prevent slides stacking horizontally on document ready
				*******************************************************************************/
				$swiperWrapper.css({display: '-webkit-box',
								    display: '-moz-box',
								    display: '-ms-flexbox',
								    display: '-webkit-flex',
								    display: 'flex'
		    					   });
			}
		}

		function _destroySwiper() {
			// if loaded from desktop and resized
			if(mySwiper) mySwiper = mySwiper.destroy(true, true); // run once on mobile if swiper initiated
		}

		function checkSwiperStatus() {
			if(jswindow.innerWidth >= 1000)
				_initSwiper();
			else
				_destroySwiper();
		}

		function _toggleScrollDownBtn() {
			$activeSwiperSlide = $('.swiper-slide.swiper-slide-active');
			if($activeSwiperSlide.data('slide-number') === 0 && !$scrollDown.is(':visible')) $scrollDown.show(0);
			else if($activeSwiperSlide.data('slide-number') !== 0 && $scrollDown.is(':visible')) $scrollDown.hide(0);
		}

		return {
			checkSwiperStatus: checkSwiperStatus,
			loadEitherSwiperOrRelyOnNativeScrollbar: loadEitherSwiperOrRelyOnNativeScrollbar
		};
	})();

	var googleMapModule = (function () {
		// init
		google.maps.event.addDomListener(jswindow, 'load', _initializeGoogleMap);

		function _initializeGoogleMap() {
			var mapCanvas = document.getElementsByClassName("google-map")[0];
			var myLatLng = {lat: 33.901139, lng: 35.492890};
			var map = new google.maps.Map(mapCanvas, {
			    zoom: 13,
				scrollwheel: true,
				disableDefaultUI: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			    center: myLatLng
			});

	  		var marker = new google.maps.Marker({
	   			position: myLatLng,
	   			map: map,
	    		icon: "images/logoPin.png",
	      		title:"PHOENICIA BEIRUT"
	  		});
		}
	})();

	var scrollDownBtnModule = (function () {
		// cache DOM
		var $scrollDown 	   = $('.scroll-down');
		var $activeSwiperSlide = undefined;
		var isResizingFromDesktopToNonDesktop = 0;
			
		// init
		_initScrollDownBtn();

		function _initScrollDownBtn() {
			_attachListeners();
			_hideScrollDownBtnIfSwiperNotSupported();
		}

		function _attachListeners() {
			$scrollDown.on('click', _moveToNextSlideAndToggle);
		}

		function _hideScrollDownBtnIfSwiperNotSupported() {
			if(!(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener)) $scrollDown.hide(0);
		}

		function hideScrollDownBtnOnNonDesktop() {
			if(jswindow.innerWidth < 1000) {
				isResizingFromDesktopToNonDesktop++;
				if(isResizingFromDesktopToNonDesktop === 1) {
					$scrollDown.hide(0);
				}
			} else {
				isResizingFromDesktopToNonDesktop = 0;
			}
		}

		function _moveToNextSlideAndToggle() {
			_moveToNextSlide();
			_toggleScrollDownBtn();
		}

		function _toggleScrollDownBtn() {
			$activeSwiperSlide = $('.swiper-slide.swiper-slide-active');

			if($activeSwiperSlide.data('slide-number') === 0 && !$scrollDown.is(':visible')) $scrollDown.show(0);
			else if($activeSwiperSlide.data('slide-number') !== 0 && $scrollDown.is(':visible')) $scrollDown.hide(0);
		}

		function _moveToNextSlide() {
			if(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener) {
				if($('.swiper-slide.swiper-slide-active').data('slide-number') === 0) {
					mySwiper.slideNext();
					$scrollDown.hide(0);
				}
			}
		}

		return {
			hideScrollDownBtnOnNonDesktop: hideScrollDownBtnOnNonDesktop
		}
	})();

	// This module runs on window load
	var addPaddingToSlidesModule = (function () {
		// cache DOM
		var $swiperSlides 			 = $('.swiper-slide').not(':first');
		var $thisParent              = undefined;
		var paddingTop               = 0;
		
		// init on window load

		function addPaddingToSlides() {
			var $thisSlide = undefined;
			_swiperNotSupportedFallbackMarginForLastSlide();

			if(jswindow.innerWidth >= 1000) {
				$swiperSlides.each(function () {
					$thisSlide  = $(this);
					$thisParent = $thisSlide.children('.parent');
					// paddingTop = (jswindow.innerHeight - 160)/2 - $thisParent.height();
					paddingTop = (jswindow.innerHeight - 160 - $thisParent.height())/2	;

					$thisParent.css('padding-top', paddingTop <= 0 ? 0 : paddingTop);
				});
			} else {
				$swiperSlides.each(function () {
					$thisSlide = $(this);
					$thisSlide.children('.parent').css('padding-top', 40);
				});
			}
		}

		function _swiperNotSupportedFallbackMarginForLastSlide() {
			if(!(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener)) {
				if(jswindow.innerWidth >= 1000) $swiperSlides.last().css('padding-bottom', '200px');

				else $swiperSlides.last().css('padding-bottom', '0');
			}	
		}

		return {
			addPaddingToSlides: addPaddingToSlides
		}
	})();

	var tilesModule = (function () {
		// cache DOM
		var $tiles 	    = $('.tile3, .tile5');
		var $firstTile  = $('.tile1');
		var $secondTile = $('.tile2');
		var $tileContainer = $('.tile-container');

		// init
		fixTopMargin();

		function fixTopMargin() {
			$tiles.each(_setTopMarginOfTiles);
		}

		function _setTopMarginOfTiles() {
			var $thisTile = $(this);

			$thisTile.height($firstTile.height() - $secondTile.height() - 0.01 * $tileContainer.width());
		}

		return {
			fixTopMargin: fixTopMargin
		};
	})();

	// This module runs on window load
	var imageToCoverModule = (function () {
		function imageToCover(parentOfImage, childImage) {
		    var $parent = parentOfImage,
		        $child  = childImage;
		    if(jswindow.innerWidth >= 1000) {
		        var parentHeight = $parent.innerHeight(),
		            parentWidth  = $parent.innerWidth(),
		            imageHeight  = $child.innerHeight(),
		            imageWidth   = $child.innerWidth();
		            
		            if(imageWidth/imageHeight >= parentWidth/parentHeight) { //landscape mode 
		                $child.css({height: '100%', width: 'auto'});
		            } else { //portrait mode
		                $child.css({width: '100%', height: 'auto'});
		            }
		        var newImageHeight   = $child.innerHeight(),
		            newImageWidth    = $child.innerWidth(),
		            widthDifference  = (newImageWidth  - parentWidth) / 2,
		            heightDifference = (newImageHeight - parentHeight) / 2;

		            $child.css({marginLeft: -widthDifference + 'px', marginTop:  -heightDifference + 'px'});
		    }
		    else {
		        $child.css({marginLeft: 0 + 'px', marginTop:  0 + 'px', width: '100%'});
		    }
		}

		return {
		    	imageToCover: imageToCover
		    };
	})();

	var calcFallbackModule = (function () {
		// cache DOM
		var $contactUsAndContactDetails = $contactUs.add('#contact-details');
		var $bookNowMobile   			= $('#book-now-mobile');
		var windowWidth      			= 0;

		// init
		initCalcFallback();

		function initCalcFallback() {
			windowWidth = jswindow.innerWidth;

			if(!Modernizr.csscalc) {
				if(windowWidth >= 1000)
				{
					$contactUsAndContactDetails.css('right', windowWidth * 0.025 + 240);
				} 
				else
				{
					$bookNowMobile.css('right', windowWidth * 0.025 + 40);
				}
			}
		}
		
		return {
			initCalcFallback: initCalcFallback
		}
	})();

	var doubleFaceSliderModule = (function () {
		// cache DOM
		var $doubleFaceSliderWrapper 			   = $('.doubleFaceSliderWrapper');
		var $doubleFaceSliderDesktopVersion	       = $doubleFaceSliderWrapper.children('.doubleFaceSlider.desktop-only');
		var $doubleFaceSliderMobileVersion 		   = $doubleFaceSliderWrapper.children('.doubleFaceSlider.mobile-only');
		var $imageWrapper 						   = $('.imgWrapper');
		var $thisImageWrapper                      = undefined;
		var doubleFaceSliderDesktopVersionInstance = undefined;

		// attach listeners
		_attachListeners();

		function _attachListeners() {
			$doubleFaceSliderWrapper.on('click', '.arrow', _gotoAnotherSlide);
		}

		function setTheDoubleFaceSliderDesktopVersionImagesToCover() {
			if(jswindow.innerWidth >= 1000) {
				$imageWrapper.each(function() {
					$thisImageWrapper = $(this);
					imageToCoverModule.imageToCover($thisImageWrapper, $thisImageWrapper.children('img'));
				});
			}

			addPaddingToSlidesModule.addPaddingToSlides();
		}

		function initDoubleFaceSliderDesktopVersion() {
			$doubleFaceSliderDesktopVersion.royalSlider({
			    arrowsNav: false,
			    arrowsNavAutoHide: false,
			    fadeinLoadedSlide: false,
			    controlNavigationSpacing: 0,
			    controlNavigation: 'none',
			    imageScaleMode: 'none',
			    imageAlignCenter:false,
			    loop: true,
			    loopRewind: false,
			    numImagesToPreload: 10,
			    keyboardNavEnabled: true,
			    usePreloader: true
			});
		}

		function initDoubleFaceSlidersMobileVersion() {
			$doubleFaceSliderMobileVersion.royalSlider({
			   	autoHeight: true,
			    arrowsNav: false,
			    arrowsNavAutoHide: false,
			    fadeinLoadedSlide: false,
			    controlNavigationSpacing: 0,
			    controlNavigation: 'none',
			    imageScaleMode: 'none',
			    imageAlignCenter:false,
			    loop: true,
			    loopRewind: false,
			    numImagesToPreload: 1,
			    keyboardNavEnabled: true,
			    usePreloader: true
			});
		}

		function _gotoAnotherSlide() {
			var $arrowClicked = $(this);

			if($arrowClicked.hasClass('leftArrow')) _gotoPreviousSlide($arrowClicked);
			else _gotoNextSlide($arrowClicked);
		}

		function _gotoNextSlide($arrowClicked) {
			$arrowClicked.nextAll('.doubleFaceSlider').data('royalSlider').next();
		}

		function _gotoPreviousSlide($arrowClicked) {
			$arrowClicked.nextAll('.doubleFaceSlider').data('royalSlider').prev();
		}

		return {
			initDoubleFaceSlidersMobileVersion: initDoubleFaceSlidersMobileVersion,
			initDoubleFaceSliderDesktopVersion: initDoubleFaceSliderDesktopVersion,
			setTheDoubleFaceSliderDesktopVersionImagesToCover: setTheDoubleFaceSliderDesktopVersionImagesToCover,
			$doubleFaceSliderDesktopVersion: $doubleFaceSliderDesktopVersion,
			$doubleFaceSliderMobileVersion: $doubleFaceSliderMobileVersion
		};
	})();

	var windowLoadListenersModule = (function () {
		// attach Listeners
		_attachListeners();

		function _attachListeners() {
			$window.on('load', _activeDoubleFaceSliders);
		}

		function _activeDoubleFaceSliders() {
			swiperModule.loadEitherSwiperOrRelyOnNativeScrollbar();
			doubleFaceSliderModule.setTheDoubleFaceSliderDesktopVersionImagesToCover();
			doubleFaceSliderModule.initDoubleFaceSliderDesktopVersion();
			doubleFaceSliderModule.$doubleFaceSliderDesktopVersion.animate({opacity: 1}, 1000);
			doubleFaceSliderModule.initDoubleFaceSlidersMobileVersion();
			doubleFaceSliderModule.$doubleFaceSliderMobileVersion.animate({opacity: 1}, 1000);
			addPaddingToSlidesModule.addPaddingToSlides();
		}
	})();

	var windowResizeModule = (function (timer) {
		// attach listeners
		_attachListeners();

		function _attachListeners() {
			$window.on('resize', _resizeListeners);
		}

		function _resizeListeners() {
			_runAfterResize();
		}

		function _runAfterResize() {
			clearTimeout(timer);
			timer = setTimeout(_runAfterResizeListeners, 100);
		}

		function _runAfterResizeListeners() {
			if(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener) {
				swiperModule.checkSwiperStatus();
			} else {
				headerModule.toggleHeaderIfSwiperNotSupported();
			}
			headerModule.fixHeaderSize();
			scrollDownBtnModule.hideScrollDownBtnOnNonDesktop();
			tilesModule.fixTopMargin();
			calcFallbackModule.initCalcFallback();
			doubleFaceSliderModule.setTheDoubleFaceSliderDesktopVersionImagesToCover();
		}
	})(undefined);
})($(window), window, undefined);