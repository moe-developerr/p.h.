/*************************
*
*	1. Book now desktop
*	2. Book now mobile
*	3. contact us
*	4. calendar counter 
*	5. sub calendar (mini calendar)
*	6. mobile nav open close
*	7. mobile nav sub menu
*	8. mobile nav strip sub menu styles
*	9. opacity layer and z-indexing
*	10. window resize event
*
*************************/
var commonCodeForAllPagesModule = (function ($window, jswindow) {
	// cache DOM
	var $calendar 	    		= $('#calendar');
	var $nav      	    		= $('#ws-nav');
	var $opacityLayer   		= $('.opacity-layer');
	var $contactUs 	    		= $('#contact-us');
	var $bookNow 	    		= $('.book-now-desktop');
	var $contactDetails 		= $('#contact-details');
	var $contactDetailsOverlay  = $contactDetails.next('.contactDetailsOverlay'); // Complicated for nothing
	var $contactUsGroup         = [$contactUs, $contactDetails, $contactDetailsOverlay];

	// 1. Book now desktop
	var bookNowOnDesktopModule = (function () {
		// cache DOM
		var $bookNowCloseBtn = $bookNow.next('#book-now-close-btn');
		
		// attach listeners
		_attachListeners();

		function _attachListeners() {
			_attachBookNowHoverListener();
			$bookNow.on('click', showCalendarAndAddBookNowAndOpacityLayerStyles);
			$bookNowCloseBtn.on('click', hideCalendarAndRemoveBookNowAndOpacityLayerStyles);
		}

		function _attachBookNowHoverListener() {
			$bookNow.off('mouseenter mouseleave').hover(_addBookNowHoverStyle, _removeBookNowHoverStyle);
		}

		function _detachBookNowHoverListener() {
			$bookNow.off('mouseenter mouseleave');
		}

		function showCalendarAndAddBookNowAndOpacityLayerStyles(e) {
			$bookNow.addClass('isOpen');
			_detachBookNowHoverListener();
			_addBookNowStyle(e);
			_showCalendar();
			_shiftLeftContactUsAndContactDetails();
			opacityLayerModule.turnOnLayer();
		}

		function hideCalendarAndRemoveBookNowAndOpacityLayerStyles() {
			closeBookNow();
			_shiftRightContactUsAndContactDetails();
			$opacityLayer.css('z-index', -1);			
		}

		function closeBookNow() {
			$bookNow.removeClass('isOpen');
			_attachBookNowHoverListener();
			_removeBookNowStyle();
			_removeBookNowHoverStyle();
			_hideCalendar();
			_closeContactDetailsIfVisible();
		}

		function _closeContactDetailsIfVisible() {
			if($contactDetails.is(':visible')) contactUsModule.show_Contact_Btn_And_Hide_Contact_Details();
		}

		function _removeBookNowStyle() {
			$bookNowCloseBtn.css('transform', 'scale(2)').hide(0);
			$bookNow.removeClass('js-book-now-css');
		}

		function _addBookNowStyle(e) {
			e.preventDefault();
			$bookNow.addClass('js-book-now-css');
			$bookNowCloseBtn.show(0).css({'-webkit-transform': 'scale(1)','-ms-transform': 'scale(1)','transform': 'scale(1)'});
		}

		function _showCalendar() {
			$calendar.delay(400).fadeIn(100);
		}

		function _hideCalendar() {
			$calendar.hide(0);
		}

		function _shiftLeftContactUsAndContactDetails() {
			// calc(2.5% + 240px)
			$.each($contactUsGroup, function () {
				$(this).css('right', 0.025*jswindow.innerWidth + 240);
			});
		}

		function _shiftRightContactUsAndContactDetails() {
			// calc(2.5% + 200px)
			$.each($contactUsGroup, function () {
				$(this).css('right', 0.025*jswindow.innerWidth + 200);
			});
		}

		function _addBookNowHoverStyle() {
			$bookNow.css({backgroundColor: '#49176D', color: '#fff'});
		}

		function _removeBookNowHoverStyle() {
			$bookNow.css({backgroundColor: '#E4DCE9', color: '#49176D'});
		}

		return {
			hideCalendarAndRemoveBookNowAndOpacityLayerStyles: hideCalendarAndRemoveBookNowAndOpacityLayerStyles,
			showCalendarAndAddBookNowAndOpacityLayerStyles: showCalendarAndAddBookNowAndOpacityLayerStyles,
			closeBookNow: closeBookNow
		};
	})();

	// 9. opacity layer and z-indexing
	var opacityLayerModule = (function () {
		// cache DOM
		var isResizingFromDesktopToNonDesktop  = 0;
		var isResizingFromNonDesktopToDesktop  = 0;
		var isLayerTurnedOn 				   = false;
		var wasOnNonDesktopAndResizedToDesktop = false;
		var wasOnDesktopAndResizedToNonDesktop = false;

		// attach listeners
		_attachListeners();

		// init
		fixZIndex();

		function _attachListeners() {
			$opacityLayer.on('click', turnOffLayer);
		}

		function turnOffLayer() {
			$opacityLayer.css('z-index', -1);
			$nav.css('right', -1000);
			bookNowOnDesktopModule.closeBookNow();
			positionContactUsContactDetailsAndOverlayCorrectly();
			isLayerTurnedOn = false;
		}

		function turnOnLayer() {
			$opacityLayer.css('z-index', 35);
			isLayerTurnedOn = true;
		}

		function positionContactUsContactDetailsAndOverlayCorrectly() {
			windowWidth = jswindow.innerWidth;
			if(windowWidth >= 1000) {
				if($bookNow.hasClass('isOpen')) {
					$.each($contactUsGroup, function () {
						$(this).css('right', windowWidth * 0.025 + 240);
					});
				} else {
					$.each($contactUsGroup, function () {
						$(this).css('right', windowWidth * 0.025 + 200);
					});
				}
			}
		}


		/* use only inside resize event listener
		   it runs once on desktop and once on non desktop
		   event if resized multiple times */
		function fixZIndex() {
			if(jswindow.innerWidth >= 1000) {
				wasOnNonDesktopAndResizedToDesktop = isResizingFromDesktopToNonDesktop !== 0 ? true : false;

				isResizingFromNonDesktopToDesktop++;
				if((isResizingFromNonDesktopToDesktop === 1)) {
					if(isLayerTurnedOn && wasOnNonDesktopAndResizedToDesktop) {
						turnOffLayer();
					}
				}
				isResizingFromDesktopToNonDesktop = 0;
			}
			else {
				wasOnDesktopAndResizedToNonDesktop = isResizingFromNonDesktopToDesktop !== 0 ? true : false;

				isResizingFromDesktopToNonDesktop++;
				if(isResizingFromDesktopToNonDesktop === 1){
					if(isLayerTurnedOn && wasOnDesktopAndResizedToNonDesktop) {
						turnOffLayer();
					}
				}
				isResizingFromNonDesktopToDesktop = 0;
			}
		}	

		return {
			fixZIndex: fixZIndex,
			turnOnLayer: turnOnLayer,
			turnOffLayer: turnOffLayer,
			positionContactUsContactDetailsAndOverlayCorrectly: positionContactUsContactDetailsAndOverlayCorrectly
		};
	})();

	// 2. Book now mobile
	var bookNowOnMobileModule = (function () {
		// cache DOM
		var $bookNow  = $('#book-now-mobile');

		// attach listeners
		_attachListeners();

		function _attachListeners() {
			$bookNow.on('click', _toggleCalendar);
		}

		function _toggleCalendar(e) {
			if($calendar.is(':visible'))
			{
				$calendar.hide(0);
				$opacityLayer.css('z-index', -1);				
			} 
			else
			{
				$calendar.css('top', 76).show(0);
				opacityLayerModule.turnOnLayer();
			}
		}
	})();

	// 3. contact us
	var contactUsModule = (function () {
		// cache DOM
		var $contactDetailsCloseBtn = $contactDetails.children('#contact-details-close-btn');
		var isResizingFromNonDesktopToDesktop = 0;
		var isResizingFromDesktopToNonDesktop = 0;
		var windowWidth 					  = 0;

		// attach listeners
		_attachListeners();

		function _attachListeners() {
			$contactUs.on('click', _hide_Contact_Btn_And_Show_Contact_Details);
			$contactDetailsCloseBtn.on('click', show_Contact_Btn_And_Hide_Contact_Details);
		}

		function _hideContactBtn(event) {
			event.preventDefault();
			$contactUs.hide(0);
		}

		function _showContactDetails() {
			$contactDetailsOverlay.width(0);
		}

		function _hide_Contact_Btn_And_Show_Contact_Details(event) {
			if(jswindow.innerWidth >= 1330) {
				_hideContactBtn(event);
				_showContactDetails();
			}
		}

		function _showContactBtn() {
			$contactUs.show(0);
		}

		function _hideContactDetails() {
			$contactDetailsOverlay.width(910);
		}

		function show_Contact_Btn_And_Hide_Contact_Details() {
			_showContactBtn();
			_hideContactDetails();
		}

		function checkContactDetailsVisibility() {
			if(jswindow.innerWidth < 1330 && $contactDetails.is(':visible')) {
				show_Contact_Btn_And_Hide_Contact_Details();
			}
		}

		function checkContactUsVisibility() {
			if(jswindow.innerWidth >= 1000) {
				isResizingFromDesktopToNonDesktop = 0;
				isResizingFromNonDesktopToDesktop++;
				if(isResizingFromNonDesktopToDesktop === 1 && $contactDetailsOverlay.css('width') === '910px') $contactUs.show(0);
			} else {
				isResizingFromNonDesktopToDesktop = 0;
				isResizingFromDesktopToNonDesktop++;
				if(isResizingFromDesktopToNonDesktop === 1) {
					$contactUs.hide(0);
				}
			}
		}

		return {
			checkContactDetailsVisibility: checkContactDetailsVisibility,
			checkContactUsVisibility: checkContactUsVisibility,
			show_Contact_Btn_And_Hide_Contact_Details: show_Contact_Btn_And_Hide_Contact_Details,
		};
	})();

	// 5. sub calendar (mini calendar)
	var subCalendarModule = (function () {
		// cache DOM
		var $dateFields 	    = $calendar.find('.date-field');
		var wasOnDesktop		= false;
		var wasOnNonDesktop 	= false;

		// attach listeners
		_attachListeners();

		// init
		_initSubCalendars();
		
		function _attachListeners() {
			$calendar.on('click', '.calendar-icon', _toggleSubCalendars);
		}

		function _toggleSubCalendars() {
			$(this).prev('.date-field').datetimepicker('show');
		}

		function _initSubCalendars() {
			$dateFields.each(_activateDateField);
			hideSubCalendarIfVisible();
		}

		function _activateDateField() {
			var dateFieldNumber 	= 1;
			
			$(this).datetimepicker({
				timepicker: false,
				format:'d/m/Y',
				minDate: 0, // you can't select dates prior today
				value: ((dateFieldNumber === 1) ? '0' : '+1970/01/02'), // if first date field; set its date to today, otherwise to tomorrow
				onSelectDate:function(dp,$input){
					var departureDate  		= undefined;
					var arrivalDate    		= undefined;
					var $arrivalDateField   = $dateFields.filter('.arrivalDate');
					var $departureDateField = $dateFields.filter('.departureDate');
					var arrivateDateParts   = $arrivalDateField.val().split('/');
					var departureDateParts  = $departureDateField.val().split('/');
					arrivalDate   			= new Date(arrivateDateParts[2], arrivateDateParts[1] - 1, arrivateDateParts[0]);
					departureDate 			= new Date(departureDateParts[2], departureDateParts[1] - 1, departureDateParts[0]);

					if(departureDate < arrivalDate) {
						departureDate = new Date(arrivalDate.getTime() + 86400000);
						$departureDateField.val(_toTwoDigits(departureDate.getDate()) + '/' + _toTwoDigits(parseInt(departureDate.getMonth() + 1)) + '/' + departureDate.getFullYear());
					}
			  	}
			});
			dateFieldNumber++;
		}

		function _toTwoDigits(i) {
			if(i < 10) return '0' + i;
			else return i;
		}

		function hideSubCalendarIfVisible() {
			if(jswindow.innerWidth >= 1000) {
				wasOnDesktop    = true;
				if(wasOnNonDesktop) {
					$dateFields.datetimepicker('hide');
					wasOnNonDesktop = false;
				}
			} else {
				wasOnNonDesktop = true;
				if(wasOnDesktop) {
					$dateFields.datetimepicker('hide');
					wasOnDesktop = false;
				}
			}
		}

		return {
			hideSubCalendarIfVisible: hideSubCalendarIfVisible
		}
	})();

	// 4. calendar counter
	var calendarCounterModule = (function () {
		var counter       	    = 0;
		var maxRooms    	    = 5;
		var maxAdults   	    = 2;
		var maxChildren 	    = 5;
		var targetedTextField   = '';
		var targetedTextFieldId = '';
		var isIncrementAllowed  = true;
		var tempCounter         = 0;

		// attach listeners
		_attachListeners();

		function _attachListeners() {
			$calendar.on('click', '.up-arrow, .down-arrow', _incrementOrDecrementField);
			$calendar.on('blur', '.reservation-count__input-field', _checkTheNumberReserved);
		}

		function _checkTheNumberReserved() {
			targetedTextField   = $(this);
			targetedTextFieldId = targetedTextField.attr('id');
			tempCounter         = targetedTextField.val();

			if(targetedTextFieldId === 'rooms' && tempCounter > maxRooms)
				targetedTextField.val(maxRooms);
			else if(targetedTextFieldId === 'adults' && tempCounter > maxAdults)
				targetedTextField.val(maxAdults);
			else if(targetedTextFieldId === 'children' && tempCounter > maxChildren)
				targetedTextField.val(maxChildren);
			else
				targetedTextField.val(0);
		}

		function _incrementOrDecrementField() {
			var $arrowBeingClicked = $(this);
			var $targetedTextField = $arrowBeingClicked.prevAll('.reservation-count__input-field');

			if($arrowBeingClicked.hasClass('up-arrow'))
				_incrementField($targetedTextField);
			else
				_decrementField($targetedTextField);
		}

		function _incrementField($targetedTextField) {
			counter = parseInt($targetedTextField.val());
			
			isIncrementAllowed = _limitIncrement($targetedTextField, counter);
			
			if(isIncrementAllowed) $targetedTextField.val(++counter);
		}

		function _decrementField($targetedTextField) {


			counter = parseInt($targetedTextField.val());
			counter = (counter <= 0) ? 0 : --counter;
			$targetedTextField.val(counter);
		}

		function _limitIncrement($targetedTextField, counter) {
			targetedTextFieldId = $targetedTextField.attr('id');
			tempCounter = counter;
			tempCounter++;

			if(targetedTextFieldId === 'rooms' && tempCounter > maxRooms)
				return false;
			else if(targetedTextFieldId === 'adults' && tempCounter > maxAdults)
				return false;
			else if(targetedTextFieldId === 'children' && tempCounter > maxChildren)
				return false;
			else
				return true;
		}
	})();

	// 6. mobile nav open close
	var navMobileOpenCloseModule = (function () {
		// cache DOM
		var $threeBars 	 					  = $('#three-bars-wrapper');
		var $navCloseBtn 					  = $nav.children('#nav-close-btn');
		var isResizingFromDesktopToNonDesktop = 0;
		// attach listeners
		_attachListeners();

		function _attachListeners() {
			$threeBars.on('click', _showNavigation);
			$navCloseBtn.on('click', _hideNavigation);
		}

		function _showNavigation() {
			$nav.css('right', 0);
			opacityLayerModule.turnOnLayer();
		}

		function _hideNavigation() {
			$nav.css('right', -1000);
			opacityLayerModule.turnOffLayer();
		}

		function closeMobileNavIfOpenOnResizeFromDesktopToNonDesktop() {
			// if(jswindow.innerWidth < 1000) {
			// 	isResizingFromDesktopToNonDesktop++;
			// 	if(isResizingFromDesktopToNonDesktop === 1 && $opacityLayer.css('z-index') === '-1') $nav.css('right', -1000);
			// } else {
			// 	isResizingFromDesktopToNonDesktop = 0;
			// }
		}

		return {
			closeMobileNavIfOpenOnResizeFromDesktopToNonDesktop: closeMobileNavIfOpenOnResizeFromDesktopToNonDesktop
		}
	})();

	// 7. mobile nav sub menu
	var navMobileSubMenusModule = (function () {
		// cache DOM
		var $activeDropDownBtn                = $('.drop-down-btn.activeLink');
		var $activeDropDown                   = $activeDropDownBtn.children('.drop-down');
		var $dropDownBtns 					  = $('.drop-down-btn');
		var $dropDowns    					  = $dropDownBtns.children('.drop-down');
		var $background 					  = $('.background');
		var $activeLinkArrow                  = undefined;
		var isResizingFromDesktopToNonDesktop = 0;
		var isResizingFromNonDesktopToDesktop = 0;
		var runOnceOnDesktop 				  = 0;
		var runOnceOnNonDesktop 			  = 0;

		// check listeners
		checkListeners();

		// init
		checkStatusOfMobileNavSubMenuArrows();

		function _attachListeners() {
			$nav.off('click').on('click', '.drop-down-btn', _toggleDropDowns);
		}

		function _detachListeners() {
			$nav.off('click');
		}

		function checkListeners() {
			if(jswindow.innerWidth < 1000) {
				runOnceOnNonDesktop++;
				if(runOnceOnNonDesktop === 1) {
					_attachListeners();
					runOnceOnDesktop = 0;
				}
			} else {
				runOnceOnDesktop++;
				if(runOnceOnDesktop === 1) {
					_detachListeners();
					runOnceOnNonDesktop = 0;
				}
			}
		}

		function _toggleDropDowns() {
			var $thisDropDownBtn = $(this);

			$thisDropDownBtn.children('div').toggleClass('right-arrow down-arrow mobileLinkStyle');
			$thisDropDownBtn.children('.drop-down').slideToggle(200);
			
			if($thisDropDownBtn.hasClass('activeLink')) {
				$thisDropDownBtn.removeClass('activeLink');
			}
		}

		function checkStatusOfMobileNavSubMenuArrows() {
			if(jswindow.innerWidth < 1000) {
				isResizingFromDesktopToNonDesktop++;
				if(isResizingFromDesktopToNonDesktop === 1) {
					_addTheArrows();
					isResizingFromNonDesktopToDesktop = 0;
				}
			}
			else {
				isResizingFromNonDesktopToDesktop++;
				if(isResizingFromNonDesktopToDesktop === 1) {
					_removeTheArrows();
					isResizingFromDesktopToNonDesktop = 0;
				}
			}
		}

		function _addTheArrows() {
			$dropDownBtns.children('div').addClass('right-arrow');
		}

		function _removeTheArrows() {
			$dropDownBtns.children('div').removeClass('right-arrow down-arrow mobileLinkStyle');
		}

		function activeLinkResizeFix() {
			// removes display block or none, enabling hover again
			if(jswindow.innerWidth >= 1000) {
				$activeDropDownBtn.removeClass('activeLink');
				$background.hide(0);
			} else {
				$activeLinkArrow = $('.right-arrow');
				if($activeDropDownBtn.hasClass('activeLink')  && $activeLinkArrow.hasClass('right-arrow')  && $activeDropDown.is(':visible')) {
					$activeDropDown.slideToggle(200);
					$activeDropDownBtn.removeClass('activeLink');
				}
			}
		}

		return {
			activeLinkResizeFix: activeLinkResizeFix,  // this is used in windowResizeListenersModule
			checkStatusOfMobileNavSubMenuArrows: checkStatusOfMobileNavSubMenuArrows,
			checkListeners: checkListeners
		};
	})();

	// 8. mobile nav strip sub menu styles
	var navMobileStripDropDownStylesModule = (function () {
		// cache DOM
		var $navListItems = $('.ws-nav-list').children('li:not(.mobile-only)');
		var $dropDowns 	  = $('.drop-down');

		function stripDropDownStyles() {
			if(jswindow.innerWidth < 1000) {
				$navListItems.css('padding', 0);
				$dropDowns.css({width: '', left: ''/*, display: ''*/}).children('li').css('padding', 0);
			}
		}

		return {
			stripDropDownStyles: stripDropDownStyles  // this is used in windowResizeListenersModule
		}
	})();

	// 10. window resize event
	var windowResizeModule = (function (timer) {
		// attach listeners
		_attachListeners();

		function _attachListeners() {
			$window.on('resize', _resizeListeners);
		}

		function _resizeListeners() {
			_runAfterResize();

			// run continuously without a timer
			desktopNavModule.repositionNavListItems();
			opacityLayerModule.positionContactUsContactDetailsAndOverlayCorrectly();
		}

		function _runAfterResize() {
			clearTimeout(timer);
			timer = setTimeout(_runAfterResizeListeners, 100);
			
		}

		function _runAfterResizeListeners() {			
			navMobileSubMenusModule.checkStatusOfMobileNavSubMenuArrows();
			navMobileSubMenusModule.checkListeners();
			// must come after checkStatusOfMobileNavSubMenuArrows
			navMobileSubMenusModule.activeLinkResizeFix();
			navMobileStripDropDownStylesModule.stripDropDownStyles();
			navMobileOpenCloseModule.closeMobileNavIfOpenOnResizeFromDesktopToNonDesktop();
			desktopNavModule.checkNavListItemsListeners();
			desktopNavModule.repositionDropDowns();
			desktopNavModule.modifyNavOpacity();
			contactUsModule.checkContactDetailsVisibility();
			contactUsModule.checkContactUsVisibility();
			opacityLayerModule.fixZIndex();
			subCalendarModule.hideSubCalendarIfVisible();
		}
	})(undefined);
})($(window), window);