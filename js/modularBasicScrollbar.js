var basicScrollbar = (function ($window, jswindow) {
	$('.scroll-down').remove();

	var headerModule = (function () {
		// cache DOM
		var $calendar 	    	     	      = $('#calendar');
		var $nav      	    	     	      = $('#ws-nav');
		var $header   	    	     	      = $('#ws-header');
		var $contactDetails 	     	      = $('#contact-details');
		var $contactDetailsAddress   	      = $contactDetails.children('.contact-details-address');
		var $map 				     	      = $contactDetails.children('.map');
		var $contactDetailsCloseBtn  	      = $contactDetails.children('#contact-details-close-btn');
		var $logo 				     	      = $('#header-logo');
		var $contactUs      	     	      = $('#contact-us');
		var $bookNowWrapper		     	      = $('.book-now-desktop-wrapper');
		var $bookNow   			     	      = $bookNowWrapper.children('.book-now-desktop');
		var $contactDetailsOverlay  		  = $contactDetails.next('.contactDetailsOverlay');
		var $background						  = $('.background');
		var headerStatus	         	      = 'maximized';
		var isResizingFromNonDesktopToDesktop = 0;
		var isResizingFromDesktopToTablet     = 0;
		var isResizingFromTabletToMobile      = 0;
		var wasOnNonDesktop                   = false;

		function toggleHeaderSize($this) {
			if(jswindow.innerWidth >= 1000) {
				if($this.mcs.top !== 0 && headerStatus === 'maximized') {
					_minimizeHeader();
				}
				else if($this.mcs.top === 0 && headerStatus === 'minimized') {
					_maximizeHeader();
				}
			}
		}

		function _minimizeHeader() {
			_initDesktopHeaderDimensions(45, {top: '5.8px', width: '160px', height: '33.402px'}, {height: '35px'}, {height: '35px', top: 5, paddingTop: 12.5, '-webkit-transform': 'translateY(0)','-ms-transform': 'translateY(0)',transform: 'translateY(0)'}, {top: 45}, {top: 40}, {height: '50px', top: 5, '-webkit-transform': 'translateY(0)','-ms-transform': 'translateY(0)',transform: 'translateY(0)'}, {top: 5, '-webkit-transform': 'translateY(0)','-ms-transform': 'translateY(0)',transform: 'translateY(0)'}, {float: 'left', clear: 'none'}, {top: 7}, {top: 6}, 90, {height: 45});
			headerStatus = 'minimized';
		}

		function _maximizeHeader() {
			_initDesktopHeaderDimensions(75, {top: '14.5px', width: '220px', height: '45.9278px'}, {height: '35px'}, {height: '56px', top: 37, paddingTop: 10, '-webkit-transform': 'translateY(-50%)','-ms-transform': 'translateY(-50%)',transform: 'translateY(-50%)'}, {top: 75}, {top: 55}, {height: '35px', top: 37, '-webkit-transform': 'translateY(-50%)','-ms-transform': 'translateY(-50%)',transform: 'translateY(-50%)'}, {top: 37, '-webkit-transform': 'translateY(-50%)','-ms-transform': 'translateY(-50%)',transform: 'translateY(-50%)'}, {float: 'none', clear: 'left'}, {top: 18}, {top: 17}, 120, {height: 75});
			headerStatus = 'maximized';
		}

		function _initDesktopHeaderDimensions(headerHeight, logoDimensions, bookNowHeight, contactDetailsCSS, NavPosition, calendarTopPosition, contactUsCSS, bookNowWrapperCSS, contactDetailsAddressFloat, contactDetailsCloseBtnTopPosition, mapTopPosition, backgroundTopPosition, contactDetailsOverlayHeight) {
        	$header.height(headerHeight);
        	$logo.css(logoDimensions);
        	$bookNow.css(bookNowHeight);
        	$contactDetails.css(contactDetailsCSS);
        	$nav.css(NavPosition);
        	$calendar.css(calendarTopPosition);
			$contactUs.css(contactUsCSS);
			$bookNowWrapper.css(bookNowWrapperCSS);
			$contactDetailsAddress.css(contactDetailsAddressFloat);
			$contactDetailsCloseBtn.css(contactDetailsCloseBtnTopPosition);
			$map.css(mapTopPosition);
			$background.css('top', backgroundTopPosition)
			$contactDetailsOverlay.css(contactDetailsOverlayHeight);
    	}

    	function _initNonDesktopHeader() {
    		wasOnNonDesktop = true;

    		if(jswindow.innerWidth >= 768) {
				_initTabletHeader();		
    		} else {
    			_initMobileHeader();
    		}
    	}

    	function _initTabletHeader() { // run once on resize
    		isResizingFromDesktopToTablet++;
			isResizingFromTabletToMobile = 0;
			if(isResizingFromDesktopToTablet === 1) {
				$logo.css({width: 180, height: 37.5773, top: 26});
				$nav.css('top', 0);
    			$header.height(76);
    			$contactUs.hide(0);
			}
    	}

    	function _initMobileHeader() { // run once on resize
    		isResizingFromDesktopToTablet = 0;
			isResizingFromTabletToMobile++;
			if(isResizingFromTabletToMobile === 1) {
				$logo.css({width: 160, height: 33.402, top: 26});
				$nav.css('top', 0);
    			$header.height(76);
    			$contactUs.hide(0);
			}
    	}

    	function _initDesktopHeader() { // run once on resize
    		isResizingFromNonDesktopToDesktop++;
    		if(isResizingFromNonDesktopToDesktop === 1 && wasOnNonDesktop) {
    			_maximizeHeader();
    		}
    		wasOnNonDesktop = false;
    	}

    	function fixHeaderSize() {
    		if(jswindow.innerWidth >= 1000) {
    			isResizingFromDesktopToTablet = isResizingFromTabletToMobile = 0;
    			_initDesktopHeader();
    		} else {
    			isResizingFromNonDesktopToDesktop = 0;
    			_initNonDesktopHeader();
    		}
    	}

    	return {
    		toggleHeaderSize: toggleHeaderSize,
    		fixHeaderSize: fixHeaderSize
    	}
	})();

	var scrollbarModule = (function () {
		// cache DOM
		var $body = $('body');
		var isResizingFromNonDesktopToDesktop = 0;
		var isResizingFromDesktopToNonDesktop = 0;

		// check on window load event
		if(navigator.userAgent.indexOf('MSIE 9.0') === -1) checkScrollbarStatus();
		
		function checkScrollbarStatus() {
			if(jswindow.innerWidth >= 1000) {
				isResizingFromNonDesktopToDesktop++;
				if(isResizingFromNonDesktopToDesktop === 1) {
					_initScrollbar();
					isResizingFromDesktopToNonDesktop = 0;
				}
			} else {
				isResizingFromDesktopToNonDesktop++;
				if(isResizingFromDesktopToNonDesktop === 1){
					_destroyScrollbar();
					isResizingFromNonDesktopToDesktop = 0;
				}
			}
		}

		function _initScrollbar() {
			$body.mCustomScrollbar({
				callbacks:{
							onScroll: function () {headerModule.toggleHeaderSize(this);},
							onInit: function () {
								desktopNavModule.repositionNavListItems();
						 		desktopNavModule.repositionDropDowns();
							}
			               },
				mouseWheel:{enable: true, scrollAmount: 100},
				theme:"dark",
				scrollInertia: 100,
				scrollEasing: "linear"
			});
		}

		function _destroyScrollbar() {
			$body.mCustomScrollbar("destroy");
		}

		return {
			checkScrollbarStatus: checkScrollbarStatus
		};
	})();

	var windowResizeModule = (function (timer) {
		// attach listeners
		_attachListeners();

		function _attachListeners() {
			$window.on('resize', _resizeListeners);			
		}

		function _resizeListeners() {
			_runAfterResize();
		}

		function _runAfterResize() {
			clearTimeout(timer);
			timer = setTimeout(_runAfterResizeListeners, 100);
		}

		function _runAfterResizeListeners() {
			headerModule.fixHeaderSize();
			scrollbarModule.checkScrollbarStatus();
		}
	})(undefined);

})($(window), window);