<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<link rel="stylesheet" href="css/vendor/flickity.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/news.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
	
		<main class="MW1200 clear-fix">
			<h1 id="page-title">NEWS</h1>
			
			<div class="press-contacts clear-fix">
				<h1 class="center-content">PRESS CONTACTS</h1>
				<div class="float-block first">
					<h2>Germany</h2>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
				</div>
				<div class="float-block leftPadding second">
					<h2>UK</h2>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
				</div>
				<div class="float-block leftPadding third">
					<h2>Lebanon</h2>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
					<p>Lorem ipsum dolor sit amet.</p>
				</div>
			</div>

			<div class="recommeded-news clear-fix">
				<h1 class="recommended-news__title">MORE OPTIONS</h1>
				<div class="moreOptions js-flickity" data-flickity-options='{"lazyLoad": 2, "cellAlign": "left", "pageDots": false, "wrapAround": true, "arrowShape": {"x0": 10, "x1": 40, "y1": 50, "x2": 45, "y2": 50, "x3": 15} }'>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, velit?
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, voluptas?
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, possimus?
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, quis.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, mollitia!
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, rerum.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque, sed.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, impedit.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
				</div> <!-- flickity -->
			</div>	<!-- flickity parent -->

			<div class="recommeded-news clear-fix">
				<h1 class="recommended-news__title">MEDIA COVERAGE</h1>
				<div class="moreOptions js-flickity" data-flickity-options='{"lazyLoad": 2, "cellAlign": "left", "pageDots": false, "wrapAround": true, "arrowShape": {"x0": 10, "x1": 40, "y1": 50, "x2": 45, "y2": 50, "x3": 15} }'>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, explicabo.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, corporis.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, suscipit?
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, dignissimos.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, veritatis.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, quo.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, alias.
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
					<div class="slides">
						<a href="#" class="room bgpink">
							<img src="images/_accomodation.jpg" alt="room">
							<h2 class="room-type">REGULAR ROOM</h2>
							<div class="room-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, maiores!
								<br>
								<br>
								<br>
								<br>
								<br>
								<div class="link">http://krustallos.net</div>
							</div>
						</a>
					</div>
				</div> <!-- flickity -->
			</div>	<!-- flickity parent -->
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
	
	<script src="js/vendor/flickity.pkgd.min.js"></script>
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script>

	</script>
</body>
</html>