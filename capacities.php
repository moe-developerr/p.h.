<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/capacities.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
	
		<main class="social-events clear-fix">
			<h1 id="page-title">ROOMS CAPACITIES AND CHARTS</h1>
			<div class="accomodation-inside">

				<div id="page-img">
					<div class="desktop-only center-content whiteColor">
						<br>
						<br>
						<h1 class="whiteColor">UNWIND AND REJUVENATE</h1>
						<br>
						<br>
						<br>
						<br>
						<br>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, fugit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, asperiores.</p>
						<p>Sit amet, consectetur adipisicing elit. Inventore, autem?Dolor velit distinctio saepe fugiat adipisci accusamus officiis hic quas.</p>
						<br>
						<br>
						<br>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, fugit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, asperiores.</p>
						<p>Sit amet, consectetur adipisicing elit. Inventore, autem?Dolor velit distinctio saepe fugiat adipisci accusamus officiis hic quas.</p>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<p>Opens everyday from 7:00 am to 10:00 pm</p>
						<p>Mezzaine Level, <a href="tel:009611357030" class="boldHoverText whiteColor">+961 (0)1 357030</a></p>
					</div>
				</div>

				<div class="content-container MW1200 clear-fix">
					<div class="social-icons-inner clear-fix">
						<a href="#" class="fa fa-google-plus"></a>
						<a href="#" class="fa fa-linkedin"></a>
						<a href="#" class="fa fa-facebook"></a>
						<a href="#" class="fa fa-twitter"></a>
						<span>SHARE ON</span>
						<a href="#" class="regularBtn">VIEW INTERACTIVE PLAN</a>
					</div>
					
					<h1 class="content-container__heading">LAYOUTS</h1>
					<div class="regularBtnsWrapper clear-fix">
						<button class="regularBtn level1">LEVEL 1</button>
						<button class="regularBtn level2">LEVEL 2</button>
						<button class="regularBtn blue-print">3D BLUE PRINT</button>
					</div>
					
					<div class="rooms-maps bottomMargin30"><div></div></div>
					
					<div class="table-container bottomMargin30">
						<h1 class="content-container__heading">DIMENSIONS</h1>
						<div class="dimensions">
							<div class="table">
								<div class="tr">
									<div class="tc">Event Room</div>
									<div class="tc">Room Dimensions (LxW)</div>
									<div class="tc">Area Sq M</div>
									<div class="tc desktop-only">Ceiling Height</div>
									<div class="tc desktop-only">Door Dimensions (HxW)</div>
									<div class="tc desktop-only">Floor Level</div>
									<div class="tc desktop-only">Natural Lighting</div>
								</div>
							</div>

							<div class="table">
								<div class="tr white clickable">
									<div class="tc">Berytus</div>
									<div class="tc">23.00 X 14.00</div>
									<div class="tc light-pink">322.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc light-pink desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc light-pink desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, eligendi.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr pink clickable">
									<div class="tc">Tyre</div>
									<div class="tc">23.00 X 16.00</div>
									<div class="tc">368.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, tempora.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr white clickable">
									<div class="tc">Sidon</div>
									<div class="tc">23.00 X 14.00</div>
									<div class="tc light-pink">322.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc light-pink desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc light-pink desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, fugit!</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr pink clickable">
									<div class="tc">Byblos</div>
									<div class="tc">23.00 X 16.00</div>
									<div class="tc">368.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, error.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr white clickable">
									<div class="tc">Carthage 1</div>
									<div class="tc">23.00 X 14.00</div>
									<div class="tc light-pink">322.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc light-pink desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc light-pink desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, mollitia.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr pink clickable">
									<div class="tc">Carthage 2</div>
									<div class="tc">23.00 X 16.00</div>
									<div class="tc">368.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, cumque.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>
						</div> <!-- dimensions table -->
					</div>

					<div class="table-container bottomMargin30">
						<h1 class="content-container__heading">DIMENSIONS</h1>
						<div class="capacities">
							<div class="table">
								<div class="tr">
									<div class="tc">Event Room</div>
									<div class="tc">Room Dimensions (LxW)</div>
									<div class="tc">Area Sq M</div>
									<div class="tc desktop-only">Ceiling Height</div>
									<div class="tc desktop-only">Door Dimensions (HxW)</div>
									<div class="tc desktop-only">Floor Level</div>
									<div class="tc desktop-only">Natural Lighting</div>
								</div>
							</div>

							<div class="table">
								<div class="tr white clickable">
									<div class="tc">Berytus</div>
									<div class="tc">23.00 X 14.00</div>
									<div class="tc light-pink">322.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc light-pink desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc light-pink desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, eligendi.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr pink clickable">
									<div class="tc">Tyre</div>
									<div class="tc">23.00 X 16.00</div>
									<div class="tc">368.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, tempora.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr white clickable">
									<div class="tc">Sidon</div>
									<div class="tc">23.00 X 14.00</div>
									<div class="tc light-pink">322.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc light-pink desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc light-pink desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, fugit!</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr pink clickable">
									<div class="tc">Byblos</div>
									<div class="tc">23.00 X 16.00</div>
									<div class="tc">368.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, error.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr white clickable">
									<div class="tc">Carthage 1</div>
									<div class="tc">23.00 X 14.00</div>
									<div class="tc light-pink">322.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc light-pink desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc light-pink desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, mollitia.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>

							<div class="table">
								<div class="tr pink clickable">
									<div class="tc">Carthage 2</div>
									<div class="tc">23.00 X 16.00</div>
									<div class="tc">368.00</div>
									<div class="tc desktop-only">4.00</div>
									<div class="tc desktop-only">2.30 X 1.80</div>
									<div class="tc desktop-only">GROUND</div>
									<div class="tc desktop-only">No</div>
								</div>
							</div>
							<div class="table">
								<div class="tr sub-row">
									<div class="tc sub-cell"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, cumque.</p></div>
									<div class="tc sub-cell"><a href="images/_beirut.jpg" class="imageLink"><img class="capacities__image" src="images/_beirut.jpg" alt=""></a></div>
								</div>
							</div>
						</div> <!-- capacities table -->
					</div>

				</div>
			</div>
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
		
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script>
		var showRoomDetailsInsideTable = (function () {
			// cache DOM
			var $tableContainer = $('.table-container');

			// attach click event Listener
			_attachListeners();

			function _attachListeners() {
				$tableContainer.on('click', '.clickable', _showRoomDetails);
			}

			function _showRoomDetails() {
				var $rowYouWantToShow = $(this).closest('.table').next('.table').children('.sub-row');

				if($rowYouWantToShow.is(':visible')) $rowYouWantToShow.hide(0);
				else $rowYouWantToShow.css('display', 'table-row');
			}
		})();
	</script>
</body>
</html>