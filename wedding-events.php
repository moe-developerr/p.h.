<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<link rel="stylesheet" href="css/pages-content/events.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">
	
		<main class="social-events MW1200 clear-fix">
			<h1 id="page-title">WEDDINGS</h1>
			
			<div class="slider-wrapper">
				<div class="arrow rightArrow"></div>
				<div class="arrow leftArrow"></div>
				<div class="imageSlider rsMinW">
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, inventore.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, eaque.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, iste!</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
				</div>
			</div>
			
			<div class="content-container clear-fix">
				<div class="social-icons-inner clear-fix">
					<a href="" class="fa fa-google-plus"></a>
					<a href="" class="fa fa-linkedin"></a>
					<a href="" class="fa fa-facebook"></a>
					<a href="" class="fa fa-twitter"></a>
					<span>SHARE ON</span>
					<a href="#" class="regularBtn">FILL IN OUR RFP FORM</a>
				</div>
				<h1 class="content-container__heading">PLAN YOUR EVENT</h1>
				<ul class="content-container__list1 without-arrows">
					<li class="content-container__list1-row"><p>From the venue to the menu, the hotel experienced team uses a personal yet professional approach to plan and cater to every type of event, whether a grand banquet or simply an intimate celebration.</p></li>
					<li class="content-container__list1-row">
						<h2>PHOENICIA TOUCH</h2>
						<p>The standard equipment set up includes a stage, parquet dance floor, white or off-white table cloths and chair covers</p>
						<p>Standard audiovisual equipment includes LCD projectors and a ceiling screen</p>
						<p>Cloak room services available.</p>
						<p>One night accommodation is offered with the hotel’s compliments including romantic wedding amenities, room decoration and in-room breakfast for two.</p></li>			
					</li>
					<li class="content-container__list1-row"><h2>CUSTOMIZED WEDDING MENUS</h2>
						<p>Social events, gala dinners, engagements, anniversaries, launchings, fashion shows, concerts, among others have been held in the Grand Ballroom. In keeping up with the revolution in the electronics industry, the Grand Ballroom is fully equipped with the latest and most sophisticated audio-visual facilities. While the vast open space of the Grand Ballroom can host up to 2000 guests, it may be divided into four separate and soundproof rooms to cater for different occasions.</p>					</li>
					<li class="content-container__list1-row"><h2>WEDDING ADVISOR</h2>
						<p>Chefs concoct a tailored menu with respect to the theme of the wedding event using only the freshest ingredients. The Local Origins menu options include regional dishes or specialised local components for which the destination is known. Alternatively the World Kitchen menu options leverage our global know-how to create authentic dishes from around the world.</p>
					</li>
					<li class="content-container__list1-row"><h2>RELAXATION SPA TREATMENTS FOR BRIDE AND GROOMS</h2>
						<p>For the celebration of Love, SPA Phoenicia makes this event an unforgettable one!</p>
						<p>SPA Phoenicia pampers the Bride and Groom with a wide variety of treatments especially concocted to relieve the stress of the wedding preparations and rejuvenate the body &amp; soul to get them ready for this memorable day.</p>
						<p>Also, to thank the Mothers of the Bride and Groom, for their unwavering support through out the daughter and son’s journeys, SPA Phoenicia will give the Mothers a well deserved treatment that will help them release the stress of the big day.</p>
					</li>
				</ul>

				<ul class="content-container__list2">
					<li class="collapsed clear-fix">
						<div><span>Customized Menus that suits the theme</span> <a href="#">Request</a></div>
					</li>
					<li class="collapsed clear-fix">
						<div><span>Preferred Vendors &amp; Service Coordination</span> <a href="#">Request</a></div>
					</li>
					<li class="collapsed clear-fix">
						<div><span>E-Brochure</span> <a href="#">Download</a></div>
					</li>
					<li class="collapsed clear-fix">
						<div><span>Capacity Charts</span> <a href="#">View</a></div>
					</li>
					<li class="collapsable">
						<div>Event Spaces</div>
						<ul>
							<li><span>Meeting Rooms</span> <span>11</span></li>
							<li><span>Meeting Space (sq. ft)</span><span>18116</span></li>
							<li><span>Ballroom</span><span>Available</span></li>
							<li><span>Exhibit Space</span><span>Available</span></li>
							<li><span>Separate Meeting Registration Area</span><span>Available</span></li>
							<li><span>Sales &amp; Meeting Professionals on Site</span><span>15</span></li>
						</ul>
					</li>
					<li class="collapsable pink">
						<div>Equipment &amp; Supplies</div>
						<ul>
							<li><span>Projector - Digital</span> <span>Microphone</span></li>
							<li><span>Projector - LCD</span><span>Stage Lights</span></li>
							<li><span>Projector - Overhead</span><span>Stages Platforms</span></li>
							<li><span>TV - LCD</span><span>Portable Dance Floor</span></li>
							<li><span>TV - Plasma</span><span>Whiteboards</span></li>
							<li><span>DVD Player</span><span>Flip Chart &amp; Markers</span></li>
							<li><span>Podium</span><span>Pens &amp; Writing Pads</span></li>
						</ul>
					</li>
					<li class="share-love-story">
						<div>SHARE YOUR LOVE STORY</div>
						<form action="" action="post">
							<textarea name="" id="" cols="30" rows="10"></textarea>
							<input type="submit" value="SUBMIT">
						</form>
					</li>
				</ul>
			</div> <!-- about room -->
			
			<?php include 'include/img-to-cover-carousel.php'; ?>
			
		</main>
	</div>	<!-- Ws Wrapper -->
	
	<?php include 'include/footer.php'; ?>
	<script src="js/vendor/jquery.royalslider.min.js"></script>
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	
	<script>
		var doubleFaceSliderPackagedModule = (function ($window, jswindow) {
			
			var imageSliderModule = (function () {
				// cache DOM
				var $sliderWrapper 		= $('.slider-wrapper');
				var $imageSlider   		= $sliderWrapper.children('.imageSlider');
				var $thisArrow          = undefined;
				var imageSliderInstance = undefined;

				// attach listeners
				_attachListeners();

				// init
				_init();

				function _attachListeners() {
					$sliderWrapper.on('click', '.arrow', _activateArrows);
				}

				function _init() {
					imageSliderInstance = $imageSlider.royalSlider({
						autoScaleSlider: true,
						imageScaleMode: 'fill',
						loop: true,
						controlNavigation: 'none',
						keyboardNavEnabled: true,
						transitionSpeed: 1000,
						autoplay: {
							enabled: true,
							pauseOnHover: true,
							stopAtAction: false,
							delay: 3000
						}
					}).data('royalSlider');
				}

				function _activateArrows() {
					$thisArrow = $(this);

					if($thisArrow.hasClass('leftArrow')) imageSliderInstance.prev();
					else imageSliderInstance.next();
				}
			})();
			
			var doubleFaceSliderModule = (function () {
				// cache DOM
				var $doubleFaceSliderWrapper 		= $('.doubleFaceSliderWrapper');
				var $doubleFaceSliderDesktopVersion = $doubleFaceSliderWrapper.children('.doubleFaceSlider.desktop-only');
				var $doubleFaceSliderMobileVersion  = $doubleFaceSliderWrapper.children('.doubleFaceSlider.mobile-only');
				var $imageWrapper 					= $('.imgWrapper');	
				var $thisImageWrapper 				= undefined;

				// attach listeners
				_attachListeners();

				function _attachListeners() {
					$doubleFaceSliderWrapper.on('click', '.arrow', _gotoAnotherSlide);
				}

				function setTheDoubleFaceSliderDesktopVersionImagesToCover() {
					if(jswindow.innerWidth >= 1000) {
						$imageWrapper.each(function() {
							$thisImageWrapper = $(this);
							imageToCoverModule.imageToCover($thisImageWrapper, $thisImageWrapper.children('img'));
						});
					}
				}

				function initDoubleFaceSliderDesktopVersion() {
					$doubleFaceSliderDesktopVersion.royalSlider({
					    arrowsNav: false,
					    arrowsNavAutoHide: false,
					    fadeinLoadedSlide: false,
					    controlNavigationSpacing: 0,
					    controlNavigation: 'none',
					    imageScaleMode: 'none',
					    imageAlignCenter:false,
					    loop: true,
					    loopRewind: false,
					    numImagesToPreload: 10,
					    keyboardNavEnabled: true,
					    usePreloader: true
					});
				}

				function initDoubleFaceSlidersMobileVersion() {
					$doubleFaceSliderMobileVersion.royalSlider({
					   	autoHeight: true,
					    arrowsNav: false,
					    arrowsNavAutoHide: false,
					    fadeinLoadedSlide: false,
					    controlNavigationSpacing: 0,
					    controlNavigation: 'none',
					    imageScaleMode: 'none',
					    imageAlignCenter:false,
					    loop: true,
					    loopRewind: false,
					    numImagesToPreload: 1,
					    keyboardNavEnabled: true,
					    usePreloader: true
					});
				}

				function _gotoAnotherSlide() {
					var $arrowClicked = $(this);

					if($arrowClicked.hasClass('leftArrow')) _gotoPreviousSlide($arrowClicked);
					else _gotoNextSlide($arrowClicked);
				}

				function _gotoNextSlide($arrowClicked) {
					$arrowClicked.nextAll('.doubleFaceSlider').data('royalSlider').next();
				}

				function _gotoPreviousSlide($arrowClicked) {
					$arrowClicked.nextAll('.doubleFaceSlider').data('royalSlider').prev();
				}

				return {
					initDoubleFaceSlidersMobileVersion: initDoubleFaceSlidersMobileVersion,
					initDoubleFaceSliderDesktopVersion: initDoubleFaceSliderDesktopVersion,
					setTheDoubleFaceSliderDesktopVersionImagesToCover: setTheDoubleFaceSliderDesktopVersionImagesToCover,
					$doubleFaceSliderDesktopVersion: $doubleFaceSliderDesktopVersion,
					$doubleFaceSliderMobileVersion: $doubleFaceSliderMobileVersion
				};
			})();

			var imageToCoverModule = (function () {
				function imageToCover(parentOfImage, childImage) {
				    var $parent = parentOfImage,
				        $child  = childImage;
				    if(jswindow.innerWidth >= 1000) {
				        var parentHeight = $parent.innerHeight(),
				            parentWidth  = $parent.innerWidth(),
				            imageHeight  = $child.innerHeight(),
				            imageWidth   = $child.innerWidth();
				            
				            if(imageWidth/imageHeight >= parentWidth/parentHeight) { //landscape mode 
				                $child.css({height: '100%', width: 'auto'});
				            } else { //portrait mode
				                $child.css({width: '100%', height: 'auto'});
				            }
				        var newImageHeight   = $child.innerHeight(),
				            newImageWidth    = $child.innerWidth(),
				            widthDifference  = (newImageWidth  - parentWidth) / 2,
				            heightDifference = (newImageHeight - parentHeight) / 2;

				            $child.css({marginLeft: -widthDifference + 'px', marginTop:  -heightDifference + 'px'});
				    }
				    else {
				        $child.css({marginLeft: 0 + 'px', marginTop:  0 + 'px', width: '100%'});
				    }
				}

				return {
				    	imageToCover: imageToCover
				    };
			})()

			var windowLoadListenersModule = (function () {
				// attach Listeners
				_attachListeners();

				function _attachListeners() {
					$window.on('load', _activateDoubleFaceSliders);
				}

				function _activateDoubleFaceSliders() {
					doubleFaceSliderModule.setTheDoubleFaceSliderDesktopVersionImagesToCover();
					doubleFaceSliderModule.initDoubleFaceSliderDesktopVersion();
					doubleFaceSliderModule.$doubleFaceSliderDesktopVersion.animate({opacity: 1}, 1000);
					doubleFaceSliderModule.initDoubleFaceSlidersMobileVersion();
					doubleFaceSliderModule.$doubleFaceSliderMobileVersion.animate({opacity: 1}, 1000);
				}
			})();

			var windowResizeModule = (function (timer) {
				// attach listeners
				_attachListeners();

				function _attachListeners() {
					$window.on('resize', _resizeListeners);
				}

				function _resizeListeners() {
					_runAfterResize();
				}

				function _runAfterResize() {
					clearTimeout(timer);
					timer = setTimeout(_runAfterResizeListeners, 100);
				}

				function _runAfterResizeListeners() {
					doubleFaceSliderModule.setTheDoubleFaceSliderDesktopVersionImagesToCover();
				}
			})(undefined);
		})($(window), window);
	</script>
</body>
</html>