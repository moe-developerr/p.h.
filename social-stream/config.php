<?php

/**
 * PHP Social Stream 1.2
 * Copyright 2015 Axent Media (axentmedia@gmail.com)
 */

// Path to script directory (non-relative)
define( 'SB_PATH', './social-stream/' );

// Locale & language
define( 'SB_LOCALE', 'en' );

/**
 * Available options:
 * en => English (United States)
 * ar => العربية
 * az => Azərbaycan dili
 * bg_BG => Български,
 * bs_BA => Bosanski,
 * ca => Català,
 * cy => Cymraeg,
 * da_DK => Dansk,
 * de_CH => Deutsch (Schweiz),
 * de_DE => Deutsch,
 * el => Ελληνικά,
 * en_CA => English (Canada),
 * en_AU => English (Australia),
 * en_GB => English (UK),
 * eo => Esperanto,
 * es_PE => Español de Perú,
 * es_ES => Español,
 * es_MX => Español de México,
 * es_CL => Español de Chile,
 * eu => Euskara,
 * fa_IR => فارسی,
 * fi => Suomi,
 * fr_FR => Français,
 * gd => Gàidhlig,
 * gl_ES => Galego,
 * haz => هزاره گی,
 * he_IL => עִבְרִית,
 * hr => Hrvatski,
 * hu_HU => Magyar,
 * id_ID => Bahasa Indonesia,
 * is_IS => Íslenska,
 * it_IT => Italiano,
 * ja => 日本語,
 * ko_KR => 한국어,
 * lt_LT => Lietuvių kalba,
 * my_MM => ဗမာစာ,
 * nb_NO => Norsk bokmål,
 * nl_NL => Nederlands,
 * nn_NO => Norsk nynorsk,
 * oci => Occitan,
 * pl_PL => Polski,
 * ps => پښتو,
 * pt_PT => Português,
 * pt_BR => Português do Brasil,
 * ro_RO => Română,
 * ru_RU => Русский,
 * sk_SK => Slovenčina,
 * sl_SI => Slovenščina,
 * sq => Shqip,
 * sr_RS => Српски језик,
 * sv_SE => Svenska,
 * th => ไทย,
 * tr_TR => Türkçe,
 * ug_CN => Uyƣurqə,
 * uk => Українська,
 * zh_CN => 简体中文,
 * zh_TW => 繁體中文
*/

// DateTime Format - Unix style
define( 'SB_DATE_FORMAT', '%B %e, %Y' );
define( 'SB_TIME_FORMAT', '%I:%M %p' );

// For Ajax Security
define( 'SB_NONCE_KEY', '1a2b3c4d' ); // Replace this with a different unique phrases

// Social API connection timeout (sec)
define( 'SB_API_TIMEOUT', 15 );

// API Credentials
$GLOBALS['api'] = array(
    'facebook' => array(
        'facebook_access_token' => '116937628381291|3Nsj7Wu99Ep0OInEKWlqqwYmaK8' // Replace with a valid Facebook App Token
        // Use a long-lived App Token - e.g. <numeric part>|<alphanumeric part>.
    ),
    'twitter' => array(
        'twitter_api_key' => '', // Replace with your Twitter API Key
        'twitter_api_secret' => '', // Replace with your Twitter API Secret
        'twitter_access_token' => '', // Replace with your Twitter OAuth Access Token
        'twitter_access_token_secret' => '' // Replace with your Twitter OAuth Access Token Secret
    ),
    'google' => array(
        'google_api_key' => '' // Replace with your Google API KEY
    ),
    'instagram' => array(
        'instagram_access_token' => '232004505.1677ed0.5bf9bd283eee448ba0266e8e09849991' // Replace with your Instagram Access Token
    ),
    'flickr' => array(
        'flickr_api_key' => '' // Replace with your Flickr API Key
    ),
    'tumblr' => array(
        'tumblr_api_key' => '' // Replace with your Tumblr API Key
    ),
    'soundcloud' => array(
        'soundcloud_client_id' => '' // Replace with your SoundCloud Client ID
    )
);

$social_colors = array(
    0 => '#305790',
    1 => '#06d0fe',
    2 => '#c04d2e',
    3 => '#2E4E65',
    4 => '#2d6eae',
    5 => '#cb1218',
    6 => '#ff0185',
    7 => '#295477',
    8 => '#b80000',
    9 => '#00a0dc',
    10 => '#ec4415',
    11 => '#495d51',
    12 => '#d78b2d',
    13 => '#4c75a3',
    14 => '#ff3300'
);

$social_colors = array(
    0 => '#48166c',
    1 => '#48166c',
    2 => '#48166c',
    3 => '#48166c',
    4 => '#48166c',
    5 => '#48166c',
    6 => '#48166c',
    7 => '#48166c',
    8 => '#48166c',
    9 => '#48166c',
    10 => '#48166c',
    11 => '#48166c',
    12 => '#48166c',
    13 => '#48166c',
    14 => '#48166c'
);

// Themes
$GLOBALS['themes'] = array(
    // Modern Light
    'sb-modern-light' => array(
        'layout' => 'modern',
        'font_size' => '11',
        'social_colors' => $social_colors,
        'social_icons' => '',
        'type_icons' => '',
        'custom_css' => '',
        'wall' => array(
            //'background_color' => '#f3f3f3',
            //'border_color' => '#d9d9d9',
            //'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'link_color' => '#305790',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e5e5e5',
            'item_border_size' => 1
        ),
        'timeline' => array(
            'background_color' => '',
            'border_color' => '',
            'border_size' => 0,
            'background_image' => '',
            'font_color' => '#000000',
            'link_color' => '#305790',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e5e5e5',
            'item_border_size' => 1
        ),
        'feed' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#ffffff',
            'border_color' => '#ffffff',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'link_color' => '#305790',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#ffffff',
			'height' => '100',
            'item_border_size' => 0
        ),
        'feed_sticky' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#FFFFFF',
            'opener_image' => '',
            'background_color' => '#f2f2f2',
            'border_color' => '#d6d6d6',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'link_color' => '#305790',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e2e2e2',
            'item_border_size' => 1
        ),
        'feed_carousel' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#f2f2f2',
            'border_color' => '#e5e5e5',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'link_color' => '#305790',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e2e2e2',
            'item_border_size' => 1
        )
    ),
    // Modern 2 Light
    'sb-modern2-light' => array(
        'layout' => 'modern2',
        'font_size' => '11',
        'social_colors' => $social_colors,
        'type_icons' => '',
        'custom_css' => '',
        'wall' => array(
            //'background_color' => '#f3f3f3',
            //'border_color' => '#d9d9d9',
            //'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e5e5e5',
            'item_border_size' => 1
        ),
        'timeline' => Array(
            'background_color' => '',
            'border_color' => '',
            'border_size' => 0,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e5e5e5',
            'item_border_size' => 1
        ),
        'feed' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#ffffff',
            'border_color' => '#ffffff',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e2e2e2',
            'item_border_size' => 0
        ),
        'feed_sticky' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#FFFFFF',
            'opener_image' => '',
            'background_color' => '#f2f2f2',
            'border_color' => '#d6d6d6',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e2e2e2',
            'item_border_size' => 1
        ),
        'feed_carousel' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            //'background_color' => '#f2f2f2',
            //'border_color' => '#e5e5e5',
            //'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#e2e2e2',
            'item_border_size' => 1
        )
        ),
    // Default Light
    'sb-default-light' => array(
        'layout' => 'default',
        'font_size' => '11',
        'social_colors' => $social_colors,
        'type_icons' => '',
        'custom_css' => '',
        'wall' => Array(
            //'background_color' => '#f3f3f3',
            //'border_color' => '#d9d9d9',
            //'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '',
            'item_border_size' => 1
        ),
        'timeline' => Array(
            'background_color' => '',
            'border_color' => '',
            'border_size' => 0,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => 'transparent',
            'item_border_size' => 1
        ),
        'feed' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#fcfcfc',
            'border_color' => '#e5e5e5',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#050505',
            'item_background_color' => '#ffffff',
            'item_border_color' => '',
            'item_border_size' => 1
        ),
        'feed_sticky' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#FFFFFF',
            'opener_image' => '',
            'background_color' => '#FFFFFF',
            'border_color' => '#e5e5e5',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#FFFFFF',
            'item_border_color' => '',
            'item_border_size' => 1
        ),
        'feed_carousel' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#fcfcfc',
            'border_color' => '#e5e5e5',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#050505',
            'item_background_color' => '#ffffff',
            'item_border_color' => '',
            'item_border_size' => 1
        )
        ),
    // Flat Light
    'sb-flat-light' => array(
        'layout' => 'flat',
        'font_size' => '11',
        'social_colors' => $social_colors,
        'type_icons' => '',
        'custom_css' => '',
        'wall' => Array(
            'background_color' => '',
            'border_color' => '',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '',
            'item_border_size' => 1
        ),
        'timeline' => Array(
            'background_color' => '',
            'border_color' => '',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => 'transparent',
            'item_border_size' => 2
        ),
        'feed' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#ffffff',
            'border_color' => '#cecece',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '',
            'item_border_size' => 2
        ),
        'feed_sticky' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'opener_image' => '',
            'background_color' => '#ffffff',
            'border_color' => '#545454',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '#a3a3a3',
            'item_border_size' => 2
        ),
        'feed_carousel' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#ffffff',
            'border_color' => '#cecece',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#000000',
            'item_background_color' => '#ffffff',
            'item_border_color' => '',
            'item_border_size' => 2
        )
        ),
    // Modern Dark
    'sb-modern-dark' => array(
        'layout' => 'modern',
        'font_size' => '11',
        'social_colors' => $social_colors,
        'type_icons' => '',
        'custom_css' => '',
        'wall' => Array(
            'background_color' => '#2d2d2d',
            'border_color' => '#280000',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#ffffff',
            'item_background_color' => '#444444',
            'item_border_color' => '#050505',
            'item_border_size' => 1
        ),
        'timeline' => Array(
            'background_color' => '#2d2d2d',
            'border_color' => '#280000',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#ffffff',
            'item_background_color' => '#444444',
            'item_border_color' => '#000000',
            'item_border_size' => 1,
        ),
        'feed' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#2b2b2b',
            'border_color' => '#000000',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#ffffff',
            'item_background_color' => '#444444',
            'item_border_color' => '#000000',
            'item_border_size' => 1
        ),
        'feed_sticky' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#FFFFFF',
            'opener_image' => '',
            'background_color' => '#2d2d2d',
            'border_color' => '#000000',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#FFFFFF',
            'item_background_color' => '#545454',
            'item_border_color' => '#000000',
            'item_border_size' => 3
        ),
        'feed_carousel' => Array(
            'title_background_color' => '#dd3333',
            'title_color' => '#ffffff',
            'background_color' => '#2b2b2b',
            'border_color' => '#000000',
            'border_size' => 1,
            'background_image' => '',
            'font_color' => '#ffffff',
            'item_background_color' => '#444444',
            'item_border_color' => '#000000',
            'item_border_size' => 1
        )
        )
    );