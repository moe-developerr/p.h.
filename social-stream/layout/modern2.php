<?php

/**
 * PHP Social Stream 1.3
 * Copyright 2015 Axent Media (axentmedia@gmail.com)
 */

class modern2Layout {
    public $target;
    function create_item( $feed_class, $param, $attr = array(), $output = array() ) {
        $iconSocial = ( @$param['icon'][0] ) ? '<img src="'.$param['icon'][0].'" class="origin-flag" style="vertical-align:middle">' : '<span class="icon sb-' . $feed_class . '"><i class="sb-cicon sb-' . $feed_class . '"></i></span>';
        $inner = '
            <div class="sb-inner">';

        if (@$param['user']) {
            if (@$param['user']['title'] && @$param['user']['name']) {
                $user_title = @$param['user']['title'];
                $user_text = ( @$param['user']['url'] ) ? '<a href="' . @$param['user']['url'] . '"'.$this->target.'>' . @$param['user']['name'] . '</a>' : @$param['user']['name'];
            } else {
                $user_title = @$param['user']['name'];
                if (@$param['user']['status'])
                    $user_text = ( @$param['url'] ) ? '<a href="' . @$param['url'] . '"'.$this->target.'>' . @$param['user']['status'] . '</a>' : $param['user']['status'];
                else
                    $user_title_style = ' style="padding-top: 5px"';
            }
            if (@$output['user']) {
                $inner .= '
                <span class="sb-user">';
                if (@$param['user']['image']) {
                    $user_image = ( @$param['user']['url'] ) ? '<a href="' . @$param['user']['url'] . '"'.$this->target.'><img alt="' . @$param['user']['name'] . '" src="' . $param['user']['image'] . '"></a>' : '<img alt="' . @$param['user']['name'] . '" src="' . $param['user']['image'] . '">';
                    $inner .= '
    				<div class="sb-uthumb">'.$user_image.'</div>';
                } else {
                    $no_thumb_class = ' sb-nouthumb';
                }
                $user_title_linked = ( @$param['user']['url'] ) ? '<a href="' . @$param['user']['url'] . '"'.$this->target.'>'.$user_title.'</a>' : $user_title;
                $inner .= '
                    <div class="sb-uinfo'.@$no_thumb_class.'">
                        <div class="sb-utitle"'.@$user_title_style.'>' . $user_title_linked . '</div>';
                if (@$user_text)
                    $inner .= '<div class="name">' . $user_text . '</div>';
                $inner .= '
                    </div>
                </span>';
            }
        }
        $inner .= $iconSocial;
        if (@$param['title'] && @$output['title'] && ! @$attr['carousel']) {
            $inner .= '
            <span class="sb-title">
                ' . $param['title'] . '
            </span>';
        }
        
        if (@$attr['carousel']) {
            if (@$param['object'] && @$output['thumb']) {
                $inner .= '
                <div class="sb-thumb sb-object">
                    ' . $param['object'] . '
                </div>';
            } else {
            $cropclass = 'sb-crop';
            if (@$param['iframe'])
                $cropclass .= ' iframe';
            if (@$param['thumb'] && @$output['thumb']) {
                $thumb = '<a class="'.$cropclass.'" style="background-image: url(\'' . htmlspecialchars($param['thumb']) . '\');" href="' . (@$param['thumburl'] ? $param['thumburl'] : @$param['url']) . '"'.$this->target.'></a>';
            } else {
                $cropclass .= ' sb-userimg';
                if (@$param['user']['image'] && ! @$output['thumb'] ) {
                    $thumb = '<div class="'.$cropclass.'"><img src="' . $param['user']['image'] . '" alt=""><br /><span>'.$user_title.'</span></div>';
                }
                else {
                    $thumb = (@$param['title']) ? '<div class="'.$cropclass.'"><br /><br /><span>'.$param['title'].'</span></div>' : '<div class="'.$cropclass.' sb-quote"></div>';
                }
            }
            
            $inner .= '
            <div class="sb-thumb">
                ' . $thumb . '
            </div>';
            }
        } else {
            if (@$param['thumb'] && @$output['thumb']) {
                $playstate = (@$param['iframe'] !== null) ? '<div class="sb-playstate"></div>' : '';
                $inner .= '
                <div class="sb-thumb">
                    <a href="' . htmlspecialchars(@$param['thumburl'] ? $param['thumburl'] : @$param['url']) . '"'.@$param['iframe'].$this->target.'><img src="' . htmlspecialchars($param['thumb']) . '" alt="">'.$playstate.'</a>
                </div>';
            }
            elseif (@$param['object'] && @$output['thumb']) {
                $inner .= '
                <span class="sb-thumb sb-object">
                    ' . $param['object'] . '
                </span>';
            }
        }
        
        if ( (@$param['text'] && @$output['text']) || @$attr['carousel'] )
            $inner .= '
            <span class="sb-text">
                ' . $param['text'] . '
            </span>';
        
        if ( ! @$attr['carousel']) {
        if (@$param['tags'] && @$output['tags'])
            $inner .= '
            <span class="sb-text">
                <strong>'.ss_lang( 'tags' ).': </strong>' . $param['tags'] . '
            </span>';
        
        $inner .= @$param['meta'];
        }
        $iconType = ( @$param['icon'][0] ) ? '<img src="'.$param['icon'][0].'" style="vertical-align:middle">' : '<i class="sb-bico sb-' . $param['type'] . '" title="' . ucfirst($param['type']) . '"></i>';
        $us = '';
        if ($param['date'] && @$output['info'])
        $us .= '
            <div class="sb-date">
                ' . $iconType . '
                <a href="' . @$param['url'] . '"'.$this->target.'>'.ss_lang( 'posted' ).': ' . ss_friendly_date($param['date']) . '</a>
            </div>';
        if (@$param['url'] && @$output['share']) {
            if (@$param['share'])
                $us .= $param['share'];
            else {
            $sharetitle = @urlencode( strip_tags($param['title']) );
            $us .= '
                <span class="sb-share">
                    <a class="sb-sicon sb-facebook fa-hover" href="http://www.facebook.com/sharer.php?u=' . urlencode($param['url']) . '&t=' . @$sharetitle . '"'.$this->target.'></a>
                    <a class="sb-sicon sb-twitter fa-hover" href="https://twitter.com/share?url=' . urlencode($param['url']) . '&text=' . @$sharetitle . '"'.$this->target.'></a>
                    <a class="sb-sicon sb-google fa-hover" href="https://plus.google.com/share?url=' . urlencode($param['url']) . '"'.$this->target.'></a>
                    <a class="sb-sicon sb-linkedin fa-hover" href="http://www.linkedin.com/shareArticle?mini=true&url=' . urlencode($param['url']) . '&title=' . @$sharetitle . '"'.$this->target.'></a>
                </span>';
            }
        }
        if (@$us)
            $inner .= '
            <span class="sb-info">
                ' . $us . '
            </span>';
        $inner .= '
        </div>';
        
        if ( $attr['type'] == 'timeline' ) {
            $icon = ( @$param['icon'][1] ) ? '<img src="'.$param['icon'][1].'" style="vertical-align:middle">' : '<i class="sb-bico sb-wico sb-' . $param['type'] . '"></i>';
            $out = '
          <div class="timeline-row">
            <div class="timeline-time">
              <small>'. ss_i18n_date($param['date'], SB_DT_FORMAT) .'</small>'. ss_i18n_date($param['date'], SB_TT_FORMAT) .'
            </div>
            <div class="timeline-icon">
              <div class="bg-' . $feed_class . '">
                ' . $icon . '
              </div>
            </div>
            <div class="timeline-content">
              <div class="panel-body sb-item sb-' . $feed_class . '">
              ' . $inner . '
              </div>
            </div>
          </div>' . "\n";
        } else {
            $tag = ( $attr['type'] != 'feed' || @$attr['carousel'] ) ? 'div' : 'li';
            $out1 = '
            <'.$tag.' class="sb-item sb-' . $feed_class . '">
                ' . $inner;
            $out1 .= '
            </'.$tag.'>' . "\n";
            
            $out = (@$attr['carousel']) ? '<li>'.$out1.'</li>' : $out1;
        }
        return $out;
    }

    function create_colors( $social_colors, $feed_keys, $type, $dotboard, $attr, $themetypeoption ) {
        $style = array();
        foreach ($social_colors as $colorKey => $colorVal) {
            if (@$colorVal && @$colorVal != 'transparent') {
                // set colors for networks
                $rgbColorVal = ss_hex2rgb($colorVal);
                
                if ( $type == 'timeline' )
                    $style[$dotboard.' .bg-'.$feed_keys[$colorKey]][] = 'background-color: rgba('.$rgbColorVal.', 0.8) !important';

                $dotfilter = ( $type == 'wall' ) ? str_replace(array('timeline', '.sboard'), array('sb', ''), $dotboard) : $dotboard;
                $style[$dotfilter.' .sb-'.$feed_keys[$colorKey].'.fa-hover:hover, '.$dotfilter.' .sb-'.$feed_keys[$colorKey].'.active'][] = 'background-color: '.$colorVal.' !important;border-color: '.$colorVal.' !important;color: #fff !important';
                
                // set colors for tabs
                if (@$attr['tabable']) {
                    if (@$attr['position'] == 'normal')
                        $style["$dotboard.tabable .sb-tabs .sticky .".$feed_keys[$colorKey].":hover, $dotboard.tabable .sb-tabs .sticky .".$feed_keys[$colorKey].".active"][] = 'border-bottom-color: '.$colorVal;
                    else
                        $style["$dotboard.tabable .sb-tabs .sticky .".$feed_keys[$colorKey].":hover, $dotboard.tabable .sb-tabs .sticky .".$feed_keys[$colorKey].".active"][] = 'background-color: '.$colorVal;
                }
            }
        }

        // set item border
        if ( @$themetypeoption['item_border_color'] ) {
            if ( $themetypeoption['item_border_color'] != 'transparent') {
                $dontbordersize = true;
                $style["$dotboard .sb-item"][] = 'border: '.@$themetypeoption['item_border_size'].'px solid '.$themetypeoption['item_border_color'];
            }
        }
        if ( @$themetypeoption['item_border_size'] && ! @$dontbordersize ) {
            $style["$dotboard .sb-item"][] = 'border-width: '.@$themetypeoption['item_border_size'].'px';
        }
        // set footer color
        if ( @$themetypeoption['font_color'] && @$themetypeoption['font_color'] != 'transparent') {
            $font_rgbColorVal = ss_hex2rgb($themetypeoption['font_color']);
            $style[$dotboard.'.sb-modern2 .sb-item .sb-info a'][] = 'color: rgba('.$font_rgbColorVal.', 0.8) !important';
        }
        
        return $style;
    }
}
?>