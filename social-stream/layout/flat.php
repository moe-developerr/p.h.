<?php

/**
 * PHP Social Stream 1.2
 * Copyright 2015 Axent Media (axentmedia@gmail.com)
 */

class flatLayout {
    public $target;
    function create_item( $feed_class, $param, $attr = array(), $output = array() ) {
        $inner = '
            <div class="sb-inner">';
            
        if (@$param['title'] && @$output['title'] && ! @$attr['carousel'])
            $inner .= '
            <span class="sb-title">
                ' . $param['title'] . '
            </span>';
            
        if (@$attr['carousel']) {
            $cropclass = 'sb-crop';
            if (@$param['iframe'])
                $cropclass .= ' iframe';
            if (@$param['thumb'] && @$output['thumb']) {
                $thumb = '<a class="'.$cropclass.'" style="background-image: url(\'' . htmlspecialchars($param['thumb']) . '\');" href="' . (@$param['thumburl'] ? $param['thumburl'] : @$param['url']) . '"'.$this->target.'></a>';
            } else {
                $user_title = (@$param['user']['title']) ? $param['user']['title'] : $param['user']['name'];
                $cropclass .= ' sb-userimg';
                if (@$param['user']['image']) {
                    $thumb = '<div class="'.$cropclass.'"><img src="' . $param['user']['image'] . '" alt=""><br /><span>'.$user_title.'</span></div>';
                }
                else {
                    $thumb = '<div class="'.$cropclass.'"><br /><br /><span>'.$user_title.'</span></div>';
                }
            }
            $inner .= '
            <div class="sb-thumb">
                ' . $thumb . '
            </div>';
        } else {
            if (@$param['thumb'] && @$output['thumb']) {
                $playstate = (@$param['iframe'] !== null) ? '<div class="sb-playstate"></div>' : '';
                $inner .= '
                <div class="sb-thumb">
                    <a href="' . htmlspecialchars(@$param['thumburl'] ? $param['thumburl'] : @$param['url']) . '"'.@$param['iframe'].$this->target.'><img src="' . htmlspecialchars($param['thumb']) . '" alt="">'.$playstate.'</a>
                </div>';
            }
        }
        if ( (@$param['text'] && @$output['text']) || @$attr['carousel'] ) {
            $inner .= '<span class="sb-text">';
            
            if (@$attr['carousel'])
                $inner .= '
                <span class="sb-title">
                    ' . @$param['title'] . '
                </span>';
                
            $inner .= $param['text'];
            $inner .= '</span>';
        }
        if (!@$attr['carousel']) {
        if (@$param['tags'] && @$output['tags'])
            $inner .= '
            <span class="sb-text">
                <strong>'.ss_lang( 'tags' ).': </strong>' . $param['tags'] . '
            </span>';
        
        $inner .= @$param['meta'];
        }
        $us = '';
        if ( @$param['url'] && @$output['share'] ) {
            if (@$param['share'])
                $us .= $param['share'];
            else {
            $sharetitle = @urlencode( strip_tags($param['title']) );
            $us .= '
                <span class="sb-share">
                    <a class="sb-sicon sb-facebook fa-hover" href="http://www.facebook.com/sharer.php?u=' . urlencode($param['url']) . '&t=' . @$sharetitle . '"'.$this->target.'></a>
                    <a class="sb-sicon sb-twitter fa-hover" href="https://twitter.com/share?url=' . urlencode($param['url']) . '&text=' . @$sharetitle . '"'.$this->target.'></a>
                    <a class="sb-sicon sb-google fa-hover" href="https://plus.google.com/share?url=' . urlencode($param['url']) . '"'.$this->target.'></a>
                    <a class="sb-sicon sb-linkedin fa-hover" href="http://www.linkedin.com/shareArticle?mini=true&url=' . urlencode($param['url']) . '&title=' . @$sharetitle . '"'.$this->target.'></a>
                </span>';
            }
        }
        if ( @$param['user'] && @$output['user'] ) {
            $user_text = ( @$param['user']['url'] ) ? '<a href="' . @$param['user']['url'] . '"'.$this->target.'>' . @$param['user']['name'] . '</a>' : @$param['user']['name'];
            $us .= '
            <span class="sb-user">
                <i class="sb-bico sb-user"></i> ' . $user_text . '
            </span>';
        }
        if (@$us)
            $inner .= '
            <span class="sb-info">
                ' . $us . '
            </span>';
        $inner .= '
        </div>';
        
        if ( $attr['type'] == 'timeline' ) {
            $icon = ( @$param['icon'][1] ) ? '<img src="'.$param['icon'][1].'" style="vertical-align:middle">' : '<i class="sb-icon sb-' . $feed_class . '"></i>';
            $out = '
          <div class="timeline-row">
            <div class="timeline-time">
              <small>'. ss_i18n_date($param['date'], SB_DT_FORMAT) .'</small>'. ss_i18n_date($param['date'], SB_TT_FORMAT) .'
            </div>
            <div class="timeline-icon">
              <div class="bg-' . $feed_class . '">
                ' . $icon . '
              </div>
            </div>
            <div class="timeline-content">
              <div class="panel-body sb-item sb-' . $feed_class . '">
              ' . $inner . '
              </div>
            </div>
          </div>' . "\n";
        } else {
            $icon = ( @$param['icon'][0] ) ? '<img src="'.$param['icon'][0].'" style="vertical-align:middle">' : '<i class="sb-icon sb-' . $feed_class . ' fa-2x pull-left"></i>';
            $tag = ( $attr['type'] != 'feed' || @$attr['carousel'] ) ? 'div' : 'li';
            $out1 = '
            <'.$tag.' class="sb-item sb-' . $feed_class . '">
                ' . $inner;
            if ($param['date'] && @$output['info'])
            $out1 .= '
                <div class="sb-footer bg-' . $feed_class . '">
                    ' . $icon . '
                    <a href="' . @$param['url'] . '"'.$this->target.'>'.ss_lang( 'posted' ).': ' . ss_friendly_date($param['date']) . '</a>
                </div>';
            $out1 .= '
            </'.$tag.'>' . "\n";
            
            $out = (@$attr['carousel']) ? '<li>'.$out1.'</li>' : $out1;
        }
        return $out;
    }
    
    function create_colors( $social_colors, $feed_keys, $type, $dotboard, $attr, $themetypeoption ) {
        $style = array();
        foreach ($social_colors as $colorKey => $colorVal) {
            if (@$colorVal && @$colorVal != 'transparent') {
                $style[$dotboard.' .bg-'.$feed_keys[$colorKey]][] = 'background-color:'.$colorVal;
                $style[$dotboard.'.sb-flat .sb-item.sb-'.$feed_keys[$colorKey]][] = 'border-color:'.$colorVal;
                //.' .inner'

                $dotfilter = ( $type == 'wall' ) ? str_replace(array('timeline', '.sboard'), array('sb', ''), $dotboard) : $dotboard;
                if (!@$attr['carousel'])
                    $style[$dotfilter.' .sb-'.$feed_keys[$colorKey].'.fa-hover:hover, '.$dotfilter.' .sb-'.$feed_keys[$colorKey].'.active'][] = 'background-color: '.$colorVal.' !important;border-color: '.$colorVal.' !important;color: #fff !important';
                
                // set colors for tabs
                if (@$attr['tabable']) {
                    $style["$dotboard.tabable .sb-tabs .sticky .".$feed_keys[$colorKey].":hover, $dotboard.tabable .sb-tabs .sticky .".$feed_keys[$colorKey].".active"][] = 'background-color: '.$colorVal;
                }
            }
        }
        
        // set item border
        if ( @$themetypeoption['item_border_color'] ) {
            if ( $themetypeoption['item_border_color'] != 'transparent') {
                $dontbordersize = true;
                $style["$dotboard .sb-item"][] = 'border: '.@$themetypeoption['item_border_size'].'px solid '.$themetypeoption['item_border_color'];
                // .inner
            }
        }
        if ( @$themetypeoption['item_border_size'] && ! @$dontbordersize ) {
            $style["$dotboard .sb-item"][] = 'border-width: '.@$themetypeoption['item_border_size'].'px';
            // .inner
        }
        
        return $style;
    }
}