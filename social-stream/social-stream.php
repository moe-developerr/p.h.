<?php

/**
 * Script Name: PHP Social Stream
 * Script URI: http://axentmedia.com/php-social-stream/
 * Description: Combine all your social media network & feed updates (Facebook, Twitter, Google+, Flickr, YouTube, RSS, ...) into one feed and display on your website.
 * Tags: social media, social networks, social feed, social tabs, social wall, social timeline, social stream, php social stream, feed reader, facebook, twitter, google+, tumblr, delicious, pinterest, flickr, instagram, youtube, vimeo, stumbleupon, deviantart, rss, soundcloud, vk
 * Version: 1.3
 * Author: Axent Media
 * Author URI: http://axentmedia.com/
 * License: http://codecanyon.net/licenses/standard
 * 
 * Copyright 2015 Axent Media (axentmedia@gmail.com)
 */

// Load configuration
define( 'SB_DIR', dirname( __FILE__ ) );
require( SB_DIR . '/config.php' );

if ( ! defined( 'SB_PATH' ) )
    exit('Path to PHP Social Stream script directory is not defined!');

// For load more
session_start();

// Make sure feeds are getting local timestamps
if ( ! ini_get('date.timezone') )
	date_default_timezone_set('UTC');

// DateTime localization
if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
    setlocale(LC_ALL, ss_win_locale(SB_LOCALE));
} else {
    setlocale(LC_ALL, SB_LOCALE);
}

// Define constants
define( 'SB_DT_FORMAT', ss_format_locale(SB_DATE_FORMAT) );
define( 'SB_TT_FORMAT', ss_format_locale(SB_TIME_FORMAT) );
define( 'SB_LOGFILE', SB_DIR . '/sb.log' );

// load cache system
include( SB_DIR . '/library/SimpleCache.php' );

// Language localization
include( SB_DIR . '/language/social-stream-'.SB_LOCALE.'.php' );

// Social Stream main class
class SocialStream {
    public $attr;
    public $echo = true;
    public $args = null;
    
    public function __construct() {}
    
    // Initialize by property
    function run(){
        $this->init( $this->attr, $this->echo, $this->args );
    }

    // Initialize by function
    function init( $attr, $echo = true, $args = null, $ajax_feed = array(), $loadmore = array() ) {
        
        $id = (@$attr['id']) ? $attr['id'] : 1;
        $type = (@$attr['type']) ? $attr['type'] : 'wall';
        
        // get default setting & post options
        $setoption = array(
            'setting' => array(
                'theme' => 'sb-modern-light',
                'results' => '30',
                'words' => '40',
                'commentwords' => '20',
                'titles' => '15',
                'readmore' => '1',
                'order' => 'date',
                'filters' => '1',
                'loadmore' => '1',
                'links' => '1',
                'nofollow' => '1',
                'filters_order' => 'facebook,twitter,google,tumblr,delicious,pinterest,flickr,instagram,youtube,vimeo,stumbleupon,deviantart,rss,soundcloud,vk',
                'cache' => '360',
                'crawl' => '10',
                'debuglog' => '0'
            ),
            'wallsetting' => array(
                'animate' => 'true',
                'delay' => '0',
                'filter_direction' => 'false',
                'itemwidth' => '230',
                'fixSize' => '1'
            ),
            'feedsetting' => array(
                'rotate_speed' => '100',
                'duration' => '4000',
                'direction' => 'up',
                'controls' => '1',
                'autostart' => '1',
                'pauseonhover' => '1',
                'width' => '400'
            ),
            'carouselsetting' => array(
                'cs_speed' => '400',
                'autoWidth'=> 'false',
                'cs_item' => array('4', '3', '2', '2', '1'),
                'cs_width' => '230',
                'cs_rtl' => 'false',
                'cs_controls' => 'true',
                'cs_auto' => 'false',
                'cs_loop' => 'true',
                'cs_pager' => 'true',
                'slideMargin' => '10',
                'slideMove' => '1'
            ),
            'timelinesetting' => array(
                'onecolumn' => 'false'
            )
        );
        $this->sboption = $attr['network'];
        $attr_ajax = json_encode($attr);
        
        // define theme
        if ( ! $theme = @$attr['theme'] )
            $theme = $setoption['setting']['theme'];
        $themeoption = $GLOBALS['themes'][$attr['theme']];

        // set some settings
        $label = $type.$id;
        if ( $type == 'feed' ) {
            $is_feed = true;
            $settingsection = (@$attr['carousel']) ? 'carouselsetting' : 'feedsetting';
            $filterlabel = '';
        } else {
            $is_feed = false;
            $settingsection = $type.'setting';
            $filterlabel = ' filter-label';
        }
        $is_timeline = ( $type == 'timeline' ) ? true : false;
        $is_wall = ( $type == 'wall' ) ? true : false;

        $typeoption = $type;
        if ( $is_feed ) {
            if ( @$attr['position'] ){
                if ( @$attr['position'] != 'normal' )
                    $typeoption = 'feed_sticky';
            }
            if (@$attr['carousel'])
                $typeoption = 'feed_carousel';
        }

        // main container id
        if ( @$label )
            $attr_id = ' id="timeline_'.$label.'"';
        $class = array('sboard');

        // merge shortcode and widget attributes with related default settings
        if ( ! @$setoption[$settingsection] )
            $setoption[$settingsection] = array();
        $this->attr = $attr = array_merge($setoption['setting'], $setoption[$settingsection], $attr);
        
    	// the array of feeds to get
    	$filters_order = explode(',', str_replace(' ', '', $setoption['setting']['filters_order']) );
        $this->feed_keys = ( ! empty($ajax_feed) && $ajax_feed != 'all' ) ? $ajax_feed : $filters_order;
        
    	if ( ! $results = $attr['results'] )
    		$results = 10;
    	if (@$args['liveresults'])
        if ( $results < $args['liveresults'] )
            $results = $args['liveresults'];
        if ($results > 100)
            $results = 100;
        $attr['results'] = $results;
            
        $attr['cache'] = (int)$attr['cache'];
        // set crawl time limit (some servers can not read a lot of feeds at the same time)
        $GLOBALS['crawled'] = 0;
        $crawl_limit = ($attr['cache'] == 0) ? 0 : (int)@$attr['crawl'];
        
        // Init cache
        $cache = new SimpleCache;
        $cache->debug_log = @$attr['debuglog'];
        
        if ( $is_feed ) {
            if (@$attr['carousel']){
                $class[] = 'sb-carousel';
                if (@$args['widget_id'])
                    $class[] = 'sb-widget';
            } else {
                $class[] = 'sb-widget';
            }
        } elseif ($is_wall) {
            $class[] = 'sb-wall';
        }

        // set the block height
        $block_height = (@$attr['height']) ? $attr['height'] : 400;

        // load layout
        include_once( SB_DIR . '/layout/'.$themeoption['layout'].'.php' );
        if ( $attr['theme'] != $themeoption['layout'] )
            $class[] = 'sb-'.$themeoption['layout'];

        $layoutclass = $themeoption['layout'].'Layout';
        $layoutobj = new $layoutclass;
        
        if ( ! $ajax_feed) {
        // do some styling stuffs
        $dotboard = "#timeline_$label.sboard";
        if ( @$themeoption['social_colors'] ) {
            $style = $layoutobj->create_colors( $themeoption['social_colors'], $filters_order, $type, $dotboard, $attr, @$themeoption[$typeoption] );
            
            foreach ($themeoption['social_colors'] as $colorKey => $colorVal) {
                if (@$colorVal && @$colorVal != 'transparent') {
                }
            }
        }
        
        if ($is_wall) {
            if (@$attr['itemwidth']) {
                $dotitem2 = '.sb-item';
                $itemwidth = $attr['itemwidth'];
            }
        } else {
            if (@$attr['carousel']) {
                if (@$attr['cs_width']) {
                    $dotitem2 = '.lslide';
                    $itemwidth = $attr['cs_width'];
                }
            }
        }
        if (@$itemwidth)
            $style["$dotboard $dotitem2"][] = 'width: '.$itemwidth.'px;';
        
        if ( $font_size = @$themeoption['font_size'] ) {
            $style["$dotboard, $dotboard a"][] = 'font-size: '.$font_size.'px';
            $style["$dotboard .sb-heading"][] = 'font-size: '.($font_size+1).'px !important';
        }
        
        if ( $is_feed && @$themeoption[$typeoption]['title_background_color'] )
        if ( $themeoption[$typeoption]['title_background_color'] != 'transparent') {
            $style["$dotboard .sb-heading, $dotboard .sb-opener"][] = 'background-color: '.$themeoption[$typeoption]['title_background_color'].' !important';
        }
        if ( $is_feed && @$themeoption[$typeoption]['title_color'] )
        if ( $themeoption[$typeoption]['title_color'] != 'transparent')
            $style["$dotboard .sb-heading"][] = 'color: '.$themeoption[$typeoption]['title_color'];
        
        if ( $is_feed )
            $csskey = "$dotboard .sb-content, $dotboard .toolbar";
        else
            $csskey = '#sb_'.$label;
            
        if ( @$themeoption[$typeoption]['background_color'] ) {
            if ( $themeoption[$typeoption]['background_color'] != 'transparent') {
                $bgexist = true;
                $style[$csskey][] = 'background-color: '.$themeoption[$typeoption]['background_color'];
            }
        }
        
        if ( @$themeoption[$typeoption]['item_background_color'] ) {
            if ( $themeoption[$typeoption]['item_background_color'] != 'transparent') {
                $style["$dotboard .sb-item"][] = 'background-color: '.$themeoption[$typeoption]['item_background_color'];
            }
        }
        
        if ( $is_timeline ) {
            $fontcsskey = "$dotboard .timeline-row";
        } else
            $fontcsskey = "$dotboard .sb-item";
            
        if (@$themeoption[$typeoption]['font_color'])
        if ($themeoption[$typeoption]['font_color'] != 'transparent') {
            $rgbColorVal = ss_hex2rgb($themeoption[$typeoption]['font_color']); // returns the rgb values separated by commas

            if ( $is_timeline ) {
                $style["$dotboard .timeline-row small"][] = 'color: '.$themeoption[$typeoption]['font_color'];
            }
            
            $style["$fontcsskey .sb-title a"][] = 'color: '.$themeoption[$typeoption]['font_color'];
            $style["$fontcsskey"][] = 'color: rgba('.$rgbColorVal.', 0.8)';
        }
        if (@$themeoption[$typeoption]['link_color'])
        if ($themeoption[$typeoption]['link_color'] != 'transparent') {
            $rgbColorVal = ss_hex2rgb($themeoption[$typeoption]['link_color']); // returns the rgb values separated by commas
            $style["$fontcsskey a"][] = 'color: '.$themeoption[$typeoption]['link_color'];
            $style["$fontcsskey a:visited"][] = 'color: rgba('.$rgbColorVal.', 0.8)';
        }

        if ( @$themeoption[$typeoption]['background_image'] ) {
            $bgexist = true;
            $cssbgkey = $csskey;
            if ( $is_feed )
                $cssbgkey = "$dotboard .sb-content";
            $style[$cssbgkey][] = 'background-image: url('.$themeoption[$typeoption]['background_image'].');background-repeat: repeat';
        }

        $location = null;
        if ( $is_feed ) {
            $class[] = @$attr['position'];
            if ( @$attr['position'] != 'normal' ) {
                $class[] = @$attr['location'];
                if ( ! @$attr['autoclose'] ) {
                    $class[] = 'open';
                    $active = ' active';
                }
                
                $locarr = explode('_', str_replace('sb-', '', @$attr['location']) );
                $location = $locarr[0];
            }
        }

        if (@$attr['carousel'] && @$attr['tabable'])
            unset($attr['tabable']);
            
        if (@$attr['tabable'])
            $class[] = 'tabable';
        
        if ( (@$attr['filters'] or @$attr['controls']) && !@$attr['carousel']) {
            $style[$dotboard.' .sb-content'][] = 'border-bottom-left-radius: 0 !important;border-bottom-right-radius: 0 !important';
        }
        if ( (@$attr['showheader'] || ($location == 'bottom' && ! @$attr['tabable']) ) && $is_feed) {
            $style[$dotboard.' .sb-content'][] = 'border-top: 0 !important;border-top-left-radius: 0 !important;border-top-right-radius: 0 !important';
        }
        if ( $location == 'left' )
            $style[$dotboard.' .sb-content'][] = 'border-top-left-radius: 0 !important';
        if ( $location == 'right' )
            $style[$dotboard.' .sb-content'][] = 'border-top-right-radius: 0 !important';
        
    	// set block border
        if ( @$themeoption[$typeoption]['border_color'] ) {
            if ( $themeoption[$typeoption]['border_color'] != 'transparent') {
                $bgexist = true;
                if ( $is_feed ) {
                    $style[$dotboard.' .toolbar'][] = 'border-top: 0 !important';
                }
                $style[$csskey][] = 'border: '.@$themeoption[$typeoption]['border_size'].'px solid '.$themeoption[$typeoption]['border_color'];
            }
        } else {
            if (@$attr['carousel'])
                $style[$dotboard.' .sb-content'][] = 'padding: 10px 0 5px 0;';
        }
        
        // set block padding if required
        if (@$bgexist) {
            $border_radius = @$themeoption[$typeoption]['border_radius'];
            if ( ! $is_feed ) {
                if ($border_radius == '')
                    $border_radius = 7;
            }
            if ($border_radius != '') {
                $radius = 'border-radius: '.$border_radius.'px;-moz-border-radius: '.$border_radius.'px;-webkit-border-radius: '.$border_radius.'px';
                if ( ! $is_feed ) {
                    $style['#sb_'.$label][] = $radius;
                } else {
                    if ($location == 'bottom') {
                        $radius = ': '.$border_radius.'px '.$border_radius.'px 0 0;';
                        $style["$dotboard .sb-content, $dotboard.sb-widget, $dotboard .sb-heading"][] = 'border-radius'.$radius.'-moz-border-radius'.$radius.'-webkit-border-radius'.$radius;
                    } else {
                        $style["$dotboard .sb-content, $dotboard.sb-widget"][] = $radius;
                        $style[$dotboard.' .toolbar'][] = 'border-radius: 0 0 '.$border_radius.'px '.$border_radius.'px;-moz-border-radius: 0 0 '.$border_radius.'px '.$border_radius.'px;-webkit-border-radius: 0 0 '.$border_radius.'px '.$border_radius.'px;';
                    }
                }
            }
            if ( $is_wall )
                $style['#sb_'.$label][] = 'padding: 10px';
        }
        
        if ($is_feed) {
            if (@$attr['width'] != '')
                $style["$dotboard"][] = 'width: '.$attr['width'].'px';
        }
        
        if ( @$attr['height'] != '' && ! $is_feed ) {
            $style[$csskey][] = 'height: '.$attr['height'].'px';
            if ( ! $is_feed ) {
                $style[$csskey][] = 'overflow: scroll';
                if ( $is_timeline )
                    $style[$csskey][] = 'padding-right: 0';
                $style[$dotboard][] = 'padding-bottom: 30px';
            }
        }
        } // end no ajax
        
        if ( @$theme ) {
            $class['theme'] = $attr['theme'];
        }
        
    	if ( ! $order = $attr['order'] )
            $order = 'date';
        
        $target = '';
        // nofollow links
        if (@$attr['nofollow'])
            $target .= ' rel="nofollow"';
        
        // open links in new window
        if (@$attr['links'])
            $target .= ' target="_blank"';
        $this->target = $layoutobj->target = $target;
        
        $output = '';
        if ( ! $ajax_feed)
            $output .= "\n<!-- PHP Social Stream By Axent Media -->\t";

        $GLOBALS['islive'] = false;
        if (@$attr['live']) {
            $GLOBALS['islive'] = true;
        }
        
        if ($GLOBALS['islive']) {
            // Live update need cache to be disabled
            $forceCrawl = true;
        } else {
            // If a cache time is set in the admin AND the "cache" folder is writeable, set up the cache.
            if ( $attr['cache'] > 0 && is_writable( SB_DIR . '/cache/' ) ) {
                $cache->cache_path = SB_DIR . '/cache/';
                $cache->cache_time = $attr['cache'] * 60;
        		$forceCrawl = false;
        	} else {
        		// cache is not enabled, call local class
                $forceCrawl = true;
        	}
        }

        // order counter
        $GLOBALS['order'] = 0;
        
        if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if (@$_SESSION["$label-temp"]) {
            	$_SESSION[$label] = $_SESSION["$label-temp"];
                $_SESSION["$label-temp"] = array();
            }
            if ($_REQUEST['action'] == "sb_liveupdate") {
                $_SESSION["$label-temp"] = @$_SESSION[$label];
                $_SESSION[$label] = array();
            }
        }
        else {
            $_SESSION[$label] = array();
            unset($_SESSION["$label-temp"]);
        }
        
        // Check which feeds are specified
        $feeds = array();
        foreach ( $this->feed_keys as $key ) {
            for ($i = 1; $i <= 5; $i++) {
        		if ( $keyitems = @$this->sboption[$key][$key.'_id_'.$i] ) {
                    foreach ($keyitems as $key2 => $eachkey) {
                        if ( (@$_REQUEST['action'] == "sb_loadmore" && @$_SESSION[$label]['loadcrawl']) && ! @$_SESSION[$label]['loadcrawl'][$key.$i.$key2])
                            $load_stop = true;
                        else
                            $load_stop = false;
                        if ( ! @$load_stop) {
                        if ( $eachkey != '') {
         			        if ( ! @$attr['tabable'] || $ajax_feed ) {
                                if ( $crawl_limit && $GLOBALS['crawled'] >= $crawl_limit )
                                    break;
                                if ( $feed_data = $this->get_feed( $key, $i, $key2, $eachkey, $results, $this->sboption[$key], $cache, $forceCrawl, $label ) ) {
                                    $feeds[$key][$i][$key2] = $feed_data;
                                    $filterItems[$key] = ($is_feed) ? '<span class="fa-hover sb-'.$key.$filterlabel.'" data-filter=".sb-'.$key.'"><i class="sb-micon sb-'.$key.'"></i></span>' : '<span class="fa-hover sb-'.$key.$filterlabel.'" data-filter=".sb-'.$key.'"><i class="sb-icon sb-'.$key.'"></i></span>';
                                }
                            } else {
                                $activeTab = '';
                                if ( @$attr['position'] == 'normal' || (@$attr['slide'] && ! @$attr['autoclose']) ) {
                                    if ( ! isset($fistTab) ) {
                                        if ( $feed_data = $this->get_feed( $key, $i, $key2, $eachkey, $results, $this->sboption[$key], $cache, $forceCrawl, $label ) ) {
                                            $feeds[$key][$i][$key2] = $feed_data;
                                        }
                                        $fistTab = true;
                                        $activeTab = ' active';
                                    }
                                }
                                
                                $fi = '
                    			<li class="'.$key.@$activeTab.'" data-feed="'.$key.'">';
                                if (@$attr['position'] == 'normal') {
                                    $fi .= '
                                    <span><i class="sb-icon sb-'.$key.'"></i></span>';
                                } else {
                                $fi .= '
                                    <i class="sb-icon sb-'.$key.'"></i>';
                                    if ( $location != 'bottom' )
                                        $fi .= ' <span>'.ucfirst($key).'</span>';
                                }
                    			$fi .= '</li>';
                                $filterItems[$key] = $fi;
                            }
                        }
                        }
                    }
                }
            }
        }

        // set timeline style class
        if ( $is_timeline ) {
            $class[] = ($attr['onecolumn'] == 'true') ? 'timeline onecol' : 'timeline';
            $class[] = 'animated';
        }
        
        if ( ! $ajax_feed) {
            if (@$attr['add_files']) {
                // add css files
                $output .= '<link href="'.SB_PATH . 'public/css/colorbox/light/colorbox.css" rel="stylesheet" type="text/css" />';
                $output .= '<link href="'.SB_PATH . 'public/css/timeline-styles.css" rel="stylesheet" type="text/css" />';
                $output .= '<link href="'.SB_PATH . 'public/css/lightslider.css" rel="stylesheet" type="text/css" />';
                $output .= '<link href="'.SB_PATH . 'public/css/styles.css" rel="stylesheet" type="text/css" />';

                // add js files
                $output .= '<script type="text/javascript" src="'.SB_PATH . 'public/js/jquery-1.11.3.min.js"></script>';
                $output .= '<script type="text/javascript" src="'.SB_PATH . 'public/js/jquery.colorbox-min.js"></script>';
                if ( $is_timeline ) {
                    $output .= '<script type="text/javascript" src="'.SB_PATH . 'public/js/timeline.js"></script>';
                } else {
                    if ( $is_feed ) {
                        if (@$attr['carousel'])
                            $output .= '<script type="text/javascript" src="'.SB_PATH . 'public/js/lightslider.min.js"></script>';
                        else
                            $output .= '<script type="text/javascript" src="'.SB_PATH . 'public/js/jquery.newsTicker.min.js"></script>';
                    } else {
                        $output .= '<script type="text/javascript" src="'.SB_PATH . 'public/js/freewall.min.js"></script>';
                    }
                }
            }

            if ( @$style ) {
                $output .= '<style type="text/css">';
                if ( @$themeoption['custom_css'] )
                    $output .= $themeoption['custom_css']."\n";
                foreach ($style as $stKey => $stItem) {
                    $output .= $stKey.'{'.implode(';', $stItem).'}';
                }
                $output .= '</style>';
            }

            if ($is_wall || $is_timeline)
                $output .= '<div id="sb_'.$label.'">';
                
            if ( @$attr['filters'] && ! $is_feed && (@$attr['position'] == 'normal' || ! $is_timeline && ! @$attr['tabable'] ) && @$filterItems ) {
            $output .= '
        		<div class="filter-items">
                    <span class="fa-hover filter-label active" title="'.ss_lang( 'show_all' ).'"><i class="sb-icon sb-ellipsis-h"></i></span>
                    '.implode("\n", $filterItems).'
        		</div>';
            }

            $output .= '<div' . @$attr_id . ' class="' . @implode(' ', $class) . '" data-columns>' . "\n";
            if ( $is_feed ) {
                if (@$attr['tabable']) {
                        $minitabs = ( count($filterItems) > 5 ) ? ' minitabs' : '';
                        $output .= '
                        <div class="sb-tabs'.$minitabs.'">
                    		<ul class="sticky" data-nonce="'.ss_nonce_create( 'tabable', $label ).'">
                            '.implode("\n", $filterItems).'
                    		</ul>
                    	</div>';
                }
                if ( $is_feed ) {
                    if ( ! @$attr['tabable'] && @$attr['slide'] ) {
                        if ( $location == 'left' || $location == 'right' ) {
                            $opener_image = (@$themeoption[$typeoption]['opener_image']) ? $themeoption[$typeoption]['opener_image'] : SB_PATH.'public/img/opener.png';
                            $output .= '<div class="sb-opener'.@$active.'" title="'.@$attr['label'].'"><img src="'.$opener_image.'" alt="" /></div>';
                        } else {
                            $upicon = '<i class="sb-arrow"></i>';
                        }
                    }
                    if ( @$attr['showheader'] || ($location == 'bottom' && ! @$attr['tabable']) )
                        $output .= '<div class="sb-heading'.@$active.'">'.@$attr['label'].@$upicon.'</div>';
                }
                
                $content_style = (!@$attr['carousel']) ? ' style="height: '.$block_height.'px"' : '';
                $output .= '<div class="sb-content"'.$content_style.'>';
                $output .= '<ul id="ticker_'.$label.'">';
            }
        }

        if ( ! empty($feeds) ) {
            // Loop through the items in the combined feed
            foreach ( $feeds as $feed_class => $feeditem ) {
                foreach ($feeditem as $i => $feeds2) {
                foreach ($feeds2 as $ifeed => $feed) {
                $inner = '';
                if ( $feed_class == 'facebook' ) {
                    if (@$feed) {
                        $facebook_output = ( ! empty($this->sboption['facebook']['facebook_output']) ) ? ss_explode($this->sboption['facebook']['facebook_output']) : array('title' => true, 'thumb' => true, 'text' => true, 'comments' => true, 'likes' => true, 'user' => true, 'share' => true, 'info' => true);
                        $iframe = (@$this->sboption['facebook']['facebook_iframe']) ? ' class="iframe"' : '';

                    foreach ($feed as $data) {
                    if (@$data) {
                    foreach ($data as $entry) {
                        $url = '';
                        $iframe2 = null;
                        $link = @$entry->link;
                        if ( ! $link) {
                            $idparts = @explode('_', @$entry->id);
                            if ( @count($idparts) > 1 )
                                $link = 'https://www.facebook.com/'.$idparts[0].'/posts/'.$idparts[1];
                            else
                                $link = 'https://www.facebook.com/'.@$entry->from->id.'/posts/'.@$entry->id;
                        }
                        if ( ! $link)
                            $link = @$entry->source;
                        
                        if ( $this->make_remove($link) ) {
                        
                        // body text
                        $content = array();
                        if (@$entry->message)
                            $content[] = $entry->message;
                        if (@$entry->description)
                            $content[] = $entry->description;
                        if (@$entry->story)
                            $content[] = $entry->story;
                        $content = implode("\n\r", $content);
                        $text = (@$attr['words']) ? $this->word_limiter($content, $link) : $this->format_text($content);
                        
                        // comments
                        $count = 0;
                        $comments_data = '';
                        if ( ! empty($entry->comments->data) ) {
                            if ( ! $comments_count = @$this->sboption['facebook']['facebook_comments'] )
                                $comments_count = 3;
                            foreach ( $entry->comments->data as $comment ) {
                                $count++;
                                $comment_message = (@$attr['commentwords']) ? $this->word_limiter(nl2br($comment->message), @$link, true) : nl2br($comment->message);
                                $comments_data .= '<span class="meta item-comments"><img src="http://graph.facebook.com/' . $comment->from->id . '/picture?type=square" alt="" /><a href="https://www.facebook.com/' . $comment->from->id . '"'.$target.'>' . $comment->from->name . '</a> ' . $comment_message . '</span>';
                                if ( $count >= $comments_count ) break;
                            }
                        }
                        // likes
                        $count = 0;
                        $likes_data = '';
                        if ( ! empty($entry->likes->data) ) {
                            if ( ! @$likes_count = @$this->sboption['facebook']['facebook_likes'] )
                                $likes_count = 5;
                            foreach ( $entry->likes->data as $like ) {
                                $count++;
                                $likes_data .= '<img src="http://graph.facebook.com/' . $like->id . '/picture?type=square" title="' . $like->name . '" alt="">';
                                if ( $count >= $likes_count ) break;
                            }
                        }
                        
                        $meta = '';
                        if ($comments_data || $likes_data) {
                            $meta .= '
                            <span class="sb-meta">';
                            if (@$facebook_output['comments'] && $comments_data)
                                $meta .= '
                                <span class="meta">
                                    <span class="comments"><i class="sb-bico sb-comments"></i> '.ucfirst( ss_lang( 'comments' ) ).'</span>
                                </span>
                                ' . $comments_data;
                            if (@$facebook_output['likes'] && $likes_data)
                            $meta .= '
                                <span class="meta">
                                    <span class="likes"><i class="sb-bico sb-star"></i> '.ucfirst( ss_lang( 'likes' ) ).'</span>
                                </span>
                                <span class="meta item-likes">
                                    ' . $likes_data . '
                                </span>';
                            $meta .= '
                            </span>';
                        }
                        
                        if (@$entry->type == 'video') {
                            $iframe2 = $iframe;
                            $url = @$entry->source;
                        }
                        
                        $image_width = (@$this->sboption['facebook']['facebook_image_width']) ? $this->sboption['facebook']['facebook_image_width'] : '300';
                        $source = @$entry->picture;
                        if ( ! empty($entry->images) ) {
                            if ($image_width) {
                                foreach ($entry->images as $image) {
                                    if ($image_width > $image->width && $image->width > $image_width - 80) {
                                        $source = $image->source;
                                        break;
                                    }
                                }
                            }
                        } else {
                            if ($image_width > 180) {
                                if (@$entry->full_picture) {
                                    $source = $entry->full_picture;
                                } else {
                                    if ( $object_id = @$entry->object_id )
                                        $source = 'https://graph.facebook.com/'.$object_id.'/picture?type=normal';
                                }
                            }
                        }
                        
                        $thetime = (@$entry->created_time) ? $entry->created_time : $entry->updated_time;
                        $final[$this->make_timestr($thetime, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'title' => (@$entry->name) ? '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($entry->name) : $entry->name) . '</a>' : '',
                        'thumb' => (@$source) ? $source : '',
                        'thumburl' => $url,
                        'text' => @$text,
                        'meta' => @$meta,
                        'url' => @$link,
                        'iframe' => $iframe2,
                        'date' => $thetime,
                        'user' => array(
                            'name' => @$entry->from->name,
                            'url' => 'https://www.facebook.com/' . $entry->from->id,
                            'image' => 'http://graph.facebook.com/' . $entry->from->id . '/picture?type=square',
                            'status' => (@$entry->status_type) ? ucfirst( str_replace('_', ' ', $entry->status_type) ) : ''
                            ),
                        'type' => ($i == 2) ? 'image' : 'pencil',
                        'icon' => array(@$themeoption['social_icons'][0], ($i == 2) ? @$themeoption['type_icons'][4] : @$themeoption['type_icons'][0] )
                        ), $attr, $facebook_output);
                        }
                    } // end foreach
                    
                    // facebook get last item date
                    $loadcrawl[$feed_class.$i.$ifeed] = strtotime($thetime)-1;
                    
                    } // end $data
                    }
                    }
                }
        		elseif ( $feed_class == 'twitter' ) {
                    if (@$feed) {
                        if ($i == 3)
                            $feed = $feed->statuses;

                        $twitter_output = ( ! empty($this->sboption['twitter']['twitter_output']) ) ? ss_explode($this->sboption['twitter']['twitter_output']) : array('thumb' => true, 'text' => true, 'user' => true, 'share' => true, 'info' => true);
                        
                    foreach ( $feed as $data ) {
                        if ( isset($data->created_at) ) {
                        if ($_SESSION[$label]['loadcrawl'][$feed_class.$i.$ifeed] == $data->id_str)
                            continue;
                        $link = 'https://twitter.com/' . $data->user->screen_name . '/status/' . $data->id_str;
                        if ( $this->make_remove($link) ) {
                        
                        $text = (@$attr['words']) ? $this->word_limiter($data->text) : @$this->format_text($data->text);
                        // Add links to all @ mentions
                        $text = preg_replace('/@([^\s]+)/', '<a href="http://twitter.com/$1"'.$target.'>@$1</a>', $text );
                        // Add links to all hash tags
                        $text = preg_replace('/#([^\s]+)/', '<a href="http://twitter.com/search/%23$1"'.$target.'>#$1</a>', $text );
                        
                        $twitter_images = (@$this->sboption['twitter']['twitter_images']) ? $this->sboption['twitter']['twitter_images'] : 'small';
                        
                        $final[$this->make_timestr($data->created_at, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'thumb' => (@$data->entities->media[0]) ? $data->entities->media[0]->media_url . ':' . $twitter_images : '',
                        'thumburl' => (@$data->entities->media[0]) ? $data->entities->media[0]->url : '',
                        'text' => $text,
                        'share' => (@$twitter_output['share']) ? '
                        <span class="sb-share">
                            <span class="meta">
                                <a href="https://twitter.com/intent/tweet?in_reply_to=' . $data->id_str . '&via=' . $data->user->screen_name . '"'.$target.'><i class="sb-bico sb-reply"></i></a>
                                <a href="https://twitter.com/intent/retweet?tweet_id=' . $data->id_str . '&via=' . $data->user->screen_name . '"'.$target.'><i class="sb-bico sb-retweet"></i> ' . $data->retweet_count . '</a>
                                <a href="https://twitter.com/intent/favorite?tweet_id=' . $data->id_str . '"'.$target.'><i class="sb-bico sb-star-o"></i> ' . $data->favorite_count . '</a>
                            </span>
                        </span>' : null,
                        'url' => @$link,
                        'date' => $data->created_at,
                        'user' => array(
                            'name' => '@'.$data->user->screen_name,
                            'url' => 'http://twitter.com/'.$data->user->screen_name,
                            'title' => $data->user->name,
                            'image' => $data->user->profile_image_url
                            ),
                        'type' => 'pencil',
                        'icon' => array(@$themeoption['social_icons'][1], @$themeoption['type_icons'][0])
                        ), $attr, $twitter_output);
                        }
                        }
                    } // end foreach
                    
                    // twitter get last id
                    $loadcrawl[$feed_class.$i.$ifeed] = @$data->id_str;
                    }
        		}
                elseif ( $feed_class == 'google' ) {
                    $keyTypes = array( 'note' => array('pencil', 0), 'article' => array('edit', 1), 'activity' => array('quote-right', 2), 'photo' => array('image', 5), 'video' => array('video-camera', 6) );
                    $google_output = ( ! empty($this->sboption['google']['google_output']) ) ? ss_explode($this->sboption['google']['google_output']) : array('title' => true, 'thumb' => true, 'text' => true, 'stat' => true, 'user' => true, 'share' => true, 'info' => true);
                    
                    $iframe = ' class="iframe"';
                    if ( isset($this->sboption['google']['google_iframe']) )
                        if ( ! $this->sboption['google']['google_iframe'])
                            $iframe = '';
                    
                    // google next page
                    $loadcrawl[$feed_class.$i.$ifeed] = @$feed->nextPageToken;
                    
                    if (@$feed->items) {
                    foreach ($feed->items as $item) {
                        $url = '';
                        $iframe2 = null;
                        $link = @$item->url;
                        if ( $this->make_remove($link) ) {
                            // get text
                            if ($attachments = @$item->object->attachments[0]) {
                                if (@$attachments->content)
                                    $content = (@$attr['words']) ? $this->word_limiter($attachments->content, $link) : $this->format_text($attachments->content);
                                $text = '<span class="sb-title"><a href="' . $attachments->url . '"'.$target.'>'.$attachments->displayName.'</a></span>'.@$content;
                                if (@$attachments->objectType == 'video') {
                                    $iframe2 = $iframe;
                                    if (@$attachments->embed) {
                                        $url = $attachments->embed->url;
                                    }
                                }
                            }
                            else
                                $text = (@$attr['words']) ? $this->word_limiter($item->object->content, $link) : $this->format_text($item->object->content);
                                
                        $image_url = @$item->object->attachments[0]->image->url;
                        $final[$this->make_timestr($item->updated, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'thumb' => (@$image_url) ? $image_url : '',
                        'thumburl' => $url,
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                        'text' => $text,
                        'iframe' => $iframe2,
                        'meta' => (@$google_output['stat']) ? '
                        <span class="sb-text">
                            <span class="meta">
                                <span class="plusones">+1s ' . $item->object->plusoners->totalItems . '</span>
                                <span class="shares"><i class="sb-bico sb-users"></i> ' . $item->object->resharers->totalItems . '</span>
                                <span class="comments"><i class="sb-bico sb-comment"></i> ' . $item->object->replies->totalItems . '</span>
                            </span>
                        </span>' : null,
                        'url' => @$link,
                        'date' => @$item->published,
                        'user' => array(
                            'name' => $item->actor->displayName,
                            'url' => $item->actor->url,
                            'image' => @$item->actor->image->url
                            ),
                        'type' => $keyTypes[$item->object->objectType][0],
                        'icon' => array(@$themeoption['social_icons'][2], @$themeoption['type_icons'][$keyTypes[$item->object->objectType][1]])
                        ), $attr, $google_output);
                        }
                    } // end foreach
                    }
                }
                elseif ( $feed_class == 'tumblr' ) {
                    $keyTypes = array( 'text' => array('pencil', 0), 'quote' => array('quote-right', 2), 'link' => array('link', 3), 'answer' => array('reply', 1), 'video' => array('video-camera', 6), 'audio' => array('youtube-play', 7), 'photo' => array('image', 5), 'chat' => array('comment', 9) );
                    $embed = @$this->sboption['tumblr']['tumblr_embed'];
                    
                    // tumblr next page start
                    $total_posts = @$feed->response->total_posts;
                    $loadcrawl[$feed_class.$i.$ifeed] = (@$_SESSION[$label]['loadcrawl'][$feed_class.$i.$ifeed]) ? $_SESSION[$label]['loadcrawl'][$feed_class.$i.$ifeed] + $results : $results;
                    $tumblr_video = (@$this->sboption['tumblr']['tumblr_video']) ? $this->sboption['tumblr']['tumblr_video'] : '250';
                    
                    // blog info
                    $blog = $feed->response->blog;
                    
                    if (@$feed->response->posts) {
                        $tumblr_output = (@$this->sboption['tumblr']['tumblr_output']) ? ss_explode($this->sboption['tumblr']['tumblr_output']) : array('title' => true, 'thumb' => true, 'text' => true, 'user' => true, 'share' => true, 'info' => true, 'tags' => false);
                    
                    foreach ($feed->response->posts as $item) {
                        $title = $thumb = $text = $object = $tags = $url = '';
                        $embed2 = null;
                        
                        $link = @$item->post_url;
                        if ( $this->make_remove($link) ) {
                        
                        // tags
                        if ( @$tumblr_output['tags'] ) {
                            if ( @$item->tags ) {
                                $tags = implode(', ', $item->tags);
                            }
                        }
                        
                        if ( @$item->title ) {
                            $title = '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>';
                        }
                        
                        // set image
                        if ($photoItem = @$item->photos[0]) {
                            if (@$photoItem->alt_sizes) {
                                foreach ($photoItem->alt_sizes as $photo) {
                                    $tumblr_thumb = (@$this->sboption['tumblr']['tumblr_thumb']) ? $this->sboption['tumblr']['tumblr_thumb'] : '250';
                                    if ($photo->width == $tumblr_thumb)
                                        $thumb = $photo->url;
                                }
                            }
                        }
                        
                        if ( $item->type == 'photo') {
                            $body = @$item->caption;
                        }
                        elseif ( $item->type == 'video') {
                            $embed2 = $embed;
                            // set player
                            if ($embed) {
                                if (@$item->player) {
                                    foreach ($item->player as $player) {
                                        if ($player->width == $tumblr_video)
                                            $object = $player->embed_code;
                                    }
                                }
                            } else {
                                $url = @$item->permalink_url;
                                if (@$item->thumbnail_url)
                                    $thumb = $item->thumbnail_url;
                            }
                            $body = @$item->caption;
                        }
                        elseif ( $item->type == 'audio') {
                            $tit = @$item->artist . ' - ' . @$item->album . ' - ' . @$item->id3_title;
                            $title = '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($tit) : $tit) . '</a>';
                            $thumb = @$item->player;
                            $body = @$item->caption;
                        }
                        elseif ( $item->type == 'link') {
                            $title = '<a href="' . $item->url . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>';
                            $url = $item->url;
                            if (@$item->excerpt)
                                $excerpt = $item->excerpt.'<br />';
                            $body = @$excerpt.@$item->description;
                            if ( ! @$thumb)
                                $thumb = @$item->link_image;
                        }
                        elseif ( $item->type == 'answer') {
                            $body = @$item->question.'<br />'.@$item->answer;
                        }
                        elseif ( $item->type == 'quote') {
                            if (@$item->source)
                                $source = $item->source;
                            $text = '<span class="sb-title">'.@$item->text.'</span>'.@$source;
                        }
                        elseif ( $item->type == 'chat') {
                            $body = @$item->body;
                        }
                        // type = text
                        else {
                            if ( @$item->body )
                                $body = $item->body;
                            
                            // find img
                            if ( ! @$thumb) {
                                $thumb = '';
                                $image = array();
                                preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $body, $image);
                                if ( ! empty($image) )
                                    $thumb = $image[1];
                            }
                        }
                        
                        if ( ! empty($body) ) {
                            if ( ! @$text )
                                $text = (@$attr['words']) ? $this->word_limiter($body, $link) : $this->format_text($body);
                        }
                        
                        $final[$this->make_timestr($item->timestamp, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'title' => @$title,
                        'thumb' => @$thumb,
                        'thumburl' => $url,
                        'object' => @$object,
                        'iframe' => $embed2,
                        'text' => @$text,
                        'tags' => @$tags,
                        'url' => @$link,
                        'date' => $item->date,
                        'user' => array(
                            'name' => $blog->name,
                            'title' => $blog->title,
                            'url' => $blog->url,
                            'image' => 'http://api.tumblr.com/v2/blog/'.$blog->name.'.tumblr.com/avatar/64'
                            ),
                        'type' => $keyTypes[$item->type][0],
                        'icon' => array(@$themeoption['social_icons'][3], @$themeoption['type_icons'][$keyTypes[$item->type][1]])
                        ), $attr, $tumblr_output);
                        }
                    }
                    }
                }
                elseif ( $feed_class == 'delicious' ) {
                    if (@$feed) {
                        $delicious_output = (@$this->sboption['delicious']['delicious_output']) ? ss_explode($this->sboption['delicious']['delicious_output']) : array( 'title' => true, 'text' => true, 'user' => true, 'share' => true, 'info' => true, 'tags' => false );
                    foreach ($feed as $item) {
                        $link = @$item->u;
                        if ( $this->make_remove($link) ) {
                        // tags
                        $tags = '';
                        if ( @$delicious_output['tags'] ) {
                            $tags = '';
                            if ( @$item->t ) {
                                $tags = implode(', ', $item->t);
                            }
                        }
                        $final[$this->make_timestr($item->dt, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->d) : $item->d) . '</a>',
                        'text' => (@$attr['words']) ? $this->word_limiter(@$item->n, $link) : @$item->n,
                        'tags' => $tags,
                        'url' => $link,
                        'date' => $item->dt,
                        'user' => array('name' => $item->a),
                        'type' => 'pencil',
                        'icon' => array(@$themeoption['social_icons'][4], @$themeoption['type_icons'][0])
                        ), $attr, $delicious_output);
                        }
                    }
                    }
                }
                elseif ( $feed_class == 'pinterest' ) {
                    $pinterest_output = (@$this->sboption['pinterest']['pinterest_output']) ? ss_explode($this->sboption['pinterest']['pinterest_output']) : array('title' => true, 'thumb' => true, 'text' => true, 'user' => true, 'share' => true, 'info' => true);
                    $fcount = 0;
                    $channel = @$feed->channel;
                    if (@$channel->item)
                    foreach($channel->item as $item) {
                        $link = @$item->link;
                        if ( $this->make_remove($link) ) {
                        $fcount++;
        
                        if (@$item->category) {
                            foreach($item->category as $category) {
                                $cats[] = (string) $category;
                            }
                        }
                        
                        // fix the links in description
                        $pattern = "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/";
                        if (preg_match($pattern, $item->description, $url)) {
                            $description = preg_replace($pattern, "https://www.pinterest.com$url[0]", $item->description);
                        }
                        
                        // find img
                        $thumb = '';
                        $image = array();
                        preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $description, $image);
                        if ( ! empty($image) )
                            $thumb = $image[1];

                        $final[$this->make_timestr($item->pubDate, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                        'text' => (string) (@$attr['words']) ? $this->word_limiter($description, $link) : $description,
                        'thumb' => $thumb,
                        'tags' => @implode(', ', $cats),
                        'url' => $link,
                        'date' => $item->pubDate,
                        'user' => array(
                            'name' => $channel->title,
                            'url' => $channel->link
                            ),
                        'type' => 'pencil',
                        'icon' => array(@$themeoption['social_icons'][5], @$themeoption['type_icons'][0])
                        ), $attr, $pinterest_output);
                        
                        if ( $fcount >= $results ) break;
                        }
                    }
                }
                elseif ( $feed_class == 'flickr' ) {
                    // flickr next page
                    $loadcrawl[$feed_class.$i.$ifeed] = @$feed->photos->page+1;
                    
                    if (@$feed->photos->photo) {
                        $flickr_output = (@$this->sboption['flickr']['flickr_output']) ? ss_explode($this->sboption['flickr']['flickr_output']) : array( 'title' => true, 'thumb' => true, 'text' => true, 'user' => true, 'share' => true, 'info' => true, 'tags' => false );
                        
                    foreach ($feed->photos->photo as $media) {
                        $link = 'https://flickr.com/photos/' . $media->owner . '/' . $media->id;
                        if ( $this->make_remove($link) ) {
                        // tags
                        if ( @$flickr_output['tags'] ) {
                            if ( @$media->tags ) {
                                $tags = $media->tags;
                            }
                        }
                        $text = '';
                        if (@$attr['carousel'])
                            $text = (@$attr['words']) ? $this->word_limiter($media->title, $link) : $media->title;
                        
                        $flickr_thumb = (@$this->sboption['flickr']['flickr_thumb']) ? $this->sboption['flickr']['flickr_thumb'] : 'm';
                        $image = 'https://farm' . $media->farm . '.staticflickr.com/' . $media->server . '/' . $media->id . '_' . $media->secret . '_' . $flickr_thumb . '.jpg';
                        $author_icon = 'https://farm' . $media->iconfarm . '.staticflickr.com/' . $media->iconserver . '/buddyicons/' . $media->owner . '_s.jpg';
                        
                        $mediadate = (@$media->dateadded) ? $media->dateadded : $media->dateupload;
                        $final[$this->make_timestr($mediadate, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'thumb' => $image,
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($media->title) : $media->title) . '</a>',
                        'text' => $text,
                        'tags' => @$tags,
                        'url' => $link,
                        'date' => $media->datetaken,
                        'user' => array(
                            'name' => @$media->ownername,
                            'url' => 'https://www.flickr.com/people/' . $media->owner . '/',
                            'image' => $author_icon
                            ),
                        'type' => 'image',
                        'icon' => array(@$themeoption['social_icons'][6], @$themeoption['type_icons'][5])
                        ), $attr, $flickr_output);
                        }
                    }
                    }
                }
                elseif ( $feed_class == 'instagram' ) {
                    $keyTypes = array( 'image' => array('camera', 5), 'video' => array('video-camera', 6) );
                    $instagram_output = (@$this->sboption['instagram']['instagram_output']) ? ss_explode($this->sboption['instagram']['instagram_output']) : array( 'title' => true, 'thumb' => true, 'text' => true, 'comments' => true, 'likes' => true, 'user' => true, 'share' => true, 'info' => true, 'tags' => false );
                    $iframe = (@$this->sboption['instagram']['instagram_iframe']) ? ' class="iframe"' : '';
                    
                    // instagram next page
                    if ($i != 4) {
                        if ($i == 2) {
                            $next_id = @$feed->pagination->next_max_tag_id;
                        } else {
                            $next_id = @$feed->pagination->next_max_id;
                        }
                        $loadcrawl[$feed_class.$i.$ifeed] = $next_id;
                    }
                    
                    if (@$feed->data) {
                    foreach ($feed->data as $item) {
                        $link = @$item->link;
                        if ( $this->make_remove($link) ) {
                        $thumb = '';
                        $instagram_images = (@$this->sboption['instagram']['instagram_images']) ? $this->sboption['instagram']['instagram_images'] : 'low_resolution';
                        if (@$item->images)
                            $thumb = $item->images->{$instagram_images}->url;
                        
                        $url = $link;
                        if (@$item->type == 'video' && $iframe) {
                            $instagram_videos = (@$this->sboption['section_instagram']['instagram_videos']) ? $this->sboption['section_instagram']['instagram_videos'] : 'low_resolution';
                            $url = $item->videos->{$instagram_videos}->url;
                            $iframe2 = $iframe;
                        } else {
                            $iframe2 = null;
                        }
                        
                        // tags
                        $tags = '';
                        if ( @$instagram_output['tags'] ) {
                            if ( @$item->tags ) {
                                $tags = implode(', ', $item->tags);
                            }
                        }
                        
                        // comments
                        $count = 0;
                        $comments_data = '';
                        if ( ! empty($item->comments->data) ) {
                            if ( ! $comments_count = $this->sboption['instagram']['instagram_comments'] )
                                $comments_count = 3;
                            foreach ( $item->comments->data as $comment ) {
                                $count++;
                                $comment_message = (@$attr['commentwords']) ? $this->word_limiter($comment->text, $link, true) : $comment->text;
                                $comments_data .= '<span class="meta item-comments"><img src="' . $comment->from->profile_picture . '" alt=""><a href="http://instagram.com/' . $comment->from->username . '"'.$target.'>' . $comment->from->full_name . '</a> ' . $comment_message . '</span>';
                                if ( $count >= $comments_count ) break;
                            }
                        }
                        // likes
                        $count = 0;
                        $likes_data = '';
                        if ( ! empty($item->likes->data) ) {
                            if ( ! $likes_count = $this->sboption['instagram']['instagram_likes'] )
                                $likes_count = 5;
                            foreach ( $item->likes->data as $like ) {
                                $count++;
                                $likes_data .= '<img src="' . $like->profile_picture . '" title="' . $like->full_name . '" alt="">';
                                if ( $count >= $likes_count ) break;
                            }
                        }
                        
                        $meta = '';
                        if ($comments_data || $likes_data) {
                            $meta .= '
                            <span class="sb-meta">';
                            if (@$instagram_output['comments'] && $comments_data)
                                $meta .= '
                                    <span class="meta">
                                        <span class="comments"><i class="sb-bico sb-comments"></i> ' . $item->comments->count . ' '.ss_lang( 'comments' ).'</span>
                                    </span>
                                    ' . $comments_data;
                            if (@$instagram_output['likes'] && $likes_data)
                            $meta .= '
                                <span class="meta">
                                    <span class="likes"><i class="sb-bico sb-star"></i> ' . $item->likes->count . ' '.ss_lang( 'likes' ).'</span>
                                </span>
                                <span class="meta item-likes">
                                    ' . $likes_data . '
                                </span>';
                            $meta .= '
                            </span>';
                        }
                        
                        $text = (@$attr['words']) ? $this->word_limiter(@$item->caption->text, $link) : @$this->format_text($item->caption->text);
                        // Add links to all hash tags
                        if ( @$item->tags ) {
                            foreach ($item->tags as $hashtag) {
                                $text = str_ireplace('#'.$hashtag, '<a href="https://instagram.com/explore/tags/'.$hashtag.'/"'.$target.'>#'.$hashtag.'</a>', $text);
                            }
                        }
                        
                        $final[$this->make_timestr($item->created_time, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'thumb' => @$thumb,
                        'thumburl' => $url,
                        'iframe' => $iframe2,
                        'text' => $text,
                        'meta' => @$meta,
                        'tags' => $tags,
                        'url' => $link,
                        'date' => $item->created_time,
                        'user' => array(
                            'name' => $item->user->username,
                            'title' => @$item->user->full_name,
                            'url' => 'https://instagram.com/'.$item->user->username.'/',
                            'image' => @$item->user->profile_picture
                            ),
                        'type' => @$keyTypes[$item->type][0],
                        'icon' => array(@$themeoption['social_icons'][7], @$themeoption['type_icons'][$keyTypes[$item->type][1]])
                        ), $attr, $instagram_output);
                        }
                    } // end foreach
                    
                    // next page timestamp only for /media/search
                    if ( $i == 4 && ! @$next_id)
                        $loadcrawl[$feed_class.$i.$ifeed] = @$item->created_time;
                    }
                }
                elseif ( $feed_class == 'youtube' ) {
                    $iframe = ' class="iframe"';
                    if ( isset($this->sboption['youtube']['youtube_iframe']) )
                        if ( ! $this->sboption['youtube']['youtube_iframe'])
                            $iframe = '';
                    
                    // youtube next page
                    $loadcrawl[$feed_class.$i.$ifeed] = @$feed->nextPageToken;
                    
                    $youtube_output = (@$this->sboption['youtube']['youtube_output']) ? ss_explode($this->sboption['youtube']['youtube_output']) : array('title' => true, 'thumb' => true, 'text' => true, 'user' => true, 'share' => true, 'info' => true);
                    
                    if (@$feed->items)
                    foreach ($feed->items as $item) {
                        $watchID = ($i == 3) ? @$item->id->videoId : @$item->snippet->resourceId->videoId;
                        $link = 'http://www.youtube.com/watch?v='.$watchID;
                        $snippet = $item->snippet;
                        if ( $this->make_remove($link) ) {
                        $dateof = @$snippet->publishedAt;
                        $title = @$snippet->title;
                        $text = @$snippet->description;
                        $text = (@$attr['words']) ? $this->word_limiter(@$text, $link) : @$this->format_text($text);

                        $thumb = '';
                        $url = $link;
                        if ($iframe) {
                            $url = 'http://www.youtube.com/embed/' . $watchID . '?rel=0&wmode=transparent';
                        }
                        $youtube_thumb = (@$this->sboption['youtube']['youtube_thumb']) ? $this->sboption['youtube']['youtube_thumb'] : 'medium';
                        $thumbnail = @$snippet->thumbnails->{$youtube_thumb};
                        if ( ! $thumbnail )
                            $thumbnail = @$snippet->thumbnails->{'medium'};
                        $thumb = @$thumbnail->url;
                        
                        $final[$this->make_timestr($dateof, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'thumb' => $thumb,
                        'thumburl' => $url,
                        'iframe' => $iframe,
                        'title' => '<a href="' . $link . '"'.$iframe.$target.'>' . (@$attr['titles'] ? $this->title_limiter($title) : $title) . '</a>',
                        'text' => $text,
                        'url' => $link,
                        'date' => $dateof,
                        'user' => array(
                            'name' => $snippet->channelTitle,
                            'url' => 'https://www.youtube.com/channel/'.$snippet->channelId
                            ),
                        'type' => 'youtube-play',
                        'icon' => array(@$themeoption['social_icons'][8], @$themeoption['type_icons'][6])
                        ), $attr, $youtube_output);
                        }
                    }
                }
                elseif ( $feed_class == 'vimeo' ) {
                    $iframe = ' class="iframe"';
                    if ( isset($this->sboption['vimeo']['vimeo_iframe']) )
                        if ( ! $this->sboption['vimeo']['vimeo_iframe'])
                            $iframe = '';
                            
                    $vimeo_output = ( ! empty($this->sboption['vimeo']['vimeo_output']) ) ? ss_explode($this->sboption['vimeo']['vimeo_output']) : array('title' => true, 'thumb' => true, 'text' => true, 'user' => true, 'share' => true, 'info' => true);
                    
                    if (@$feed) {
                    foreach ($feed as $dataKey => $data) {
                        foreach ($data as $item) {
                            $link = @$item->url;
                            if ( $this->make_remove($link) ) {
                                $thumb = '';
                            if ($dataKey == 'channels' || $dataKey == 'groups') {
                                $datetime = @$item->created_on;
                                $user_image = $thumb = $item->logo;
                                $title = $item->name;
                                if ($dataKey == 'groups') {
                                $meta = '
                                <span class="sb-text">
                                    <span class="meta">
                                        <span class="views"><i class="sb-bico sb-play-circle"></i> ' . $item->total_videos . '</span>
                                    </span>
                                </span>';
                                } else {
                                $meta = '
                                <span class="sb-text">
                                    <span class="meta">
                                        <span class="views"><i class="sb-bico sb-play-circle"></i> ' . $item->total_videos . '</span>
                                        <span class="subscribers"><i class="sb-bico sb-check-square-o"></i> ' . $item->total_subscribers . '</span>
                                    </span>
                                </span>';
                                }
                                $user_name = $item->creator_display_name;
                                $user_url = $item->creator_url;
                            } else {
                                $vimeo_thumb = (@$this->sboption['vimeo']['vimeo_thumb']) ? $this->sboption['vimeo']['vimeo_thumb'] : 'medium';
                                $thumb = $item->{"thumbnail_$vimeo_thumb"};
                                $title = $item->title;
                                $url = ($iframe) ? 'http://player.vimeo.com/video/'. $item->id : $link;
                                
                                if ($dataKey == 'albums') {
                                    $datetime = @$item->last_modified;
                                $meta = '
                                <span class="sb-text">
                                    <span class="meta">
                                        <span class="views"><i class="sb-bico sb-play-circle"></i> ' . @$item->total_videos . '</span>
                                    </span>
                                </span>';
                                } else {
                                $datetime = @$item->upload_date;
                                $meta = '
                                <span class="sb-text">
                                    <span class="meta">
                                        <span class="likes"><i class="sb-bico sb-thumbs-up"></i> ' . @$item->stats_number_of_likes . '</span>
                                        <span class="views"><i class="sb-bico sb-play-circle"></i> ' . @$item->stats_number_of_plays . '</span>
                                        <span class="comments"><i class="sb-bico sb-comment"></i> ' . @$item->stats_number_of_comments . '</span>
                                        <span class="duration"><i class="sb-bico sb-clock-o"></i> ' . @$item->duration . ' secs</span>
                                    </span>
                                </span>';
                                $user_name = @$item->user_name;
                                $user_url = @$item->user_url;
                                $user_image = @$item->user_portrait_medium;
                                }
                            }
                            
                            $final[$this->make_timestr($datetime, $link)] = $layoutobj->create_item($feed_class,
                            array(
                            'thumb' => @$thumb,
                            'thumburl' => $url,
                            'iframe' => $iframe,
                            'title' => '<a href="' . $link . '"'.@$iframe.$target.'>' . (@$attr['titles'] ? $this->title_limiter($title) : $title) . '</a>',
                            'text' => (@$attr['words']) ? $this->word_limiter($item->description, $link) : $item->description,
                            'meta' => (@$vimeo_output['share']) ? $meta : null,
                            'url' => $link,
                            'date' => $datetime,
                            'user' => array(
                                'name' => $user_name,
                                'url' => $user_url,
                                'image' => $user_image
                                ),
                            'type' => 'video-camera',
                            'icon' => array(@$themeoption['social_icons'][9], @$themeoption['type_icons'][6])
                            ), $attr, $vimeo_output);
                            }
                        }
                    }
                    }
                }
                elseif ( $feed_class == 'stumbleupon' ) {
                    $stumbleupon_output = ( ! empty($this->sboption['stumbleupon']['stumbleupon_output']) ) ? ss_explode($this->sboption['stumbleupon']['stumbleupon_output']) :  array( 'title' => true, 'thumb' => true, 'text' => true, 'user' => true, 'share' => true, 'info' => true );
                    $stumbleupon_feeds = (@$this->sboption['stumbleupon']['stumbleupon_feeds']) ? ss_explode($this->sboption['stumbleupon']['stumbleupon_feeds']) : array( 'comments' => true, 'likes' => true );
                    $fcount = 0;
                    if (@$feed)
                    foreach ($feed as $dataKey => $data) {
                        if (@$stumbleupon_feeds[$dataKey]) {
                        $channel = $data->channel;
                        $items = ( $dataKey == 'likes' ) ? $channel->item : $data->item;
                        foreach($items as $item) {
                            $link = @$item->link;
                            if ( $this->make_remove($link) ) {
                            $fcount++;
                            
                            // find user
                            $pattern = ( $dataKey == 'likes' ) ? '/http:\/\/www.stumbleupon.com\/stumbler\/(\w+)/i' : '/http:\/\/www.stumbleupon.com\/stumbler\/(\w+)\/comments/i';
                            $replacement = '$1';
                            $user_name = preg_replace($pattern, $replacement, $channel->link);
                            
                            $thumb = '';
                            $text = '';
                            $image = array();
                            if ($description = (string) @$item->description) {
                                if (@$attr['words']) {
                                    preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $description, $image);
                                    if ( ! empty($image) )
                                        $thumb = $image[1];
                                    $text = $this->word_limiter($description, $link);
                                }
                                else {
                                    $text = $description;
                                }
                            }

                            $final[$this->make_timestr($item->pubDate, $link)] = $layoutobj->create_item($feed_class,
                            array(
                            'thumb' => $thumb,
                            'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                            'text' => $text,
                            'url' => $link,
                            'date' => $item->pubDate,
                            'user' => array(
                                'name' => $user_name,
                                'url' => "http://www.stumbleupon.com/stumbler/$user_name",
                                'title' => @$channel->title
                                ),
                            'type' => ( $dataKey == 'likes' ) ? 'star-o' : 'comment-o',
                            'icon' => array(@$themeoption['social_icons'][10], @$themeoption['type_icons'][( $dataKey == 'likes' ) ? 8 : 9])
                            ), $attr, $stumbleupon_feeds);
                            
                            if ( $fcount >= $results ) break;
                            }
                        }
                        }
                    }
                }
                elseif ( $feed_class == 'deviantart' ) {
                    $fcount = 0;
                    $channel = @$feed->channel;
                    if (@$channel->item)
                    foreach($channel->item as $item) {
                        $link = @$item->link;
                        if ( $this->make_remove($link) ) {
                        $fcount++;

                        $description = $item->children('media', true)->description;
                        
                        $final[$this->make_timestr($item->pubDate, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'thumb' => @$item->children('media', true)->thumbnail->{1}->attributes()->url,
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                        'text' => (@$attr['words']) ? $this->word_limiter($description, $link) : $description,
                        'tags' => '<a href="' . $item->children('media', true)->category . '"'.$target.'>' . $item->children('media', true)->category->attributes()->label . '</a>',
                        'url' => $link,
                        'date' => $item->pubDate,
                        'user' => array(
                            'name' => $item->children('media', true)->credit->{0},
                            'url' => $item->children('media', true)->copyright->attributes()->url,
                            'image' => $item->children('media', true)->credit->{1}),
                        'type' => 'image',
                        'icon' => array(@$themeoption['social_icons'][11], @$themeoption['type_icons'][4])
                        ), $attr, $stumbleupon_output);
                        
                        if ( $fcount >= $results ) break;
                        }
                    }
                }
                elseif ( $feed_class == 'rss' ) {
                    $rss_output = (@$this->sboption['rss']['rss_output']) ? ss_explode($this->sboption['rss']['rss_output']) : array('title' => true, 'thumb' => true, 'text' => true, 'user' => true, 'tags' => false, 'share' => true, 'info' => true);
                    $fcount = 0;
                    if ( $channel = @$feed->channel ) { // rss
                        if (@$channel->item)
                        foreach($channel->item as $item) {
                            $link = @$item->link;
                            if ( $this->make_remove($link) ) {
                            $fcount++;
    
                            $thumb = '';
                            foreach($item->children('media', true)->thumbnail as $thumbnail) {
                                $thumb = $thumbnail->attributes()->url;
                            }
                            
                            if (@$item->category && @$rss_output['tags'])
                            foreach($item->category as $category) {
                                $cats[] = (string) $category;
                            }
                            
                            // set Snippet or Full Text
                            if (@$this->sboption['rss']['rss_text'])
                                $description = (@$item->children("content", true)->encoded) ? $item->children("content", true)->encoded : $item->description;
                            else
                                $description = $item->description;
    
                            $final[$this->make_timestr($item->pubDate, $link)] = $layoutobj->create_item($feed_class,
                            array(
                            'thumb' => (@$thumb) ? $thumb : '',
                            'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                            'text' => (string) (@$attr['words']) ? $this->word_limiter($description, $link) : $description,
                            'tags' => @implode(', ', $cats),
                            'url' => $link,
                            'date' => $item->pubDate,
                            'user' => array(
                                'name' => $channel->title,
                                'url' => $channel->link,
                                'image' => @$channel->image->url
                                ),
                            'type' => 'pencil',
                            'icon' => array(@$themeoption['social_icons'][12], @$themeoption['type_icons'][0])
                            ), $attr, $rss_output);
                            
                            if ( $fcount >= $results ) break;
                            }
                        }
                    } elseif ( $entry = @$feed->entry ) { // atom
                        // get feed link
                        foreach($feed->link as $link) {
                            if ($link->attributes()->rel == 'alternate')
                                $user_url = $link->attributes()->href;
                        }
                        foreach($feed->entry as $item) {
                            $link = @$item->link[0]->attributes()->href;
                            if ( $this->make_remove($link) ) {
                            $fcount++;
    
                            $title = (string) $item->title;
                            $thumb = '';
                            foreach($item->media as $thumbnail) {
                                $thumb = $thumbnail->attributes()->url;
                            }
                            
                            $cats = '';
                            if (@$item->category && @$rss_output['tags']) {
                                foreach($item->category as $category) {
                                    $cats .= $category->attributes()->term.', ';
                                }
                                $cats = rtrim($cats, ", ");
                            }

                            // set Snippet or Full Text
                            if (@$this->sboption['rss']['rss_text']) {
                                $content = (string) @$item->content;
                                $description = ($content) ? $content : (string) $item->summary;
                            } else
                                $description = (string) $item->summary;
                                
                            $text = '';
                            $image = array();
                            if (@$description) {
                                if (@$attr['words']) {
                                    if (!$thumb) {
                                        preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $description, $image);
                                        if ( ! empty($image) )
                                            $thumb = $image[1];
                                    }
                                    $text = $this->word_limiter($description, $link);
                                }
                                else {
                                    $text = $description;
                                }
                            }
                            
                            $final[$this->make_timestr($item->published, $link)] = $layoutobj->create_item($feed_class,
                            array(
                            'thumb' => @$thumb,
                            'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($title) : $title) . '</a>',
                            'text' => @$text,
                            'tags' => @$cats,
                            'url' => $link,
                            'date' => $item->published,
                            'user' => array(
                                'name' => $feed->title,
                                'url' => @$user_url,
                                'image' => @$feed->logo
                                ),
                            'type' => 'pencil',
                            'icon' => array(@$themeoption['social_icons'][12], @$themeoption['type_icons'][0])
                            ), $attr, $rss_output);
                            
                            if ( $fcount >= $results ) break;
                            }
                        }
                    }
                }
                elseif ( $feed_class == 'soundcloud' ) {
                    $soundcloud_output = (@$this->sboption['soundcloud']['soundcloud_output']) ? ss_explode($this->sboption['soundcloud']['soundcloud_output']) : array('title' => true, 'text' => true, 'thumb' => true, 'user' => true, 'share' => true, 'info' => true, 'meta' => true, 'tags' => false);
                    if (@$feed)
                    foreach ($feed as $item) {
                        $link = @$item->permalink_url;
                        if ( $this->make_remove($link) ) {
                        // tags
                        $tags = '';
                        if ( @$soundcloud_output['tags'] ) {
                            if (@$item->tag_list)
                                $tags .= $item->tag_list;
                        }
                        
                        // convert duration to mins
                        $duration = '';
                        if (@$item->duration) {
                            $seconds = floor($item->duration / 1000);
                            $duration = floor($seconds / 60);
                        }
                        
                        $download = '';
                        if (@$item->download_url) {
                            $download .= '<span class="download"><i class="sb-bico sb-cloud-download"></i> ' . @$item->download_count . '</span>';
                        }
                        
                        $meta = '
                        <span class="sb-text">
                            <span class="meta">
                                <span class="likes"><i class="sb-bico sb-thumbs-up"></i> ' . @$item->favoritings_count . '</span>
                                <span class="views"><i class="sb-bico sb-play-circle"></i> ' . @$item->playback_count . '</span>
                                <span class="comments"><i class="sb-bico sb-comment"></i> ' . @$item->comment_count . '</span>
                                <span class="duration"><i class="sb-bico sb-clock-o"></i> ' . @$duration . ' mins</span>
                                ' . $download . '
                            </span>
                        </span>';
                        
                        $final[$this->make_timestr($item->created_at, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                        'text' => (@$attr['words']) ? $this->word_limiter(@$item->description, $link) : @$item->description,
                        'thumb' => (@$item->artwork_url) ? $item->artwork_url : '',
                        'tags' => $tags,
                        'url' => $link,
                        'meta' => (@$soundcloud_output['meta']) ? $meta : null,
                        'date' => $item->created_at,
                        'user' => array(
                            'name' => $item->user->username,
                            'url' => $item->user->permalink_url,
                            'image' => $item->user->avatar_url
                            ),
                        'type' => 'youtube-play',
                        'icon' => array(@$themeoption['social_icons'][13], @$themeoption['type_icons'][7])
                        ), $attr, $soundcloud_output);
                        }
                    }
                }
                elseif ( $feed_class == 'vk' ) {
                    if (@$feed) {
                        $vk_output = (@$this->sboption['vk']['vk_output']) ? ss_explode($this->sboption['vk']['vk_output']) : array( 'thumb' => true, 'text' => true, 'stat' => true, 'user' => true, 'share' => true, 'info' => true );
                    // vk next page start
                    $offset = @$feed->offset;
                    $loadcrawl[$feed_class.$i.$ifeed] = ($offset == 0) ? $results : $results + $offset;
                    
                    if ($groups = @$feed->response->groups) {
                        foreach ($feed->response->groups as $group) {
                            $groupdata['-'.$group->id] = $group;
                        }
                    }
                    if ($profiles = @$feed->response->profiles) {
                        foreach ($feed->response->profiles as $profile) {
                            $userdata[$profile->id] = $profile;
                        }
                    }
                    if (@$feed->response)
                    foreach ($feed->response->items as $entry) {
                        $link = 'http://vk.com/wall'.@$entry->owner_id.'_'.@$entry->id;
                        if ( $this->make_remove($link) ) {
                        
                        // body text
                        $text = @$entry->text;
                        if ( ! $text) {
                            if (@$entry->copy_history)
                                $text = $entry->copy_history[0]->text;
                        }
                        $text = (@$attr['words']) ? @$this->word_limiter($text, $link) : @$this->format_text($text);
                        // Add links to all hash tags
                        $text = preg_replace('/#([^\s]+)/', '<a href="http://vk.com/feed?section=search&q=%23$1"'.$target.'>#$1</a>', $text );
                        
                        // user info
                        $user = (@$userdata[$entry->from_id]) ? $userdata[$entry->from_id] : $groupdata[$entry->from_id];
                        $user_name = (@$user->name) ? $user->name : $user->first_name.' '.$user->last_name;
                        $user_image = $user->photo_50;
                        $user_url = ($user->screen_name) ? 'http://vk.com/' . $user->screen_name : 'http://vk.com/id' . $entry->from_id;
                        
                        $image_width = (@$this->sboption['vk']['vk_image_width']) ? $this->sboption['vk']['vk_image_width'] : '604';
                        $attachments = @$entry->attachments;
                        if ( ! $attachments) {
                            if (@$entry->copy_history)
                                $attachments = $entry->copy_history[0]->attachments;
                        }
                        $source = '';
                        if ( ! empty($attachments) ) {
                            if ($image_width) {
                                foreach ($attachments as $attach) {
                                    if ($attach->type == 'photo') {
                                        $photo_width = "photo_$image_width";
                                        $source = @$attach->photo->{$photo_width};
                                        break;
                                    } if ($attach->type == 'link') {
                                        $source = (@$attach->link->image_big) ? $attach->link->image_big : @$attach->link->image_src;
                                        $thumburl = @$attach->link->url;
                                        break;
                                    } if ($attach->type == 'video') {
                                        $source = ($image_width <= 130) ? @$attach->video->photo_130 : @$attach->video->photo_320;
                                        break;
                                    }
                                }
                            }
                        }
                        
                        $meta = (@$vk_output['stat']) ? '
                        <span class="sb-text">
                            <span class="meta">
                                <span class="likes"><i class="sb-bico sb-thumbs-up"></i>' . $entry->likes->count . '</span>
                                <span class="shares"><i class="sb-bico sb-retweet"></i> ' . $entry->reposts->count . '</span>
                                <span class="comments"><i class="sb-bico sb-comment"></i> ' . $entry->comments->count . '</span>
                            </span>
                        </span>' : null;
                        
                        $final[$this->make_timestr($entry->date, $link)] = $layoutobj->create_item($feed_class,
                        array(
                        'thumb' => (@$source) ? $source : '',
                        'thumburl' => $thumburl,
                        'text' => @$text,
                        'meta' => @$meta,
                        'url' => @$link,
                        'date' => $entry->date,
                        'user' => array(
                            'name' => @$user_name,
                            'url' => $user_url,
                            'image' => @$user_image
                            ),
                        'type' => 'pencil',
                        'icon' => array(@$themeoption['social_icons'][14], ($i == 2) ? @$themeoption['type_icons'][4] : @$themeoption['type_icons'][0] )
                        ), $attr, $vk_output);
                        }
                    } // end foreach
                    } // end $feed
                }

                // each network sorting
                if ( ! empty($final) ) {
                    krsort($final);
                    reset($final);
                    $ifeedclass = $feed_class.$i.$ifeed;
                    
                    if ( ! empty($loadmore) ) {
                        // filter last items
                        if ( $lastloaditem = $loadmore[$ifeedclass] ) {
                            $loadremovefrom = array_search( $lastloaditem, array_keys($final) );
                            if ( ! @$loadremovefrom) $loadremovefrom = 0;
                            if ( empty($_SESSION[$label]['loadcrawl'][$ifeedclass]) )
                                $loadremovefrom++;
                            $final = array_slice($final, $loadremovefrom);
                        }
                    }

                    $ranking[key($final)] = $ifeedclass;
                    $finals[$ifeedclass] = $final;
                    $rankcount[$ifeedclass] = count($final);
                }
                $final = array();
                
                } // end foreach
                }
            } // end foreach $feeds

            if ( @$ranking ) {
                // defining limits by recent basis
                krsort($ranking);
                $rsum = 0;
                $rnum = count($ranking);
                for ($i = 1; $i <= $rnum; $i++) {
                    $rsum += $i;
                }
                $i = $rnum;
                foreach ($ranking as $cfeed) {
                    $rank[$cfeed] = round( ($i * 100) / $rsum );
                    $i--;
                }
            }

            if ( @$rankcount ) {
                $maxcountkey = array_search(max($rankcount), $rankcount);
                foreach ($rankcount as $rkey => $rval) {
                    $fresults[$rkey] = @round($rank[$rkey] * $results / 100);
                }
                foreach ($rankcount as $rkey => $rval) {
                    if ( $fresults[$rkey] > $rval ) {
                        $diffrankcount = $fresults[$rkey] - $rval;
                        $fresults[$rkey] -= $diffrankcount;
                        $fresults[$maxcountkey] += $diffrankcount;
                    }
                }
            }
            
            if ( @$finals ) {
                // filnal sorting and adding
                foreach ($finals as $fkey => $fval) {
                    $fcount = 0;
                    // limit last result
                    foreach ($fval as $key => $val) {
                        $fcount++;
                        $final[$key] = $val;
                        $loadmore[$fkey] = $key;
                        if ( $fcount >= $fresults[$fkey] ) break;
                    }
                }

                if ( array_sum($rankcount) <= $results && ! $is_feed && ( ! $GLOBALS['islive'] || $_REQUEST['action'] == "sb_loadmore" ) ) {
                    // set next pages if exist
                    foreach ($rankcount as $rkey => $rval) {
                        if (@$loadcrawl[$rkey])
                            $_SESSION[$label]['loadcrawl'][$rkey] = $loadcrawl[$rkey];
                    }
                }
                
                krsort($final);
                if ( $order == 'random' ) shuffle($final);
                foreach ($final as $key => $val) {
                    $output .= $val;
                }
            } else {
                $output_error = '<p class="sboard-nodata"><strong>PHP Social Stream:</strong> There is no feed data to display!</p>';
            }
        } else {
            if ( empty($loadmore) )
                $output_error = '<p class="sboard-nodata"><strong>PHP Social Stream: </strong>There is no feed to show or you are not connected to the world wide web!</p>';
        }

        if (@$attr['loadmore']) {
            $_SESSION[$label]['loadmore'] = $loadmore;
        }
        
        if ($ajax_feed && $is_feed) {
            if (@$output_error)
                $output .= $output_error;
        }
        
    	if ( ! $ajax_feed) {
            if ( $is_feed ) {
                if (@$output_error)
                    $output .= $output_error;
                $output .= "</ul></div>";
                
                if (!@$attr['carousel']){
                if (@$attr['autostart']) {
                    $play_none = ' style="display: none;"';
                } else {
                    $pause_none = ' style="display: none;"';
                }
                $controls = (@$attr['controls']) ? '
                    <div class="control">
                        <span class="fa-hover" id="ticker-next-'.$label.'"><i class="sb-bico sb-wico sb-arrow-down"></i></span>
                        <span class="fa-hover" id="ticker-prev-'.$label.'"><i class="sb-bico sb-wico sb-arrow-up"></i></span>
                        <span class="fa-hover" id="ticker-pause-'.$label.'"'.@$pause_none.'><i class="sb-bico sb-wico sb-pause"></i></span>
                        <span class="fa-hover" id="ticker-play-'.$label.'"'.@$play_none.'><i class="sb-bico sb-wico sb-play"></i></span>
                    </div>' : '';
                    
                $filters = '';
                if ( ! @$attr['tabable'] && @$filterItems && ! empty($feeds) ) {
                    $filters = (@$attr['filters']) ? '
                    <div class="filter">
                        <span class="fa-hover active" data-filter="all"><i class="sb-bico sb-wico sb-ellipsis-h" title="'.ss_lang( 'show_all' ).'"></i></span>
                        '.implode("\n", $filterItems).'
                    </div>' : '';
                }
                
                if (@$attr['filters'] or @$attr['controls'])
                $output .= '
                <div class="toolbar">
                    '.$controls.'
                    '.$filters.'
                </div>'."\n";
            }
            }
        }
        
        if ($is_wall || $is_timeline) {
            if (@$output_error) {
                $output = str_replace(' timeline ', ' ', $output);
                $output .= $output_error;
            }
        }

        if ( ! $ajax_feed) {
        $output .= "</div>\n";
        if ( ! $is_feed && ! @$output_error )
            $output .= '<div class="sb-loadmore" data-nonce="'.ss_nonce_create( 'loadmore', $label ).'"><p>'.ss_lang( 'load_more' ).'</p></div>'."\n";
        if ($is_wall || $is_timeline)
            $output .= "</div>\n";

        $iframe_output = (@$iframe) ? '$("#timeline_'.$label.' .iframe").colorbox({iframe:true, innerWidth:640, innerHeight:390});' : '';
        
        // loadmore ajax function
        if (@$attr['loadmore']) {
        	$more_output = '
                $("#sb_'.$label.'").on("click", ".sb-loadmore", function() {
                  nonce = $(this).attr("data-nonce");';
                $more_output .= "$('#sb_".$label." .sb-loadmore').html('<p class=\"sb-loading\">&nbsp;</p>');";
                $more_output .= '
                  $.ajax({
                    type: "post",
                    url: "'.SB_PATH.'ajax.php",
                    data: {action: "sb_loadmore", attr: '.$attr_ajax.', nonce: nonce, label: "'.$label.'"},
                    cache: false
                    })
                    .done(function( response ) {
                        $("#timeline_'.$label.'").append(response);';
                if ( $is_wall )
                    $more_output .= '
            			wall.container.find(".sb-item img").load(function() {
            				wall.fitWidth();
            			});
                        wall.fitWidth();';
                    $more_output .= '
                        '.$iframe_output.'
                        $("#sb_'.$label.' .sb-loadmore").html("'.ss_lang( 'load_more' ).'");
                    })
                    .fail(function() {
                        alert( "Problem reading the feed data!" );
                    });
                });';
        }
        
        $ticker_id_t = '';
        if ( $is_feed ) {
            if (@$attr['carousel']) {
                $cs_auto = (@$attr['cs_auto']) ? 'slider.play();' : '';
                if ( ! @is_array($attr['cs_item']) )
                    $attr['cs_item'] = $setoption['carouselsetting']['cs_item'];
        $output .= '
        <script>
        	 $(document).ready(function($) {
    			var slider = $("#ticker_'.$label.'").lightSlider({
                    item: '.@$attr['cs_item'][0].',
                    autoWidth: '.@$attr['autoWidth'].',
                    slideMove: '.@$attr['slideMove'].',
                    slideMargin: '.@$attr['slideMargin'].',
                    auto: '.(@$attr['cs_auto'] ? 'true' : 'false').',
                    loop: '.(@$attr['cs_loop'] ? 'true' : 'false').',
                    controls: '.(@$attr['cs_controls'] ? 'true' : 'false').',
                    rtl: '.@$attr['cs_rtl'].',
                    pager: '.(@$attr['cs_pager'] ? 'true' : 'false').',
                    speed: '.@$attr['cs_speed'].',
                    responsive : [
                        {
                            breakpoint:960,
                            settings: {
                                item: '.@$attr['cs_item'][1].'
                              }
                        },
                        {
                            breakpoint:768,
                            settings: {
                                item: '.@$attr['cs_item'][2].'
                              }
                        },
                        {
                            breakpoint:600,
                            settings: {
                                item: '.@$attr['cs_item'][3].'
                              }
                        },
                        {
                            breakpoint:480,
                            settings: {
                                item: '.@$attr['cs_item'][4].'
                              }
                        }
                    ]
                });
                ' . $cs_auto . '
    		});
        </script>';
            }
        $output .= '
        <script type="text/javascript">
            $(document).ready(function($) {';
                if (!@$attr['carousel']) {
                $ticker_id = '#ticker_'.$label;
                $output .= '
                $("'.$ticker_id.'").newsTicker({
                    row_height: '.$block_height.',
                    max_rows: 1,
                    speed: '.@$attr['rotate_speed'].',
                    duration: '.@$attr['duration'].',
                    direction: "'.@$attr['direction'].'",
                    autostart: '.@$attr['autostart'].',
                    pauseOnHover: '.@$attr['pauseonhover'].',
                    prevButton: $("#ticker-prev-'.$label.'"),
                    nextButton: $("#ticker-next-'.$label.'"),
                    stopButton: $("#ticker-pause-'.$label.'"),
                    startButton: $("#ticker-play-'.$label.'"),
                    start: function() {
                    	$("#timeline_'.$label.' #ticker-pause-'.$label.'").show();
                        $("#timeline_'.$label.' #ticker-play-'.$label.'").hide();
                    },
                    stop: function() {
                    	$("#timeline_'.$label.' #ticker-pause-'.$label.'").hide();
                        $("#timeline_'.$label.' #ticker-play-'.$label.'").show();
                    }
                });';
            if ( ! @$attr['tabable'] && @$attr['filters'] )
                $output .= "
            	/* Filtering */
                $('#timeline_$label .filter span').click(function() {
            		/* fetch the class of the clicked item */
            		var ourClass = $(this).data('filter');
            		
            		/* reset the active class on all the buttons */
            		$('#timeline_$label .filter span').removeClass('active');
            		/* update the active state on our clicked button */
            		$(this).addClass('active');
            		
            		if(ourClass == 'all') {
            			/* show all our items */
            			$('$ticker_id').children('li.sb-item').show();
            		}
            		else {
            			/* hide all elements that don't share ourClass */
            			$('$ticker_id').children('li:not(' + ourClass + ')').fadeOut('fast');
            			/* show all elements that do share ourClass */
            			$('$ticker_id').children('li' + ourClass).fadeIn('fast');
            		}
            		return false;
            	});";
                }
                
                if ( @$attr['slide'] && ! (@$attr['tabable'] && @$attr['position'] == 'normal') ) {
                if ( $location == 'left' || $location == 'right' ){
                    $getsizeof = 'Width';
                    $opener = 'sb-opener';
                    $padding = '';
                } else {
                    $getsizeof = 'Height';
                    $opener = 'sb-heading';
                    $padding = ( @$attr['showheader'] || ($location == 'bottom' && ! @$attr['tabable']) ) ? ' - 30' : '';
                }
                $openid = (@$attr['tabable']) ? "#timeline_$label .sb-tabs li" : "#timeline_$label .$opener";
                $output .= "
                /* slide in/out */
                var padding = $('#timeline_$label').outer$getsizeof();
                padding = parseFloat(padding)$padding;";
                $output .= ( @$attr['autoclose'] ) ? "$('#timeline_$label').animate({ '$location': '-='+padding+'px' }, 'fast' );" : '';
                $output .= "
                $('$openid').click(function(event) {
                    if ( $('#timeline_$label').hasClass('open') ) {
                        if ( $(this).hasClass( 'active' ) ) {
                            $('$openid').removeClass('active');
                            $('#timeline_$label').animate({ '$location': '-='+padding+'px' }, 'slow' ).removeClass('open');
                        } else {
                            $('$openid').removeClass('active');
                            $(this).addClass('active');
                        }
                    } else {
                        $(this).addClass('active');
                        $('#timeline_$label').animate({ '$location': '+='+padding+'px' }, 'slow' ).addClass('open');
                    }
                    event.preventDefault();
                });";
                }
                else { // only for normal tabable
                    $openid = "#timeline_$label .sb-tabs li";
                $output .= "
                $('$openid').click(function(event) {
                    $('$openid').removeClass('active');
                    if ( $('#timeline_$label').hasClass('open') ) {
                        if ( $(this).hasClass( 'active' ) ) {
                            $('#timeline_$label').removeClass('open');
                        } else {
                            $(this).addClass('active');
                        }
                    } else {
                        $(this).addClass('active');
                        $('#timeline_$label').addClass('open');
                    }
                    event.preventDefault();
                });";
                }
                
                $output .= '
            });
        </script>';
        if (@$ticker_id)
            $ticker_id_t = ' '.$ticker_id;
            } elseif ( $is_wall ) {
        if ( ! empty($feeds) ) {
            $itemwidth = (@$attr['itemwidth'] ? $attr['itemwidth'] : $setoption['setting']['itemwidth']);
        $output .= '
    	<script type="text/javascript">
            $(document).ready(function($) {
    			var wall = new freewall("#timeline_'.$label.$ticker_id_t.'");
    			wall.reset({
    				selector: ".sb-item",
                    cellW: function(width) {
                      var cellWidth = '.$itemwidth.';
                      if (width > cellWidth)
                        return cellWidth;
                      else
                        return 180;
                    },
    				cellH: "auto",';
                if (@$attr['gutterX'])
                $output .= '
                    gutterX: '.$attr['gutterX'].',';
                if (@$attr['gutterY'])
                $output .= '
                    gutterY: '.$attr['gutterY'].',';
                if ( ! @$attr['fixSize'])
                $output .= '
                    fixSize: 1,';
                $output .= '
                    animate: '.@$attr['animate'].',
                    delay: '.@$attr['delay'].',
                    rightToLeft: '.@$attr['filter_direction'].',
    				onResize: function() {
    					wall.fitWidth();
    				}
    			});
                
    			$(".filter-label").click(function() {
    				wall.unFilter();
                    $(".filter-label").removeClass("active");
    				var filter = $(this).addClass("active").data("filter");
    				if (filter) {
    					wall.filter(filter);
    				} else {
    					wall.unFilter();
    				}
    			});
                
    			wall.container.find(".sb-item img").load(function() {
    				wall.fitWidth();
    			});
                wall.fitWidth();
            '.$more_output;
        $output .= '
            });
    	</script>';
        }
        } elseif ( $is_timeline ) {
            $output .= '
    	<script type="text/javascript">
            $(document).ready(function($) {
                '.$more_output.'
            });
    	</script>
            ';
        }

            if (@$attr['tabable']) {
        	$output .= '
            <script type="text/javascript">
            $(document).ready(function($) {
               $("#timeline_'.$label.' .sb-tabs").on("click", "li", function() {
                if ( $(this).hasClass( "active" ) ) {
                  feed = $(this).attr("data-feed");
                  nonce = $(this).parent().attr("data-nonce");
                  ';
               $output .= "
                  $('#timeline_".$label." .sb-content ul').html('<p class=\"sb-loading\"><i class=\"sb-icon sb-'+feed+'\"></i></p>');";
               $output .= '
                  $.ajax({
                    type: "post",
                    url: "'.SB_PATH.'ajax.php",
                    data: {action: "sb_tabable", feed: feed, attr: '.$attr_ajax.', nonce: nonce, label: "'.$label.'"},
                    cache: false
                    })
                    .done(function( response ) {
                        $("#timeline_'.$label.$ticker_id_t.'").html(response);
                        /* rebuild feed ticker */
                        $("'.$ticker_id.'").newsTicker();
                    })
                    .fail(function() {
                        alert( "Problem reading the feed data!" );
                  });
                }
               });
            });
            </script>';
            }
            
            if (@$iframe) {
            $output .= '
        	<script>
        		$(document).ready(function($) {
        		  '.$iframe_output.'
                });
        	</script>';
            }
            
            if (@$GLOBALS['islive'] && ! $is_feed) {
                $timeinterval = (@$attr['live_interval'] ? intval($attr['live_interval']) * 60000 : 60000); // 60000 = 1 Min
                $stdiv = ($is_wall) ? 'div.sb-item' : 'div.timeline-row';
                $output .= '
            <script type="text/javascript">
            $(document).ready(function($) {
              var nonce = "'.ss_nonce_create( 'liveupdate' ).'";
              setInterval(function(){
                  var stlen = $("#timeline_'.$label.' '.$stdiv.'").length;
                  $.ajax({
                    type: "post",
                    url: "'.SB_PATH.'ajax.php",
                    data: {action: "sb_liveupdate", attr: '.$attr_ajax.', nonce: nonce, results: stlen, label: "'.$label.'"},
                    cache: false,
                    success: function(data) {
                      if (data != "")
                        $("#timeline_'.$label.'").html(data);';
                        if ( $is_wall )
                            $output .= '$("#timeline_'.$label.'").trigger( "resize" );';
                        elseif ( $is_timeline )
                            $output .= '$("#timeline_'.$label.'").trigger( "scroll" );';
                            
                        $output .= $iframe_output.'
                    }
                  });
              }, '.$timeinterval.');
            });
            </script>';
            }
        }
        
        if ( ! $ajax_feed)
            $output .= ($forceCrawl) ? "\t<!-- End PHP Social Stream - cache is disabled. -->\n" : "\t<!-- End PHP Social Stream - cache is enabled - duration: " . $attr['cache'] . " minutes -->\n";
        $output = str_replace( array("\r\n","\r","\t","\n"), '', $output );
        
    	if ( $echo )
    		echo $output;
    	else
    		return $output;
    }
    
    // function for retrieving data from feeds
    public function get_feed( $feed_key, $i, $key2, $feed_value, $results, $sboption, $cache, $forceCrawl = false, $sb_label = null ) {
        $feed_value = trim($feed_value);
        switch ( $feed_key ) {
            case 'facebook':
                $pageresults = 9; // the max results that is possible to fetch from facebook API - for group = 9 for page = 30 - we used 9 to support both
                $stepresults = ceil($results / $pageresults);
                $facebook_access_token = @$GLOBALS['api']['facebook']['facebook_access_token'];
                
                if ($datetime_from = @$sboption['facebook_datetime_from'])
                    $since_str = '&since='.strtotime($datetime_from);
                    
                if ($datetime_to = @$sboption['facebook_datetime_to'])
                    $until_str = '&until='.strtotime($datetime_to);
                
                if ($until = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $until_str = '&until='.$until;
                
                $afields = array('id','created_time','updated_time','link','from','name','source','message','description','story','comments','likes','picture','object_id','status_type');
                // define the feed url
                if ($i == 5) {
                    /* Deprecated
                    // search term or hashtag
                    if ( ! @$since_str)
                        $since_str = '&since='.strtotime("-1 year"); // from last year
                    $feed_url = 'https://graph.facebook.com/v1.0/search?q='.urlencode($feed_value).'&limit=' . $pageresults . $since_str . @$until_str .'&access_token=' . $facebook_access_token;
                    $label = 'https://graph.facebook.com/search?q='.urlencode($feed_value).'&limit=' . $results;
                    */
                }
                else {
                    // Page Feed
                    if ($i == 1) {
                        $feedType = (@$sboption['facebook_pagefeed']) ? $sboption['facebook_pagefeed'] : 'feed';
                    }
                    // Group Feed
                    elseif ($i == 2) {
                        $feedType = 'feed';
                    }
                    elseif ($i == 3) {
                        $feedType = 'photos';
                        $afields[] = 'images';
                    } elseif ($i == 4) {
                        $feedType = 'videos';
                        $afields[] = 'images';
                    }
                    
                    $fields = implode(',', $afields);
                    $feed_url = 'https://graph.facebook.com/v2.3/' . $feed_value . '/' . $feedType . '?limit=' . ( ($i == 2) ? $pageresults : $results ) . @$since_str . @$until_str . '&fields=' . $fields . '&access_token=' . $facebook_access_token;
                    $label = 'https://graph.facebook.com/' . $feed_value . '/' . $feedType . '?limit=' . $results;
                }
                
                // if group feed
                if ($i == 2) {
                    // crawl the feed or read from the cache
                    $get_feed = TRUE;
                    if ( ! $forceCrawl ) {
                        if ( $cache->is_cached($label) ) {
                            $content = $cache->get_cache($label);
                            $get_feed = FALSE;
                        }
                    }
                    if ($get_feed) {
                        $feed = array();
                        for ($i = 1; $i <= $stepresults; $i++) {
                            $content = $cache->do_curl($feed_url);
                            $pagefeed = @json_decode($content);
                            if ( ! empty($pagefeed) ) {
                            $feed[] = $pagefeed->data;
                            if ( count($pagefeed->data) < $pageresults )
                                break;
                            $feed_url = $pagefeed->paging->next;
                            }
                        }
               			if ( ! $forceCrawl )
                            $cache->set_cache($label, json_encode($feed));
                    }
                    else
                        $feed = @json_decode($content);
                }
                else {
                    $content = ( ! $forceCrawl ) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    if ( $pagefeed = @json_decode($content) ) {
                        if ( isset( $pagefeed->error ) ) {
                            if (@$this->attr['debuglog']) ss_debug_log( 'Facebook error: '.@$pagefeed->error->message.' - ' . $feedType, SB_LOGFILE );
                            $feed[] = null;
                        } else {
                            $feed[] = $pagefeed->data;
                        }
                    }
                }
            break;
            case 'twitter':
                $consumer_key = @trim($GLOBALS['api']['twitter']['twitter_api_key']);
                $consumer_secret = @trim($GLOBALS['api']['twitter']['twitter_api_secret']);
                $oauth_access_token = @trim($GLOBALS['api']['twitter']['twitter_access_token']);
                $oauth_access_token_secret = @trim($GLOBALS['api']['twitter']['twitter_access_token_secret']);
                if ( isset($sboption['twitter_feeds']) )
                    $twitter_feeds = explode(',', str_replace(' ', '', $sboption['twitter_feeds']) );
                else
                    $twitter_feeds = array('retweets', 'replies');
                switch($i)
                {
                	case 1:
                        $rest = 'statuses/user_timeline';
                        $params = array(
                            'exclude_replies' => in_array('replies', $twitter_feeds) ? 'false' : 'true',
                            'screen_name' => $feed_value
                            );
                        if ( ! in_array('retweets', $twitter_feeds) )
                            $params['include_rts'] = 'false';
                	break;
                	case 2:
                        $rest = "lists/statuses";
                        if ( is_numeric($feed_value) )
                            $params = array('list_id' => $feed_value);
                        else {
                            $feedvalarr = explode('/', $feed_value);
                            $params = array('owner_screen_name' => $feedvalarr[0], 'slug' => $feedvalarr[1]);
                            if ( in_array('retweets', $twitter_feeds) )
                                $params['include_rts'] = 'true';
                        }
                	break;
                	case 3:
                        $rest = "search/tweets";
                        $feed_value = urlencode($feed_value);
                        if ( ! in_array('retweets', $twitter_feeds) )
                            $feed_value .= ' AND -filter:retweets';
                        $params = array('q' => $feed_value);
                	break;
                }
                $params['count'] = $results;
                
                if ($id_from = @$sboption['twitter_since_id'])
                    $params['since_id'] = $id_from;
                    
                if ($id_to = @$sboption['twitter_max_id'])
                    $params['max_id'] = $id_to;
                    
                if ($max_id = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $params['max_id'] = $max_id;
        		
                $get_feed = TRUE;
                $label = 'https://api.twitter.com/1.1/'.$rest.'/'.serialize($params);
                if ( ! $forceCrawl ) {
                    if ( $cache->is_cached($label) ) {
                        $content = $cache->get_cache($label);
                        $get_feed = FALSE;
                    }
                }
                if ($get_feed) {
                    if ( ! class_exists( 'TwitterOAuth' ) )
                        require_once('oauth/twitteroauth.php');
                    $auth = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_access_token, $oauth_access_token_secret);
                    $auth->timeout = SB_API_TIMEOUT;
                    $auth->connecttimeout = SB_API_TIMEOUT;
                    $auth->decode_json = FALSE;
                    $content = $auth->get( $rest, $params );
                    if ( ! $content ) {
                    	if (@$this->attr['debuglog']) ss_debug_log( 'Twitter error: An error occurs while reading the feed, please check your connection or settings.', SB_LOGFILE );
                    }
                    else {
                        $feed = @json_decode($content);
                        if ( isset( $feed->errors ) ) {
                            foreach( $feed->errors as $key => $val ) {
                                if (@$this->attr['debuglog']) ss_debug_log( 'Twitter error: '.$val->message.' - ' . $rest, SB_LOGFILE );
                            }
                            $feed = null;
                        }
                    }
           			if ( ! $forceCrawl )
                        $cache->set_cache($label, $content);
                }
                else
                    $feed = @json_decode($content);
    		break;
    		case 'google':
    			$google_api_key = @$GLOBALS['api']['google']['google_api_key'];
                if ($nextPageToken = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $pageToken = '&pageToken='.$nextPageToken;
                $feed_url = 'https://www.googleapis.com/plus/v1/people/' . $feed_value . '/activities/public?maxResults=' . $results . @$pageToken . '&key=' . $google_api_key;
                $content = ( ! $forceCrawl ) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
                if (@$feed->error) {
                    $feed = null;
                }
    		break;
            case 'flickr':
                $flickr_api_key = @$GLOBALS['api']['flickr']['flickr_api_key'];
                if ($nextPage = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $pageToken = '&page='.$nextPage;
                if ($i == 1) {
                    $feedType = 'flickr.people.getPublicPhotos';
                    $feedID = '&user_id='.$feed_value;
                } elseif ($i == 2) {
                    $feedType = 'flickr.groups.pools.getPhotos';
                    $feedID = '&group_id='.$feed_value;
                }
                $feed_url = 'http://api.flickr.com/services/feeds/'.$feedType.'.gne?id=' . $feed_value . '&lang=en-us&format=php_serial&jsoncallback=1';
                $feed_url = 'https://api.flickr.com/services/rest/?method='.$feedType.'&api_key='.$flickr_api_key . $feedID . '&per_page=' . $results . @$pageToken . '&extras=date_upload,date_taken,owner_name,icon_server,tags,views&format=json&nojsoncallback=1';
                $content = ( ! $forceCrawl ) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
    		break;
            case 'delicious':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                    $feed_url = "http://feeds.delicious.com/v2/json/" . $feed_value . '?count=' . $results;
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    $feed = @json_decode($content);
                }
            break;
    		case 'pinterest':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                    $rss_uri = ($i == 1) ? 'feed.rss' : 'rss';
                    $feed_url = "https://www.pinterest.com/" . $feed_value . "/$rss_uri";
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    $feed = @simplexml_load_string($content);
                }
    		break;
    		case 'instagram':
                $instagram_access_token = @$GLOBALS['api']['instagram']['instagram_access_token'];
                $max_str = 'max_id';
                $feed_url = '';
                if ($i == 1) {
                    $user_url = 'https://api.instagram.com/v1/users/search?q=' . $feed_value .'&access_token=' . $instagram_access_token;
                    $user_content = ( ! $forceCrawl) ? $cache->get_data($user_url, $user_url) : @$cache->do_curl($user_url);
                    if ($user_content) {
                        $user_feed = @json_decode($user_content);
                        if ( ! empty($user_feed->data) )
                            $feed_url = 'https://api.instagram.com/v1/users/' . $user_feed->data[0]->id . '/media/recent?count=' . $results;
                    }
                } elseif ($i == 2) {
                    $feed_url = 'https://api.instagram.com/v1/tags/' . urlencode($feed_value) . '/media/recent?count=' . $results;
                    $max_str = 'max_tag_id';
                } elseif ($i == 3) {
                    $feed_url = 'https://api.instagram.com/v1/locations/' . $feed_value . '/media/recent?access_token=' . $instagram_access_token;
                } elseif ($i == 4) {
                    $coordinates = explode(',', $feed_value);
                    $feed_url = 'https://api.instagram.com/v1/media/search?lat=' . $coordinates[0] . '&lng=' . $coordinates[1] . '&distance=' . $coordinates[2];
                    $max_str = 'max_timestamp';
                }
                $feed_url .= '&access_token=' . $instagram_access_token;
                
                if (@$feed_url) {
                    if ($next_max_id = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                        $feed_url .= '&'. $max_str .'='.$next_max_id;
                    
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    $feed = @json_decode($content);
                }
    		break;
    		case 'youtube':
                $google_api_key = @$GLOBALS['api']['google']['google_api_key'];
                if ($nextPageToken = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $pageToken = '&pageToken='.$nextPageToken;
                switch($i)
                {
                	case 1:
                    case 4:
                        $channel_filter = ($i == 1) ? 'forUsername' : 'id';
                        $user_url = 'https://www.googleapis.com/youtube/v3/channels?part=contentDetails&'.$channel_filter.'=' . $feed_value .'&key=' . $google_api_key;
                        $user_content = ( ! $forceCrawl) ? $cache->get_data($user_url, $user_url) : @$cache->do_curl($user_url);
                        if ($user_content) {
                            $user_feed = @json_decode($user_content);
                            if (@$user_feed->items[0])
                                $feed_url = 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId=' . $user_feed->items[0]->contentDetails->relatedPlaylists->uploads;
                        }
                    break;
                    case 2:
                        $feed_url = 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId=' . $feed_value;
                    break;
                    case 3:
                        $feed_url = 'https://www.googleapis.com/youtube/v3/search?q=' . rawurlencode($feed_value);
                    break;
                }
                if ($results > 50) $results = 50;
                if (@$feed_url) {
                    $feed_url .= '&part=snippet&maxResults=' . $results . @$pageToken . '&key=' . $google_api_key;
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    $feed = @json_decode($content);
                }
    		break;
    		case 'vimeo':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                    $vimeo_feeds = (@$sboption['vimeo_feeds']) ? ss_explode($sboption['vimeo_feeds']) : array( 'videos' => true, 'likes' => true, 'appears_in' => true, 'all_videos' => true, 'subscriptions' => true, 'albums' => true, 'channels' => true, 'groups' => true );
                $requests = "videos,likes,appears_in,all_videos,subscriptions,albums,channels,groups";
                $feedtypes = explode(',', $requests);
                foreach ($feedtypes as $type) {
                    if (@$vimeo_feeds[$type]) {
                        $feed_url = 'https://vimeo.com/api/v2/' . $feed_value . '/' . $type . ".json";
                        $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                        if ( $data = @json_decode($content) )
                            $feed[$type] = $data;
                    }
                }
                }
    		break;
    		case 'tumblr':
                $tumblr_api_key = @$GLOBALS['api']['tumblr']['tumblr_api_key'];
                $feed_url = "https://api.tumblr.com/v2/blog/" . $feed_value . ".tumblr.com/posts?api_key={$tumblr_api_key}&limit=$results";
                if ($posts_start = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $feed_url .= '&offset='.$posts_start;
                    
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
            break;
    		case 'stumbleupon':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                    $stumbleupon_feeds = (@$sboption['stumbleupon_feeds']) ? ss_explode($sboption['stumbleupon_feeds']) : array( 'comments' => true, 'likes' => true );
                $feedtypes = array('comments', 'likes');
                foreach ($feedtypes as $type) {
                    if (@$stumbleupon_feeds[$type]) {
                        $feed_url = "http://www.stumbleupon.com/rss/stumbler/" . $feed_value . "/" . $type;
                        $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                        if ( $data = @simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOCDATA) )
                            $feed[$type] = $data;
                    }
                }
                }
    		break;
    		case 'deviantart':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                $feed_url = "https://backend.deviantart.com/rss.xml?type=deviation&q=by%3A" . $feed_value . "+sort%3Atime+meta%3Aall";
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @simplexml_load_string($content);
                }
    		break;
            case 'rss':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_value, $feed_value) : $cache->do_curl($feed_value);
                    $feed = @simplexml_load_string($content);
                }
            break;
            case 'soundcloud':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                $soundcloud_client_id = @$GLOBALS['api']['soundcloud']['soundcloud_client_id'];
                $feed_url = "http://api.soundcloud.com/users/$feed_value/tracks.json?client_id=" . $soundcloud_client_id . "&limit=$results";
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
                }
            break;
            case 'vk':
                $pagefeed = (@$sboption['vk_pagefeed']) ? $sboption['vk_pagefeed'] : 'all';
                $wall_by = ($i == 1) ? 'domain' : 'owner_id';
                $feed_url = "https://api.vk.com/method/wall.get?v=5.34&{$wall_by}={$feed_value}&count={$results}&extended=1&lang=en&filter={$pagefeed}";
                if ($offset = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $feed_url .= '&offset='.$offset;
                else
                    $offset = 0;
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $content = @mb_convert_encoding($content, "UTF-8", "auto");
                $feed = @json_decode($content);
                if (is_object($feed))
                    $feed->offset = $offset;
            break;
    	}
        
    	return @$feed;
    }
    
    // create time string for sorting and applying pinning options
    private function make_timestr($time, $link) {
        $timestr = ( is_numeric($time) ) ? $time : strtotime($time);
        if ( ! empty($this->attr['pins']) ) {
            $dkey = array_search($link, $this->attr['pins']);
            if ($dkey !== false)
                $timestr = strtotime("+$dkey day");
        }
        return $timestr.'-'.$GLOBALS['order']++;
    }
    
    // applying stream items removal
    private function make_remove($link) {
        if ( ! empty($this->attr['remove']) ) {
            if ( in_array($link, $this->attr['remove']) )
                return false;
        }
        return true;
    }
    
    /**
     * Word Limiter
     *
     * Limits a string to X number of words.
     *
     * @param   $end_char   the end character. Usually an ellipsis
     */
    function word_limiter($str, $url = '', $comment = false) {
        $limit = ($comment) ? @$this->attr['commentwords'] : @$this->attr['words'];
        $end_char = '...';
        
        $str = trim( strip_tags($str) );
    	if ($str == '')
    	{
    		return $str;
    	}
        
        if ($this->str_word_count_utf8($str) < $limit) {
            $str = $this->format_text($str);
            return $str;
        }
        
    	preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);
        if (strlen($str) == strlen($matches[0]))
    	{
    		$end_char = '';
    	}
        $str = $this->format_text($matches[0]);
    	if (@$this->attr['readmore'] && $url)
            $end_char = ' <a href="' . $url . '"'.$this->target.' style="font-size: large;">' . $end_char . '</a>';
            
        return $str.$end_char;
    }
    
    // Title Limiter (limits the title of each item to X number of words)
    function title_limiter($str, $url = '') {
        $end_char = '...';
        $limit = (@$this->attr['titles']) ? $this->attr['titles'] : 15;
        $str = strip_tags($str);

    	if (trim($str) == '')
    	{
    		return $str;
    	}
        
        if ($this->str_word_count_utf8($str) < $limit) {
            return $str;
        }

    	preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);

        if (strlen($str) == strlen($matches[0]))
    	{
    		$end_char = '';
    	}
            
        return rtrim($matches[0]).$end_char;
    }
    
    function format_text($str) {
        // make the urls hyper links
        $links = array();
        if ( $matches = $this->geturls($str) ) {
            foreach ($matches as $url)
                $links[] = '<a href="'.$url.'"'.$this->target.'>'.$url.'</a>';
            $str = str_replace($matches, $links, $str);
        }
        return nl2br($str);
    }
    
    // Word counter function
    function str_word_count_utf8($str) {
        return count(preg_split('~[^\p{L}\p{N}\']+~u',$str));
    }
    
    // get all URLs from string
    function geturls($string) {
        $regex = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        preg_match_all($regex, $string, $matches);
        return ($matches[0]);
    }
} // end class

// Check for Windows to find and replace the %e modifier correctly
function ss_format_locale( $format ) {
    if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
        $format = preg_replace('#(?<!%)((?:%%)*)%e#', '\1%d', $format);
    }
    return $format;
}

// Friendly dates (i.e. "2 days ago")
function ss_friendly_date( $date ) {
	
	// Get the time difference in seconds
	$post_time = ( is_numeric($date) ) ? $date : strtotime( $date );
	$current_time = time();
	$time_difference = $current_time - $post_time;
	
	// Seconds per...
	$minute = 60;
	$hour = 3600;
	$day = 86400;
	$week = $day * 7;
	$month = $day * 31;
	$year = $day * 366;
	
	// if over 3 years
	if ( $time_difference > $year * 3 ) {
		$friendly_date = ss_lang( 'a_long_while_ago' );
	}
	
	// if over 2 years
	else if ( $time_difference > $year * 2 ) {
		$friendly_date = ss_lang( 'over_2_years_ago' );
	}
	
	// if over 1 year
	else if ( $time_difference > $year ) {
		$friendly_date = ss_lang( 'over_a_year_ago' );
	}
	
	// if over 11 months
	else if ( $time_difference >= $month * 11 ) {
		$friendly_date = ss_lang( 'about_a_year_ago' );
	}
	
	// if over 2 months
	else if ( $time_difference >= $month * 2 ) {
		$months = (int) $time_difference / $month;
		$friendly_date = sprintf( ss_lang( 'd_months_ago' ), $months );
	}
	
	// if over 4 weeks ago
	else if ( $time_difference > $week * 4 ) {
		$friendly_date = ss_lang( 'last_month' );
	}
	
	// if over 3 weeks ago
	else if ( $time_difference > $week * 3 ) {
		$friendly_date = ss_lang( '3_weeks_ago' );
	}
	
	// if over 2 weeks ago
	else if ( $time_difference > $week * 2 ) {
		$friendly_date = ss_lang( '2_weeks_ago' );
	}
	
	// if equal to or more than a week ago
	else if ( $time_difference >= $day * 7 ) {
		$friendly_date = ss_lang( 'last_week' );
	}
	
	// if equal to or more than 2 days ago
	else if ( $time_difference >= $day * 2 ) {
		$days = (int) $time_difference / $day;
		$friendly_date = sprintf( ss_lang( 'd_days_ago' ), $days );
	}
	
	// if equal to or more than 1 day ago
	else if ( $time_difference >= $day ) {
		$friendly_date = ss_lang( 'yesterday' );
	}
	
	// 2 or more hours ago
	else if ( $time_difference >= $hour * 2 ) {
		$hours = (int) $time_difference / $hour;
		$friendly_date = sprintf( ss_lang( 'd_hours_ago' ), $hours );
	}
	
	// 1 hour ago
	else if ( $time_difference >= $hour ) {
		$friendly_date = ss_lang( 'an_hour_ago' );
	}
	
	// 2–59 minutes ago
	else if ( $time_difference >= $minute * 2 ) {
		$minutes = (int) $time_difference / $minute;
		$friendly_date = sprintf( ss_lang( 'd_minutes_ago' ), $minutes );
	}
	
	else {
		$friendly_date = ss_lang( 'just_now' );
	}
	
	// HTML 5 FTW
	return '<time title="' . strftime( SB_DT_FORMAT, $post_time ) . '" datetime="' . date( 'c', $post_time ) . '" pubdate>' . ucfirst( $friendly_date ) . '</time>';
}

// i18n dates
function ss_i18n_date( $date, $format ) {
    $post_time = ( is_numeric($date) ) ? $date : strtotime( $date );
    return strftime( $format, $post_time );
}

function ss_explode( $output = array() ) {
    if ( ! empty($output) ) {
        $outputArr = explode(',', str_replace(' ', '', $output) );
        foreach ($outputArr as $val)
            $out[$val] = true;
        
        return $out;
    }
    return false;
}

// hex to rgb numerical converter for color styling
function ss_hex2rgb($hex, $str = true) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   } 
   // returns the rgb values separated by commas OR returns an array with the rgb values
   $rgb = ($str) ? "$r, $g, $b" : array($r, $g, $b);
   return $rgb;
}

// This method creates an nonce. It should be called by one of the previous two functions.
function ss_nonce_create( $action = '' , $user = '' ) {
	return substr( ss_nonce_generate_hash( $action . $user ), -12, 10);
}

// This method validates an nonce
function ss_nonce_verify( $nonce , $action = '' , $user = '' ) {
	// Nonce generated 0-12 hours ago
	if ( substr(ss_nonce_generate_hash( $action . $user ), -12, 10) == $nonce ) {
		return true;
	}
	return false;
}

// This method generates the nonce timestamp
function ss_nonce_generate_hash( $action = '' , $user = '' ) {
	return md5( SB_NONCE_KEY . $action . $user . $action );
}

// Retrieves the translated string for language
function ss_lang($label) {
    return $GLOBALS['_'][$label];
}

function ss_win_locale($locale) {
    $winlocale = array(
    "en" => 'USA_ENU',
    "ar" => 'SAU_ARA',
    "az" => 'AZE_AZE',
    "bg_BG" => 'BGR_BGR',
    "bs_BA" => 'Bosanski',
    "ca" => 'ESP_CAT',
    "cy" => 'Cymraeg',
    "da_DK" => 'DNK_DAN',
    "de_CH" => 'CHE_DES',
    "de_DE" => 'DEU_DEU',
    "el" => 'GRC_ELL',
    "en_CA" => 'CAN_ENC',
    "en_AU" => 'AUS_ENA',
    "en_GB" => 'GBR_ENG',
    "eo" => 'Esperanto',
    "es_PE" => 'PER_ESR',
    "es_ES" => 'ESP_ESN',
    "es_MX" => 'MEX_ESM',
    "es_CL" => 'CHL_ESL',
    "eu" => 'ESP_EUQ',
    "fa_IR" => 'IRN_FAR',
    "fi" => 'FIN_FIN',
    "fr_FR" => 'FRA_FRA',
    "gd" => 'Gàidhlig',
    "gl_ES" => 'ESP_GLC',
    "haz" => 'هزاره گی',
    "he_IL" => 'ISR_HEB',
    "hr" => 'HRV_HRV',
    "hu_HU" => 'HUN_HUN',
    "id_ID" => 'IDN_IND',
    "is_IS" => 'ISL_ISL',
    "it_IT" => 'ITA_ITA',
    "ja" => 'JPN_JPN',
    "ko_KR" => 'KOR_KOR',
    "lt_LT" => 'LTU_LTH',
    "my_MM" => 'ဗမာစာ',
    "nb_NO" => 'NOR_NOR',
    "nl_NL" => 'nld_nld',
    "nn_NO" => 'Norsk nynorsk',
    "oci" => 'Occitan',
    "pl_PL" => 'POL_PLK',
    "ps" => 'پښتو',
    "pt_PT" => 'PRT_PTG',
    "pt_BR" => 'BRA_PTB',
    "ro_RO" => 'ROM_ROM',
    "ru_RU" => 'Русский',
    "sk_SK" => 'Slovenčina',
    "sl_SI" => 'Slovenščina',
    "sq" => 'Shqip',
    "sr_RS" => 'Српски језик',
    "sv_SE" => 'Svenska',
    "th" => 'ไทย',
    "tr_TR" => 'Türkçe',
    "ug_CN" => 'Uyƣurqə',
    "uk" => 'Українська',
    "zh_CN" => '简体中文',
    "zh_TW" => '繁體中文');
    return $winlocale[$locale];
}

// grab the layout files
function ss_getFileTitles( $path = '/layout/' ) {
    $_aFileTitles = array();
    if ($handle = opendir( SB_DIRNAME . $path )) {
        while($file = readdir($handle)) {
            if ($file !== '.' && $file !== '..')
            {
                $finfo = pathinfo($file);
                if ( $finfo['extension'] == 'php' )
                    $_aFileTitles[$finfo['filename']] = $finfo['filename'];
            }
        }
        closedir($handle);
    }
    if ( empty($_aFileTitles) ) {
        $_aFileTitles[''] = '-- There is no layout created --';
    }
    return $_aFileTitles;
}

function ss_debug_log($mValue, $sFilePath = null) {
    $msg = date( "Y/m/d H:i:s", time() ) . ' - ' . $mValue . PHP_EOL;
    error_log($msg, 3, $sFilePath); 
}

// function for using in template files
function social_stream( $atts ) {
    $sb = new SocialStream();
    return $sb->init( $atts, false );
}

?>