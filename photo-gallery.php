<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">
			
		<main class="MW1200 clear-fix">
			<h1 id="page-title">PHOTO GALLERY</h1>
			<div class="hint no-padding-top">Filter your search by selecting your interest below.</div>
			<div class="filter">
				<div class="filter-btn">Filter</div>
				<div class="btns-wrapper no-padding">
					<div class="filteringBtn" data-filter-index="0">All</div>
					<div class="spacing"></div>
					<div class="filteringBtn" data-filter-index="1">Phoenicia Hotel</div>
					<div class="spacing"></div>
					<div class="filteringBtn" data-filter-index="2">Rooms &amp; Suites</div>
					<div class="spacing"></div>
					<div class="filteringBtn" data-filter-index="3">Dining</div>
					<div class="spacing"></div>
					<div class="filteringBtn" data-filter-index="4">Spa &amp; Wellness</div>
					<div class="spacing"></div>
					<div class="filteringBtn" data-filter-index="5">Meetings &amp; Events</div>
					<div class="spacing"></div>
					<div class="filteringBtn" data-filter-index="6">Art &amp; Collections</div>
					<div class="spacing"></div>
					<div class="filteringBtn" data-filter-index="7">Beirut</div>
					<div class="spacing"></div>
					<div class="filteringBtn applyBtn">APPLY</div>
				</div>
			</div>


			<div class="gallerySlider royalSlider rsMinW">
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="1" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="2" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="3" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="4" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="5" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="6" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="6" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="7" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="7" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="7" class="rsTmb" src="images/_photo-gallery.jpg"></a>
				<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="images/_photo-gallery.jpg" href="images/_photo-gallery.jpg">Image Description<img width="188" height="108" data-filter-index="7" class="rsTmb" src="images/_photo-gallery.jpg"></a>
		    </div>

		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
		
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script src="js/vendor/jquery.royalslider.min.js"></script>
	<script>
		var galleryModule = (function (jswindow, timer) {
			// cache DOM
			var $gallerySlider   	 = $('.gallerySlider');
			var $btnsWrapper     	 = $('.btns-wrapper');
			var $filter          	 = $('.filter');
			var $btnBeingClicked 	 = undefined;
			var $thisBtn     	 	 = undefined;
			var sliderInstance   	 = undefined;
			var isSelected 		 	 = false;
			var windowWidth      	 = 0;
			var numberOfSelectedBtns = 0;
			var imageObject          = {};
			var selectedBtns     	 = [];

			// attach event listeners
			_attachListeners();

			// init
			_initSlider();

			function _attachListeners() {
				$filter.on('click', '.filter-btn, .filteringBtn, .applyBtn', function () {
					$btnBeingClicked = $(this);
					
					if($btnBeingClicked.hasClass('applyBtn')) _filterSlides();
					else if($btnBeingClicked.hasClass('filteringBtn')) $btnBeingClicked.toggleClass('filter-btn-selected');
					else $btnsWrapper.slideToggle(500);
				});

				$(window).on('resize', function () {
					_runAfterResize();
				});
			}

			function _initSlider() {
				sliderInstance = $gallerySlider.royalSlider({
					autoScaleSlider: true,
					imageScaleMode: function(slideObject) { return (window.innerWidth >= 1000) ? 'fill' : 'fit-if-smaller'; },
				    controlNavigation: 'thumbnails',
					thumbs: {
						spacing: 14.5,
						fitInViewport: false,
						firstMargin: false
					},
				    numImagesToPreload: 2,
				    keyboardNavEnabled: true,					
				    arrowsNav:false,
				    fullscreen: {
				      enabled: true,
				      nativeFS: true
				    }
				}).data('royalSlider');

				$gallerySlider.css('visiblity', 'visible')
				.find('.rsThumbsArrow').css({position: 'absolute', top: 'auto', height:100, bottom:-116, left:-50 })
				.appendTo($gallerySlider)
				.filter('.rsThumbsArrowRight')
				.css({left:'auto', right: -50});
			}

			function _filterSlides() {
				_getSelectedBtns();

				if(numberOfSelectedBtns) {
					_destroySlider();
				
					_emptySliderHtml();

					_getImagesFromServer();

					_emptySelectedBtnsArray();

					if(jswindow.innerWidth < 1000) $btnsWrapper.slideToggle(500);
				}
			}

			function _getSelectedBtns() {
				$btnsWrapper.children('.filteringBtn').each(_checkIfBtnIsSelected);
				numberOfSelectedBtns = selectedBtns.length;
			}

			function _checkIfBtnIsSelected() {
				var $thisBtn = $(this);

				isSelected = $thisBtn.hasClass('filter-btn-selected');

				if(isSelected) selectedBtns.push($thisBtn.data('filter-index'));
			}

			function _destroySlider() {
				sliderInstance.destroy();
			}

			function _emptySliderHtml() {
				$gallerySlider.html('');
			}

			function _getImagesFromServer() {
				$.getJSON('json/gallery-images.json', selectedBtns , _appendSlides);
			}

			function _appendSlides(jsonObject) {
				// loop over indexes
				$.each(jsonObject, function (filterIndex, imageObjectsArray) {
					// loop over image Objects
					$.each(imageObjectsArray, function (imageIndex) {
						imageObject = imageObjectsArray[imageIndex];
						$gallerySlider.append('<a class="rsImg" data-rsw="1200" data-rsh="600" data-rsbigimg="' + imageObject.bigSizeImage + '" href="' + imageObject.normalSizeImage + '">Image Description<img width="188" height="108" class="rsTmb" src="' + imageObject.smallSizeImage + '"></a>');
					});
				});
				
				// after ajax finishes init slider again
				_initSlider();
			}

			function _emptySelectedBtnsArray() {
				selectedBtns = [];
			}

			function _tackleFilterClickEvent() {
				windowWidth = jswindow.innerWidth;

				if((!$btnsWrapper.is(':visible') && windowWidth >= 1000) || ($btnsWrapper.css('display') === 'block' && windowWidth >= 1000)) {
					$btnsWrapper.css('display', 'table');
				}
				else if(jswindow.innerWidth < 1000 && $btnsWrapper.css('display') === 'table') {
					$btnsWrapper.css('display', 'none');
				}
			}

			function _runAfterResize() {
				clearTimeout(timer);
				timer = setTimeout(function () {
					_tackleFilterClickEvent();
				}, 100);
			}

		})(window, undefined);
	</script>
</body>
</html>