<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/dining.css">
</head>
<body>

	<div class="opacity-layer"></div>

	<?php include 'include/header.php'; ?>
	
	<?php include 'include/nav.php'; ?>		

	<div id="ws-wrapper">

		<main class="MW1200 clear-fix">
			<h1 id="page-title">BARS &amp; RESTAURANTS</h1>
			
			<div class="dining">
				<ul class="card-menu">
					<li class="card card--thin card--odd">
						<a href="#" class="linkWrapper">
							<img class="card__image" src="images/_dining.png" alt="">
							<div class="card__text">
								<h3 class="card__text__first-title">CUISINE</h3>
								<p class="card__text__first-paragraph">INTERNATIONAL - ITALIAN</p>
								<h3>location &amp; contact</h3>	
								<p class="card__text__last-paragraph">the floor - +961(0)1 357024</p>
							</div>
							<div class="bg-link"></div>
						</a>
					</li>
					<li class="card card--thin card--even">
						<a href="#" class="linkWrapper">
							<img class="card__image" src="images/_dining.png" alt="">
							<div class="card__text">
								<h3 class="card__text__first-title">CUISINE</h3>
								<p class="card__text__first-paragraph">INTERNATIONAL - ITALIAN</p>
								<h3>location &amp; contact</h3>	
								<p class="card__text__last-paragraph">the floor - +961(0)1 357024 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, a.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, nemo.</p>
							</div>
							<div class="bg-link"></div>
						</a>
					</li>
					<li class="card card--thin card--odd">
						<a href="#" class="linkWrapper">
							<img class="card__image" src="images/_dining.png" alt="">
							<div class="card__text">
								<h3 class="card__text__first-title">CUISINE</h3>
								<p class="card__text__first-paragraph">INTERNATIONAL - ITALIAN</p>
								<h3>location &amp; contact</h3>	
								<p class="card__text__last-paragraph">the floor - +961(0)1 357024</p>
							</div>
							<div class="bg-link"></div>
						</a>
					</li>
					<li class="card card--thin card--even">
						<a href="#" class="linkWrapper">
							<img class="card__image" src="images/_dining.png" alt="">
							<div class="card__text">
								<h3 class="card__text__first-title">CUISINE</h3>
								<p class="card__text__first-paragraph">INTERNATIONAL - ITALIAN</p>
								<h3>location &amp; contact</h3>	
								<p class="card__text__last-paragraph">the floor - +961(0)1 357024</p>
							</div>
							<div class="bg-link"></div>
						</a>
					</li>
					<li class="card card--thin card--odd">
						<a href="#" class="linkWrapper">
							<img class="card__image" src="images/_dining.png" alt="">
							<div class="card__text">
								<h3 class="card__text__first-title">CUISINE</h3>
								<p class="card__text__first-paragraph">INTERNATIONAL - ITALIAN</p>
								<h3>location &amp; contact</h3>	
								<p class="card__text__last-paragraph">the floor - +961(0)1 357024</p>
							</div>
							<div class="bg-link"></div>
						</a>
					</li>
					<li class="card card--thin card--even">
						<a href="#" class="linkWrapper">
							<img class="card__image" src="images/_dining.png" alt="">
							<div class="card__text">
								<h3 class="card__text__first-title">CUISINE</h3>
								<p class="card__text__first-paragraph">INTERNATIONAL - ITALIAN</p>
								<h3>location &amp; contact</h3>	
								<p class="card__text__last-paragraph">the floor - +961(0)1 357024</p>
							</div>
							<div class="bg-link"></div>
						</a>
					</li>
					<li class="card card--wide card--odd">
						<a href="#" class="linkWrapper">
							<img class="card__image" src="images/_dining-last.jpg" alt="">
							<div class="bg-link"></div>
						</a>
					</li>
					<li class="card card--wide card--even">
						<a href="#" class="linkWrapper">
							<img class="card__image" src="images/_dining-last.jpg" alt="">
							<div class="bg-link"></div>
						</a>
					</li>
				</ul>
			</div>
		</main>

	</div>
	
	<?php include 'include/footer.php'; ?>
	
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>