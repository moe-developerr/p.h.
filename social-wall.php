<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
</head>
<body>

	<div class="opacity-layer"></div>

	<?php include 'include/header.php'; ?>
	
	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">
		
		<main class="MW1200 center-content clear-fix">
			<h1 id="page-title">PHOENICIA SOCIAL WALL</h1>
			<div>
<?php
include( './social-stream/social-stream.php' ); // Path to PHP Social Stream main file

echo social_stream(
        array(
            'id' => '1',
            'type' => 'timeline',
            'network' => array(

                'facebook' => array(
                    'facebook_id_1' => array(
                        '131884760338867' // Replace with your Facebook page ID
                    ),
                    'facebook_pagefeed' => 'posts'
                ),
                'twitter' => array(
                    'twitter_id_1' => array(
                        'mashable' // Replace with your Twitter username
                    ),
                    'twitter_images' => 'small',
                    'twitter_feeds' => 'retweets,replies'
                ),
				'instagram' => array(
                    'instagram_id_1' => array(
                        'nibalhoury'
                    )
                ),
				'instagram' => array(
                    'instagram_id_1' => array(
                        'nibalhoury'
                    )
                ),				
                'tumblr' => array(
                    'tumblr_id_1' => array(
                        'ghostphotographs' // Replace with your Tumblr username
                    )
                ),
                'pinterest' => array(
                    'pinterest_id_1' => array(
                        '2013TopPins' // Replace with your Pinterest username
                    )
                )
            ),
            'theme' => 'sb-default-light',
			'width' => '1000',
			'height' => '',
            'results' => 10,
            'debuglog' => 0,
            'add_files' => true
			
        )
    );
?>
</div>

		</main>

	</div>	<!-- Ws Wrapper -->
	
	<?php include 'include/footer.php'; ?>

	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>'