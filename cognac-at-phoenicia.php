<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<link rel="stylesheet" href="css/pages-content/cognac.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
	
		<main class="MW1200 clear-fix">
			<h1 id="page-title">COGANC AT PHOENICIA</h1>
			
			<div class="slider-wrapper">
				<div class="arrow rightArrow"></div>
				<div class="arrow leftArrow"></div>
				<div class="imageSlider rsMinW">
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, inventore.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, eaque.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, iste!</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
				</div>
			</div>
			
			<div class="content-container clear-fix">
				<div class="social-icons-inner clear-fix">
					<a href="#" class="fa google-plus"></a>
					<a href="#" class="fa fa-linkedin"></a>
					<a href="#" class="fa fa-facebook"></a>
					<a href="#" class="fa fa-twitter"></a>
					<span>SHARE ON</span>
				</div>
				<h1 class="content-container__heading">Cognac &amp; Armagnacs</h1>
				<div class="sub-title">The Phoenicia collection boasts well over 50 bottles of the world’s rarest and oldest Cognacs and Armagnacs. The following highlight a few samples of our exclusive selection.</div>
				
				<div class="champagne left">
					<span class="name">Jacques Hardy</span>
					<span class="date"> - Grande Fine champagne 1805</span>
					<p class="description">Perhaps one of the oldest names associated with very old cognac. It was made during the battle of Trafalgar and there are fewer than ten bottles left in the world.</p>
				</div>

				<div class="champagne right">
					<span class="name">Jacques Hardy</span>
					<span class="date"> - Grande Fine champagne 1805</span>
					<p class="description">Perhaps one of the oldest names associated with very old cognac. It was made during the battle of Trafalgar and there are fewer than ten bottles left in the world.</p>
				</div>

				<div class="champagne left">
					<span class="name">Jacques Hardy</span>
					<span class="date"> - Grande Fine champagne 1805</span>
					<p class="description">Perhaps one of the oldest names associated with very old cognac. It was made during the battle of Trafalgar and there are fewer than ten bottles left in the world.</p>
				</div>

				<div class="champagne right">
					<span class="name">Jacques Hardy</span>
					<span class="date"> - Grande Fine champagne 1805</span>
					<p class="description">Perhaps one of the oldest names associated with very old cognac. It was made during the battle of Trafalgar and there are fewer than ten bottles left in the world.</p>
				</div>
			</div>
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>

	<script src="js/vendor/jquery.royalslider.min.js"></script>
	<script>
		var imageSliderModule = (function () {
			// cache DOM
			var $sliderWrapper 		= $('.slider-wrapper');
			var $imageSlider   		= $sliderWrapper.children('.imageSlider');
			var $thisArrow          = undefined;
			var imageSliderInstance = undefined;

			// attach listeners
			_attachListeners();

			// init
			_init();

			function _attachListeners() {
				$sliderWrapper.on('click', '.arrow', _activateArrows);
			}

			function _init() {
				imageSliderInstance = $imageSlider.royalSlider({
					autoScaleSlider: true,
					imageScaleMode: 'fill',
					loop: true,
					controlNavigation: 'none',
					keyboardNavEnabled: true,
					autoplay: {
						enabled: true,
						pauseOnHover: true,
						stopAtAction: false,
						delay: 3000
					}
				}).data('royalSlider');
			}

			function _activateArrows() {
				$thisArrow = $(this);

				if($thisArrow.hasClass('leftArrow')) imageSliderInstance.prev();
				else imageSliderInstance.next();
			}
		})();
	</script>
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>