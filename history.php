<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<style>
	.event {
		color: #49176d;
		position: relative;
		text-align: left;
	}

	.event time {
		text-align: center;
		position: absolute;
		top: 35px;
		left: 50%;
		z-index: 2;
		-webkit-transform: translateX(-50%);
		    -ms-transform: translateX(-50%);
		   -moz-transform: translateX(-50%);
		   	  	transform: translateX(-50%);
	}

	.event .month, .event .year {
		background-color: #fff;
	}

	.event .year {
		font-size: 2.5rem;
	}

	.event .event-img {
		float: left;
		max-width: 600px;
		width: 50%;
		height: auto;
		padding: 90px 10px 50px 0;
	}

	.line {
		width: 1px;
		height: 100%;
		position: absolute;
		left: 50%;
		top: 0;
		z-index: 1;
		background-color: #49176d; 
	}

	.event .event-title, .event .about-event {
		float: right;
		width: 50%;
		padding-left: 10px;
	}

	.event .event-title {
		font-weight: 700;
		margin-bottom: 10px;
		line-height: 1.2;
		padding-top: 90px;
	}

	#events {
		text-align: center;
		max-width: 1200px;
		margin: 30px auto 0;
	}

	#events button {
		color: #49176d;
		background-color: transparent;
		border: none;
	}

	.odd-event .event-img {
		float: right;
		border: none;
		padding-right: 0;
		padding-left: 10px;
	}

	.odd-event .event-title, .odd-event .about-event {
		text-align: right;
		float: left;
		padding-right: 10px;
		padding-left: 0;
	}

	#load-more-btn {
		outline: none;
	}

	.event:last-of-type {
		padding-bottom: 31px;
	}

	@media screen and (min-width: 1000px) {
		#load-more-btn:hover {
			font-weight: bold;
			-webkit-transform: scale(1.1);
			   -moz-transform: scale(1.1);
			    -ms-transform: scale(1.1);
			        transform: scale(1.1);
		}

		#page-title {
			margin-top: 100px;
			margin-bottom: 30px;
		}

		.event .event-title, .event .about-event {
			padding-left: 20px;
		}

		.event .about-event {
			padding-right: 15%;
		}

		.odd-event .about-event {
			padding-right: 10px;
			padding-left: 15%;
		}
	}

	@media screen and (min-width: 1200px) {
		.odd-event .event-title, .odd-event .about-event {
			text-align: right;
			padding-right: 20px;
		}

		.odd-event .about-event {
			padding-left: 15%;
		}
	}
</style>
</head>
<body>

	<div class="opacity-layer"></div>

	<?php include 'include/header.php'; ?>
	
	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">
		
		<main class="MW1200 center-content clear-fix">

			<h1 id="page-title">THE HOTEL</h1>
			
			<div class="slider-wrapper">
				<div class="arrow rightArrow"></div>
				<div class="arrow leftArrow"></div>
				<div class="imageSlider rsMinW">
					<div class="slide">
						<a href="images/_accomodation.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, inventore.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, eaque.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, iste!</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
				</div>
			</div>

			<div id="events">
				<div class="event clear-fix">
					<time><div class="month">SEPTEMBER</div><div class="year">2011</div></time>
					<a href="#a" class="blockElement"><img src="images/_history.jpg" class="event-img"></a>
					<div class="line"></div>
					<h1 class="event-title">50 year anniversary</h1>
					<p class="about-event">50 years of heritage and history have consolidated the Phoenicia's place as a landmark of Beirut.</p>
				</div>
				
				<div class="event clear-fix odd-event">
					<time><div class="month">SEPTEMBER</div><div class="year">2011</div></time>
					<a href="#b" class="blockElement"><img src="images/_history.jpg" class="event-img"></a>
					<div class="line"></div>
					<h1 class="event-title">50 year anniversary</h1>
					<p class="about-event">50 years of heritage and history have consolidated the Phoenicia's place as a landmark of Beirut.</p>
				</div>
				
				<div class="event clear-fix">
					<time><div class="month">SEPTEMBER</div><div class="year">2011</div></time>
					<a href="#c" class="blockElement"><img src="images/_history.jpg" class="event-img"></a>
					<div class="line"></div>
					<h1 class="event-title">50 year anniversary</h1>
					<p class="about-event">50 years of heritage and history have consolidated the Phoenicia's place as a landmark of Beirut.</p>
				</div>

				<div class="event clear-fix odd-event">
					<time><div class="month">SEPTEMBER</div><div class="year">2011</div></time>
					<a href="#b" class="blockElement"><img src="images/_history.jpg" class="event-img"></a>
					<div class="line"></div>
					<h1 class="event-title">50 year anniversary</h1>
					<p class="about-event">50 years of heritage and history have consolidated the Phoenicia's place as a landmark of Beirut.</p>
				</div>

				<button id="load-more-btn">BACK IN TIME</button>
			</div>

		</main>

	</div>	<!-- Ws Wrapper -->
	
	<?php include 'include/footer.php'; ?>
		
	<script src="js/vendor/jquery.royalslider.min.js"></script>
	
	<script>
		var imageSliderModule = (function () {
			// cache DOM
			var $sliderWrapper 		= $('.slider-wrapper');
			var $imageSlider   		= $sliderWrapper.children('.imageSlider');
			var $thisArrow          = undefined;
			var imageSliderInstance = undefined;

			// attach listeners
			_attachListeners();

			// init
			_init();

			function _attachListeners() {
				$sliderWrapper.on('click', '.arrow', _activateArrows);
			}

			function _init() {
				imageSliderInstance = $imageSlider.royalSlider({
					autoScaleSlider: true,
					imageScaleMode: 'fill',
					loop: true,
					controlNavigation: 'none',
					keyboardNavEnabled: true,
					transitionSpeed: 1000,
					autoplay: {
						enabled: true,
						pauseOnHover: true,
						stopAtAction: false,
						delay: 3000
					}
				}).data('royalSlider');
			}

			function _activateArrows() {
				$thisArrow = $(this);

				if($thisArrow.hasClass('leftArrow')) imageSliderInstance.prev();
				else imageSliderInstance.next();
			}
		})();

		var setTheTextInTheMiddleOfImageModule = (function ($window) {
			// cache DOM
			var $eventImage = $('.event-img'),
				$aboutEvent = $('.about-event'),
				$eventTitle = $('.event-title'),
				eih      	= $eventImage.height(),
				aeh      	= $aboutEvent.height(),
				eth      	= $eventTitle.height(),
				xtimer	    = undefined;

			// attach listeners
			_attachListeners();

			function _attachListeners() {
				$window.on('load', function () {
					if(eih > eth + aeh)
						$eventTitle.css('padding-top', (90 + eih/2 - (eth + aeh)/2) + 'px');
					else
						$eventTitle.css('padding-top', '90px');
				}).on('resize', function () {
					clearTimeout(xtimer);
					xtimer = setTimeout(function () {
						var eih = $eventImage.height(),
							aeh = $aboutEvent.height(),
							eth = $eventTitle.height();
						
						if(eih > eth + aeh)
							$eventTitle.css('padding-top', (90 + eih/2 - (eth + aeh)/2) + 'px');
						else
							$eventTitle.css('padding-top', '90px');
					}, 100);
				});
			}
		})($(window));
	</script>

	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>'