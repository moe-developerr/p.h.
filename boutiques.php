<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<link rel="stylesheet" href="css/pages-content/boutiques.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">
			
		<main class="MW1200 clear-fix">
			<h1 id="page-title">BOUTIQUES</h1>

			<div class="slider-wrapper">
				<div class="arrow rightArrow"></div>
				<div class="arrow leftArrow"></div>
				<div class="imageSlider rsMinW">
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, inventore.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, eaque.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, iste!</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
				</div>
			</div>
			
			<div class="hint">Filter your search by selecting your interest below.</div>
			<div class="filter">
				<div class="filter-btn">Filter</div>
				<div class="btns-wrapper">
					<div class="filteringBtn" data-filter-type="all">All</div>
					<div class="filteringBtn" data-filter-type="fashion">Fashion</div>
					<div class="filteringBtn" data-filter-type="services">Services</div>
					<div class="filteringBtn" data-filter-type="home decoration items">Home Decoration Items</div>
					<div class="filteringBtn" data-filter-type="jewelery">Jewelery</div>
				</div>
			</div>

			<div class="results">
				<div class="result left" data-filter-type="fashion">
					<h3>Aishti Shop Main Lobby</h3>
					<p>The ultimate luxury brand house in Lebanon, takes Phoenicia as another showcase.</p>
				</div>

				<div class="result right" data-filter-type="services">
					<h3>Debra Shop Ground Floor</h3>
					<p>Specialized in luxury clothing, for glamorous women, Debra is the venue for imported Furs.</p>
				</div>

				<div class="result left" data-filter-type="fashion">
					<h3>Iwan Maktabi Main Lobby</h3>
					<p>Leader in Lebanon, Iwan Maktabi’s carpets collection spans various styles and periods.</p>
				</div>

				<div class="result right" data-filter-type="jewelery">
					<h3>Avis Rent Car Main Lobby</h3>
					<p>Ranked as the number one car rental company in the world, Avis provides a large cars portfolio.</p>
				</div>

				<div class="result left" data-filter-type="jewelery">
					<h3>Fleurs et Couleurs Ground Floor</h3>
					<p>Since 1995, Fleurs et Couleurs transforms ordinary spaces into majestic environments.</p>
				</div>

				<div class="result right" data-filter-type="services">
					<h3>MEA Ground Floor</h3>
					<p>Flying directly to 30 destinations in the world, MEA is Lebanon’s national airline company.</p>
				</div>

				<div class="result left" data-filter-type="home decoration items">
					<h3>Pace e Luce Ground Floor</h3>
					<p>With a family tradition in the hairdressing business, Pace e Luce is synonymous to style. The barber Shop “Roni Daccache” Ground Floor</p>
				</div>
			</div>
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
	<script src="js/vendor/jquery.royalslider.min.js"></script>
	
	<script>
		var imageSliderModule = (function () {
			// cache DOM
			var $sliderWrapper 		= $('.slider-wrapper');
			var $imageSlider   		= $sliderWrapper.children('.imageSlider');
			var $thisArrow          = undefined;
			var imageSliderInstance = undefined;

			// attach listeners
			_attachListeners();

			// init
			_init();

			function _attachListeners() {
				$sliderWrapper.on('click', '.arrow', _activateArrows);
			}

			function _init() {
				imageSliderInstance = $imageSlider.royalSlider({
					autoScaleSlider: true,
					imageScaleMode: 'fill',
					loop: true,
					controlNavigation: 'none',
					keyboardNavEnabled: true,
					autoplay: {
						enabled: true,
						pauseOnHover: true,
						stopAtAction: false,
						delay: 3000
					}
				}).data('royalSlider');
			}

			function _activateArrows() {
				$thisArrow = $(this);

				if($thisArrow.hasClass('leftArrow')) imageSliderInstance.prev();
				else imageSliderInstance.next();
			}
		})();

		var filter = (function (jswindow, timer) {
			// cache DOM
			var $filterBtn   = $('.filter-btn'),
				$btnsWrapper = $('.btns-wrapper'),
				$result      = $('.result'),
				activeBtns   = [];
				index        = 0;

			// init
			_init();

			function _init() {
				attachHandlers();
			}

			function attachHandlers() {
				$filterBtn.on('click', _toggleFilter);
				$(window).on('resize', _runAfterResize);
				$btnsWrapper.on('click', '>div', _filterResults);
			}
			
			function _toggleFilter() {
				$btnsWrapper.slideToggle('200');
			}

			function _showFilterIfHidden() {
				if(jswindow.innerWidth >= 1000 && $btnsWrapper.is(':hidden')) {
					$btnsWrapper.show(0);
				}
			}

			function _filterResults() {
				var $btnClicked = $(this);

				$btnClicked.toggleClass('active');
					
				if($btnClicked.data('filter-type') === 'all') {
					
					$btnClicked.toggleClass('active').siblings().removeClass('active');
					_showAllSections();

				} else if($btnClicked.hasClass('active')) { // turn on a btn
					_hideAllSections();
					_checkActiveBtns();
					_loopOverActiveBtns(activeBtns);
				} else { // turn off a btn
					_hideAllSections();
					_checkActiveBtns();
					_loopOverActiveBtns(activeBtns);
					if(!activeBtns.length) _showAllSections();
				}
			}

			function _checkActiveBtns() {
				activeBtns = [];
				index 	   = 0;

				$.each($btnsWrapper.children('div'), function () {
					if($(this).hasClass('active'))
						activeBtns[index++] = $(this).data('filter-type');
				});

				return activeBtns;
			}

			function _loopOverActiveBtns(activeBtns) {
				$.each(activeBtns, _showActiveSection);
			}

			function _showActiveSection(index, activeSection) {
				$result.filter('[data-filter-type="' + activeSection + '"]').css('opacity', 1);
			}

			function _hideAllSections() {
				$result.css('opacity', 0.3);
			}

			function _showAllSections() {
				$result.css('opacity', 1);
			}

			function _runAfterResize() {
				clearTimeout(timer);
				timer = setTimeout(_showFilterIfHidden, 100);
			}
		})(window, undefined);
	</script>

	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>