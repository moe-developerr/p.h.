<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<link rel="stylesheet" href="css/pages-content/art-at-phoenicia.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">			
		<main class="MW1200 clear-fix">
			<h1 id="page-title">ART AT PHOENICIA</h1>
			
			<div class="slider-wrapper">
				<div class="arrow rightArrow"></div>
				<div class="arrow leftArrow"></div>
				<div class="imageSlider rsMinW">
					<div class="slide">
						<a href="images/_accomodation.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, inventore.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, eaque.</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, iste!</div>
					</div>
					<div class="slide">
						<a href="images/_beirut.jpg" class="rsImg"></a>
						<div class="rsText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, praesentium.</div>
					</div>
				</div>
			</div>
			
			<div class="content-container clear-fix">
				<div class="social-icons-inner clear-fix">
					<a href="#" class="fa fa-google-plus"></a>
					<a href="#" class="fa fa-linkedin"></a>
					<a href="#" class="fa fa-facebook"></a>
					<a href="#" class="fa fa-twitter"></a>
					<span>SHARE ON</span>
				</div>

				<div class="table">
					<div class="names">
						<div data-name="andy">Andy Goldsworthy</div>
						<div data-name="">Catherine Yass</div>
						<div data-name="">Doris Riachi</div>
						<div data-name="">Howard Hodgkin</div>
						<div data-name="">Ian McKeever</div>
						<div data-name="">Ingrid Shaker Nessman</div>
						<div data-name="">Jamil Molaeb</div>
						<div data-name="">Jan Dibbets</div>
						<div data-name="">Jason Martin</div>
						<div data-name="">Mona Bassili Sehnaoui</div>
						<div data-name="">Paul Morrison</div>
						<div data-name="">Richard Long</div>
						<div data-name="">Ronald Cameron</div>
						<div data-name="">Sam Francis</div>
						<div data-name="">Sami Makarem</div>
						<div data-name="">Stephen Cox</div>
						<div data-name="">Therese Oulton</div>
					</div>
					
					<div class="about-names">
						<div class="about-name">
							<h2 class="title-name">Viewing internationally recognized Fine Art in a hotel space was a first for Lebanon, especially that promising young talents have the opportunity to examine &amp; contemplate the ART Collection</h2>
						</div>
						<div class="about-name" data-name="andy">
							<h1 class="title-name">Andy Goldsworthy</h1>
							<h2>Biography</h2>
							<p>Born in 1956 in Cheshire, Andy Goldsworthy is an extraordinary, innovative British artist whose collaborations with nature produce uniquely personal and intense artworks.</p>
							<br>
							<br>
							<br>
							<h2>The Artist Work</h2>
							<p>By exposing the trials he encounters before a final work is considered complete, Goldsworthy renders the artist a humble element of a greater sum; just as man himself finds his role on earth.</p>
						</div>
						<div class="about-name" data-name="moron">
							<h1 class="title-name">Moron</h1>
							<h2>Biography</h2>
							<p>Born in 1956 in Cheshire, Andy Goldsworthy is an extraordinary, innovative British artist whose collaborations with nature produce uniquely personal and intense artworks.</p>
							<br>
							<br>
							<br>
							<h2>The Artist Work</h2>
							<p>By exposing the trials he encounters before a final work is considered complete, Goldsworthy renders the artist a humble element of a greater sum; just as man himself finds his role on earth.</p>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
	<script src="js/vendor/jquery.royalslider.min.js"></script>
	<script>
		var imageSliderModule = (function () {
			// cache DOM
			var $sliderWrapper 		= $('.slider-wrapper');
			var $imageSlider   		= $sliderWrapper.children('.imageSlider');
			var $thisArrow          = undefined;
			var imageSliderInstance = undefined;

			// attach listeners
			_attachListeners();

			// init
			_init();

			function _attachListeners() {
				$sliderWrapper.on('click', '.arrow', _activateArrows);
			}

			function _init() {
				imageSliderInstance = $imageSlider.royalSlider({
					autoScaleSlider: true,
					imageScaleMode: 'fill',
					loop: true,
					controlNavigation: 'none',
					keyboardNavEnabled: true,
					transitionSpeed: 1000,
					autoplay: {
						enabled: true,
						pauseOnHover: true,
						stopAtAction: false,
						delay: 3000
					}
				}).data('royalSlider');
			}

			function _activateArrows() {
				$thisArrow = $(this);

				if($thisArrow.hasClass('leftArrow')) imageSliderInstance.prev();
				else imageSliderInstance.next();
			}
		})();

		var showNameDetailsModule = (function () {
			// cache DOM
			var $aboutName = $('.about-name');

			// attach Listeners
			_attachListeners();

			// init
			_showInitialText();

			function _attachListeners() {
				$('.names').on('click', 'div', _showNameDetails);
			}
		
			function _showNameDetails() {
				$aboutName.hide(0).filter('[data-name=' + $(this).data('name') + ']').show(0);
			}

			function _showInitialText() {
				$aboutName.first().show(0);
			};
		})(); 
	</script>
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>