<?php include 'include/head-top.php'; ?>
<head>
<script src="js/vendor/flexbox-calc.test.min.js"></script>
<script>
	var flexbox_calc_SupportScript = document.querySelector('script[src="js/vendor/flexbox-calc.test.min.js"]');

	function insertAfter(elementToAdd, referenceNode) {
		referenceNode.parentNode.insertBefore(elementToAdd, referenceNode.nextSibling);
	}

	if(Modernizr.flexbox || Modernizr.flexboxlegacy || Modernizr.flexboxtweener) {
		var swiperCSS  = document.createElement('link');
		swiperCSS.rel  = 'stylesheet';
		swiperCSS.href = 'css/vendor/swiper.min.css';
		insertAfter(swiperCSS, flexbox_calc_SupportScript);
	}
</script>
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/home.css">
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
</head>
<body>
	
	<div class="opacity-layer"></div>

	<?php include 'include/header.php'; ?>
			
	<?php include 'include/nav.php'; ?>	

	<div class="swiper-container">
		<main class="swiper-wrapper">
			<div class="swiper-slide" data-slide-Number="0">
				<div class="fullWidthSlider royalSlider rsMinW">
					<div class="rsContent">
						<a class="rsImg" href="images/_carousel.jpg"></a>
						<div class="text">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING ELIT. EARUM, QUOD.</div>
					</div>
					<div class="rsContent">
						<a class="rsImg" href="images/_beirut.jpg"></a>
						<div class="text">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING ELIT. NUMQUAM, LIBERO.</div>
					</div>
					<div class="rsContent">
						<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
						<div class="text">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING ELIT. IPSAM, CUMQUE.</div>
					</div>
					<div class="rsContent">
						<a class="rsImg" href="images/_beirut.jpg"></a>
						<div class="text">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING ELIT. RECUSANDAE, SOLUTA.</div>
					</div>
					<div class="rsContent">
						<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
						<div class="text">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING ELIT. NIHIL, ASSUMENDA.</div>
					</div>
				</div>
			</div>

			<div class="swiper-slide" data-slide-Number="1">
				<div class="parent">
					<div class="tile-container clear-fix">
						<div class="tile-wrapper tile1"><a href="#" class="tile-layer"><span class="tile-text">ROOMS &amp; SUITES</span></a></div>
						<div class="tile-wrapper tile2"><a href="#" class="tile-layer"><span class="tile-text">DINING</span></a></div>
						<div class="tile-wrapper tile4"><a href="#" class="tile-layer"><span class="tile-text">MEETING &amp; EVENTS</span></a></div>
						<div class="tile-wrapper tile3"><a href="#" class="tile-layer"><span class="tile-text">SPA &amp; WELLNESS</span></a></div>
						<div class="tile-wrapper tile5"><a href="#" class="tile-layer"><span class="tile-text">ART &amp; COLLECTION</span></a></div>
					</div>
				</div>
			</div>
					
			<div class="swiper-slide" data-slide-Number="2">
				<?php include 'include/img-to-cover-carousel.php'; ?>
			</div>
	
			<div class="swiper-slide" data-slide-Number="3">
				<?php include 'include/img-to-cover-carousel.php'; ?>
			</div>

			<div class="swiper-slide" data-slide-Number="4">
				<div class="parent">
					<div id="overview" class="clear-fix">
						<h1>OVERVIEW</h1>
						<div class="google-map-container">
							<div class="google-map"></div>
						</div>
						<div class="col-spacer desktop-cell-only"></div>
						<div class="overview-phoenicia">
							<h2>Its reputation for class and luxurious living echoed around the globe</h2>
							<p>As Beirut’s most sought after hotel, the Phoenicia constantly strives to raise the standards when it comes to fulfilling the sophisticated tastes of today’s more demanding clientele.</p>
						</div>
						<div class="directions">
							<h2>DIRECTIONS</h2>
							<p>Beirut Rafic Hariri International Airport (BEY)</p>
							<p>Travel time 20 minutes</p>
							<p>Distance 10 kilometres (6.3 miles)</p>
						</div>
						<div class="weather-time">
							<h2 class="beirutCity">NOW IN BEIRUT</h2>
							<time class="beirutTime"><span id="dynamicClock">03:15 pm</span> <span class="smaller">+2 GMT</span></time>
							<span class="beirutWeather">
								<img src="images/weather.png" class="cloud">
								<div class="degrees"> 27 °<sup>C</sup></div>
								<span class="smaller">Sunny - Beach time</span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</main>	

		<div class="swiper-scrollbar"></div>
	</div> <!-- Swiper Container -->

	<?php include 'include/footer.php'; ?>
	<script src="js/vendor/jquery.royalslider.min.js"></script>
	<script>
		var fullWidthSliderModule = (function () {
			$('.fullWidthSlider').royalSlider({
	    	  	autoScaleSlider: true,
				imageScaleMode: 'fill',
			    keyboardNavEnabled: true,
			    numImagesToPreload: 1,
				transitionType: 'fade',
				arrowsNav: true,
			    arrowsNavAutoHide: false,
			    controlNavigation: 'none',
			    loop: true,
			    usePreloader: true,
			    autoplay: {
			    	enabled: true,
			    	delay: 4000,
			    	pauseOnHover: false,
			    	stopAtAction: false
			    }
			});
		})();
	</script>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script src="js/modularIndex.js"></script>
</body>
</html>