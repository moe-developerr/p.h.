<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<link rel="stylesheet" href="css/vendor/flickity.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<link rel="stylesheet" href="css/pages-content/spa.css">
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>
	
	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
		
		<main>

			<div class="spa">
				<div class="center-content">
					<div class="page-logo-wrapper">
						<div class="page-logo"></div>
					</div>
				
					<div id="page-img">
						<div class="desktop-only whiteColor">
							<br>
							<br>
							<h1 class="whiteColor">UNWIND AND REJUVENATE</h1>
							<br>
							<br>
							<br>
							<br>
							<br>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, fugit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, asperiores.</p>
							<p>Sit amet, consectetur adipisicing elit. Inventore, autem?Dolor velit distinctio saepe fugiat adipisci accusamus officiis hic quas.</p>
							<br>
							<br>
							<br>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, fugit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, asperiores.</p>
							<p>Sit amet, consectetur adipisicing elit. Inventore, autem?Dolor velit distinctio saepe fugiat adipisci accusamus officiis hic quas.</p>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<p>Opens everyday from 7:00 am to 10:00 pm</p>
							<p>Mezzaine Level, <a href="tel:009611357030" class="boldHoverText whiteColor">+961 (0)1 357030</a></p>
						</div>
					</div>
					
					<div class="inline-block buttons">
						<button>MAKE A RESERVATION</button>
						<button>DOWNLOAD OUR MENU</button>
					</div>

					<?php include 'include/img-to-cover-carousel.php'; ?>
					
					<div class="moreOptions">
						<h1 class="title">SPA TREATMENTS</h1>
						<div class="spa-treatments js-flickity MW1200" data-flickity-options='{"lazyLoad": 2, "cellAlign": "center", "wrapAround": true, "pageDots": false, "arrowShape": {"x0": 10, "x1": 40, "y1": 50, "x2": 45, "y2": 50, "x3": 15} }'>
							<div class="slides">
								<a href="#" class="blockElement">
									<img data-flickity-lazyload="images/_spa-footer-img.jpg" alt="spa footer img">
									<p class="purpleHoverText">FACIAL TREATMENTS</p>
								</a>
							</div>
							<div class="slides">
								<a href="#" class="blockElement">
									<img data-flickity-lazyload="images/_spa-footer-img.jpg" alt="spa footer img">
									<p class="purpleHoverText">FACIAL TREATMENTS</p>
								</a>
							</div>
							<div class="slides">
								<a href="#" class="blockElement">
									<img data-flickity-lazyload="images/_spa-footer-img.jpg" alt="spa footer img">
									<p class="purpleHoverText">FACIAL TREATMENTS</p>
								</a>
							</div>
							<div class="slides">
								<a href="#" class="blockElement">
									<img data-flickity-lazyload="images/_spa-footer-img.jpg" alt="spa footer img">
									<p class="purpleHoverText">FACIAL TREATMENTS</p>
								</a>
							</div>
							<div class="slides">
								<a href="#" class="blockElement">
									<img data-flickity-lazyload="images/_spa-footer-img.jpg" alt="spa footer img">
									<p class="purpleHoverText">FACIAL TREATMENTS</p>
								</a>
							</div>
						</div>
					</div>
				
				</div> <!-- Center Content -->
			</div>

		</main>

	</div>
	
	<?php include 'include/footer.php'; ?>
	<script src="js/vendor/flickity.pkgd.min.js"></script>
	<script src="js/vendor/jquery.royalslider.min.js"></script>
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script>
		var doubleFaceSliderPackagedModule = (function ($window, jswindow) {
			
			var doubleFaceSliderModule = (function () {
				// cache DOM
				var $doubleFaceSliderWrapper 		= $('.doubleFaceSliderWrapper');
				var $doubleFaceSliderDesktopVersion = $doubleFaceSliderWrapper.children('.doubleFaceSlider.desktop-only');
				var $doubleFaceSliderMobileVersion  = $doubleFaceSliderWrapper.children('.doubleFaceSlider.mobile-only');
				var $imageWrapper 					= $('.imgWrapper');	
				var $thisImageWrapper 				= undefined;

				// attach listeners
				_attachListeners();

				function _attachListeners() {
					$doubleFaceSliderWrapper.on('click', '.arrow', _gotoAnotherSlide);
				}

				function setTheDoubleFaceSliderDesktopVersionImagesToCover() {
					if(jswindow.innerWidth >= 1000) {
						$imageWrapper.each(function() {
							$thisImageWrapper = $(this);
							imageToCoverModule.imageToCover($thisImageWrapper, $thisImageWrapper.children('.rsImg'));
						});
					}
				}

				function initDoubleFaceSliderDesktopVersion() {
					$doubleFaceSliderDesktopVersion.royalSlider({
					    arrowsNav: false,
					    arrowsNavAutoHide: false,
					    fadeinLoadedSlide: false,
					    controlNavigationSpacing: 0,
					    controlNavigation: 'none',
					    imageScaleMode: 'none',
					    imageAlignCenter:false,
					    loop: true,
					    loopRewind: false,
					    numImagesToPreload: 10,
					    keyboardNavEnabled: true,
					    usePreloader: true
					});
				}

				function initDoubleFaceSlidersMobileVersion() {
					$doubleFaceSliderMobileVersion.royalSlider({
					   	autoHeight: true,
					    arrowsNav: false,
					    arrowsNavAutoHide: false,
					    fadeinLoadedSlide: false,
					    controlNavigationSpacing: 0,
					    controlNavigation: 'none',
					    imageScaleMode: 'none',
					    imageAlignCenter:false,
					    loop: true,
					    loopRewind: false,
					    numImagesToPreload: 1,
					    keyboardNavEnabled: true,
					    usePreloader: true
					});
				}

				function _gotoAnotherSlide() {
					var $arrowClicked = $(this);

					if($arrowClicked.hasClass('leftArrow')) _gotoPreviousSlide($arrowClicked);
					else _gotoNextSlide($arrowClicked);
				}

				function _gotoNextSlide($arrowClicked) {
					$arrowClicked.nextAll('.doubleFaceSlider').data('royalSlider').next();
				}

				function _gotoPreviousSlide($arrowClicked) {
					$arrowClicked.nextAll('.doubleFaceSlider').data('royalSlider').prev();
				}

				return {
					initDoubleFaceSlidersMobileVersion: initDoubleFaceSlidersMobileVersion,
					initDoubleFaceSliderDesktopVersion: initDoubleFaceSliderDesktopVersion,
					setTheDoubleFaceSliderDesktopVersionImagesToCover: setTheDoubleFaceSliderDesktopVersionImagesToCover,
					$doubleFaceSliderDesktopVersion: $doubleFaceSliderDesktopVersion,
					$doubleFaceSliderMobileVersion: $doubleFaceSliderMobileVersion
				};
			})();

			var imageToCoverModule = (function () {
				function imageToCover(parentOfImage, childImage) {
				    var $parent = parentOfImage,
				        $child  = childImage;
				    if(jswindow.innerWidth >= 1000) {
				        var parentHeight = $parent.innerHeight(),
				            parentWidth  = $parent.innerWidth(),
				            imageHeight  = $child.innerHeight(),
				            imageWidth   = $child.innerWidth();
				            
				            if(imageWidth/imageHeight >= parentWidth/parentHeight) { //landscape mode 
				                $child.css({height: '100%', width: 'auto'});
				            } else { //portrait mode
				                $child.css({width: '100%', height: 'auto'});
				            }
				        var newImageHeight   = $child.innerHeight(),
				            newImageWidth    = $child.innerWidth(),
				            widthDifference  = (newImageWidth  - parentWidth) / 2,
				            heightDifference = (newImageHeight - parentHeight) / 2;

				            $child.css({marginLeft: -widthDifference + 'px', marginTop:  -heightDifference + 'px'});
				    }
				    else {
				        $child.css({marginLeft: 0 + 'px', marginTop:  0 + 'px', width: '100%'});
				    }
				}

				return {
				    	imageToCover: imageToCover
				    };
			})()

			var windowLoadListenersModule = (function () {
				// attach Listeners
				_attachListeners();

				function _attachListeners() {
					$window.on('load', _activateDoubleFaceSliders);
				}

				function _activateDoubleFaceSliders() {
					doubleFaceSliderModule.setTheDoubleFaceSliderDesktopVersionImagesToCover();
					doubleFaceSliderModule.initDoubleFaceSliderDesktopVersion();
					doubleFaceSliderModule.$doubleFaceSliderDesktopVersion.animate({opacity: 1}, 1000);
					doubleFaceSliderModule.initDoubleFaceSlidersMobileVersion();
					doubleFaceSliderModule.$doubleFaceSliderMobileVersion.animate({opacity: 1}, 1000);
				}
			})();

			var windowResizeModule = (function (timer) {
				// attach listeners
				_attachListeners();

				function _attachListeners() {
					$window.on('resize', _resizeListeners);
				}

				function _resizeListeners() {
					_runAfterResize();
				}

				function _runAfterResize() {
					clearTimeout(timer);
					timer = setTimeout(_runAfterResizeListeners, 100);
				}

				function _runAfterResizeListeners() {
					doubleFaceSliderModule.setTheDoubleFaceSliderDesktopVersionImagesToCover();
				}
			})(undefined);
		})($(window), window);
	</script>
</body>
</html>