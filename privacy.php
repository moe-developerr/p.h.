<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/pages-content/privacy.css">
</head>
<body>
	
	<div class="opacity-layer"></div>

	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
	
		<main class="MW1200 clear-fix">
			<h1 id="page-title">PHOENICIA HOTEL PRIVACY POLICY</h1>
			<h2 class="about-privacy">Your Privacy is important to Phoenicia Hotel. This privacy policy provides instructions about the personal information that Phoenicia Hotel collects, and the ways in which Phoenicia Hotel uses that personal information.</h2>
			
			<p class="privacy-paragraph">Phoenicia Hotel is committed to ensuring that your privacy is protected. Should Phoenicia Hotel ask you to provide certain information by which you can be identified when using this website, and then you can be assured that it will only be used in accordance with this privacy policy.</p>
			
			<section class="privacy-section">
				<h3 class="privacy-section-title">Personal Information collection:</h3>
				<p class="privacy-paragraph">Phoenicia Hotel may collect and use any information that can identify you as an individual. It can include among other things, the following:</p>
				<strong class="data-to-be-collected">Name</strong>
				<br>
				<strong class="data-to-be-collected">Date of Birth</strong>
				<br>
				<strong class="data-to-be-collected">Address</strong>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Using Personal Information:</h3>
				<p class="privacy-paragraph">Any of the information Phoenicia Hotel collects from you may be used in one of the following ways:</p>
				<strong class="data-to-be-collected">Reservation confirmation</strong>
				<br>
				<strong class="data-to-be-collected">Newsletter</strong>
				<br>
				<strong class="data-to-be-collected">Invitations</strong>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Personal Information may be collected in one of the following ways:</h3>
				<p class="privacy-paragraph">Phoenicia Hotel may collect and use any information that can identify you as an individual. It can include among other things, the following:</p>
				<strong class="data-to-be-collected">Rooms reservation</strong>
				<br>
				<strong class="data-to-be-collected">Restaurant reservation</strong>
				<br>
				<strong class="data-to-be-collected">Event request form</strong>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Personal Information collection:</h3>
				<strong class="data-to-be-collected">Name</strong>
				<br>
				<strong class="data-to-be-collected">Date of Birth</strong>
				<br>
				<strong class="data-to-be-collected">Address</strong>
				<p class="privacy-paragraph">
					In the event Phoenicia Hotel discloses your personal information to its agents and/or sub-contractors and/or third parties in the course of providing services to Phoenicia Hotel to better serve your needs, whether by using contractual or other arrangements, Phoenicia Hotel ensures these parties will protect your personal information in a manner consistent with the principles articulated in this privacy policy.
					In addition to the disclosures reasonably necessary for the purposes identified elsewhere above, Phoenicia Hotel may disclose your personal information to the extent that it is required to do so by law, in connection with any legal proceedings or prospective legal proceedings, and in order to establish, exercise or defend its legal rights.
					If Phoenicia Hotel suspects any unlawful activity is taking place, it may investigate and/or report its findings or suspicious to the police or other relevant law enforcement agency.
				</p>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Storing Personal Information:</h3>
				<p class="privacy-paragraph">Your Personal Information will be stored in:</p>
				<strong class="data-to-be-collected">Hotel database</strong>
				<br>
				<strong class="data-to-be-collected">Property management software system</strong>
				<br>
				<strong class="data-to-be-collected">Content management software system</strong>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Securing your data:</h3>
				<p class="privacy-paragraph">Phoenicia Hotel will take reasonable technical and organizational precautions to prevent the loss, misuse or alteration of your personal information. Phoenicia Hotel will store all the personal information you provide on its secure servers</p>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Updating this Privacy Policy:</h3>
				<p class="privacy-paragraph">
					Phoenicia Hotel may update this privacy policy by posting a new version on this website.
					You should check this page occasionally to ensure you are familiar with any minor changes.
					If at any time Phoenicia Hotel brings significant changes to this privacy policy, Phoenicia Hotel will provide you by e-mail with a prominent notice sent to your last known address. If you do not present any objection in writing within a time limit of 48 hours from the date of the receipt of the said notice, the above mentioned changes will be considered as accepted.
				</p>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Other websites:</h3>
				<p class="privacy-paragraph">This website contains links to other websites. Phoenicia Hotel is not responsible for the privacy policies or practices of any third party.</p>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Revision of the Personal Information:</h3>
				<p class="privacy-paragraph">If at any time you wish to update or access your Personal Information, you can do so by contacting</p>
				<a class="phoeniciaLink" href="mailto:Phoenicia@phoeniciabeirut.com">Phoenicia@phoeniciabeirut.com</a>
				<p class="privacy-paragraph">Your personal information will be stored indefinitely by Phoenicia Hotel as long as there is a business purpose for doing so.</p>
			</section>

			<section class="privacy-section">
				<h3 class="privacy-section-title">Law and jurisdiction applying to this Policy:</h3>
				<p class="privacy-paragraph">This Privacy policy will be governed and construed in accordance with the Lebanese laws. All disputes arising from the interpretation or the implementation of this policy shall come under the exclusive competence of Beirut Courts.</p>
			</section>
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
	
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
</body>
</html>