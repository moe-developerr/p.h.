<?php include 'include/head-top.php'; ?>
<head>
<link rel="stylesheet" href="css/vendor/scrollbar.min.css">
<?php include 'include/head.php'; ?>
<link rel="stylesheet" href="css/vendor/royalslider.css">
<link rel="stylesheet" href="css/vendor/rs-minimal-white.css">
<link rel="stylesheet" href="css/mysliders.css">
<style>
	.doubleFaceSlider {
		opacity: 0;
	}
	
	.parent:first-of-type {
		padding-top: 0;
	}

	@media screen and (min-width: 1000px) {
		.parent:first-of-type {
			padding-top: 40px;
		}
	}
</style>
</head>
<body>
	
	<div class="opacity-layer"></div>
	
	<?php include 'include/header.php'; ?>

	<?php include 'include/nav.php'; ?>

	<div id="ws-wrapper">		
	
		<main class="MW1200 clear-fix">
			<h1 id="page-title">OFFERS</h1>
			
			<div class="parent">
				<div class="doubleFaceSliderWrapper desktop-only">
					<h1 class="doubleFaceSliderTitle">OUT AND ABOUT</h1>
					<div class="arrow rightArrow"></div>
					<div class="arrow leftArrow"></div>
					<div class="doubleFaceSlider royalSlider desktop-only rsMinW">
						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sitpsum dolor, fa. Labore, facere.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_carousel.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sitpsum dolor, fa. Labore, facere.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_beirut.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sitpsum dolor, fa. Labore, facere.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_out-of-hotel.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sitpsum dolor, fa. Labore, facere.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_beirut.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sitpsum dolor, fa. Labore, facere.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_out-of-hotel.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="doubleFaceSliderWrapper mobile-only">
					<h1 class="doubleFaceSliderTitle">OUT AND ABOUT</h1>
					<div class="arrow rightArrow"></div>
					<div class="arrow leftArrow"></div>
					<div class="doubleFaceSlider royalSlider mobile-only rsMinW">
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_carousel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, facere.</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_beirut.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, sunt?</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, minima.</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_beirut.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, delectus!</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, magni!</p>
						</div>
					</div>
				</div>
			</div>

			<div class="parent">
				<div class="doubleFaceSliderWrapper desktop-only">
					<h1 class="doubleFaceSliderTitle">OUT AND ABOUT</h1>
					<div class="arrow rightArrow"></div>
					<div class="arrow leftArrow"></div>
					<div class="doubleFaceSlider royalSlider desktop-only rsMinW">
						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sitpsum dolor, fa. Labore, facere.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_carousel.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, sunt?</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_beirut.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, minima.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_out-of-hotel.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, delectus!</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_beirut.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, magni!</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_out-of-hotel.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="doubleFaceSliderWrapper mobile-only">
					<h1 class="doubleFaceSliderTitle">OUT AND ABOUT</h1>
					<div class="arrow rightArrow"></div>
					<div class="arrow leftArrow"></div>
					<div class="doubleFaceSlider royalSlider mobile-only rsMinW">
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_carousel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, facere.</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_beirut.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, sunt?</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, minima.</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_beirut.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, delectus!</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, magni!</p>
						</div>
					</div>
				</div>
			</div>

			<div class="parent">
				<div class="doubleFaceSliderWrapper desktop-only">
					<h1 class="doubleFaceSliderTitle">OUT AND ABOUT</h1>
					<div class="arrow rightArrow"></div>
					<div class="arrow leftArrow"></div>
					<div class="doubleFaceSlider royalSlider desktop-only rsMinW">
						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sitpsum dolor, fa. Labore, facere.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_carousel.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, sunt?</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_beirut.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, minima.</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_out-of-hotel.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, delectus!</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_beirut.jpg">
								</div>
							</div>
						</div>

						<div class="rsContent">
							<div class="content-wrapper">
								<h2 class="slide-title">ZEITUNA BAY</h2>
								<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, magni!</p>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE ></a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<div class="imgWrapperContainer">					
								<div class="imgWrapper">
									<img class="rsImg" src="images/_out-of-hotel.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="doubleFaceSliderWrapper mobile-only">
					<h1 class="doubleFaceSliderTitle">OUT AND ABOUT</h1>
					<div class="arrow rightArrow"></div>
					<div class="arrow leftArrow"></div>
					<div class="doubleFaceSlider royalSlider mobile-only rsMinW">
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_carousel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, facere.</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_beirut.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, sunt?</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, minima.</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_beirut.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, delectus!</p>
						</div>
						<div class="rsContent">
							<h2 class="slide-title">ZEITUNA BAY</h2>
							<div class="mobileImgWrapper">
								<a class="rsImg" href="images/_out-of-hotel.jpg"></a>
								<a href="#" class="discoverMoreBtn">DISCOVER MORE</a>
								<a href="#" class="reserveBtn">RESERVE</a>
							</div>
							<p class="slide-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, magni!</p>
						</div>
					</div>
				</div>
			</div>
		</main>
	
	</div>	<!-- Ws Wrapper -->

	<?php include 'include/footer.php'; ?>
		
	<script src="js/vendor/scrollbar.min.js"></script>
	<script src="js/modularBasicScrollbar.js"></script>
	<script src="js/vendor/jquery.royalslider.min.js"></script>

	<script>
		var doubleFaceSliderPackagedModule = (function ($window, jswindow) {
			
			var doubleFaceSliderModule = (function () {
				// cache DOM
				var $doubleFaceSliderWrapper 		= $('.doubleFaceSliderWrapper');
				var $doubleFaceSliderDesktopVersion = $doubleFaceSliderWrapper.children('.doubleFaceSlider.desktop-only');
				var $doubleFaceSliderMobileVersion  = $doubleFaceSliderWrapper.children('.doubleFaceSlider.mobile-only');
				var $imageWrapper 					= $('.imgWrapper');	
				var $thisImageWrapper 				= undefined;

				// attach listeners
				_attachListeners();

				function _attachListeners() {
					$doubleFaceSliderWrapper.on('click', '.arrow', _gotoAnotherSlide);
				}

				function setTheDoubleFaceSliderDesktopVersionImagesToCover() {
					if(jswindow.innerWidth >= 1000) {
						$imageWrapper.each(function() {
							$thisImageWrapper = $(this);
							imageToCoverModule.imageToCover($thisImageWrapper, $thisImageWrapper.children('img'));
						});
					}
				}

				function initDoubleFaceSliderDesktopVersion() {
					$doubleFaceSliderDesktopVersion.royalSlider({
					    arrowsNav: false,
					    arrowsNavAutoHide: false,
					    fadeinLoadedSlide: false,
					    controlNavigationSpacing: 0,
					    controlNavigation: 'none',
					    imageScaleMode: 'none',
					    imageAlignCenter:false,
					    loop: true,
					    loopRewind: false,
					    numImagesToPreload: 10,
					    keyboardNavEnabled: true,
					    usePreloader: true
					});
				}

				function initDoubleFaceSlidersMobileVersion() {
					$doubleFaceSliderMobileVersion.royalSlider({
					   	autoHeight: true,
					    arrowsNav: false,
					    arrowsNavAutoHide: false,
					    fadeinLoadedSlide: false,
					    controlNavigationSpacing: 0,
					    controlNavigation: 'none',
					    imageScaleMode: 'none',
					    imageAlignCenter:false,
					    loop: true,
					    loopRewind: false,
					    numImagesToPreload: 1,
					    keyboardNavEnabled: true,
					    usePreloader: true
					});
				}

				function _gotoAnotherSlide() {
					var $arrowClicked = $(this);

					if($arrowClicked.hasClass('leftArrow')) _gotoPreviousSlide($arrowClicked);
					else _gotoNextSlide($arrowClicked);
				}

				function _gotoNextSlide($arrowClicked) {
					$arrowClicked.nextAll('.doubleFaceSlider').data('royalSlider').next();
				}

				function _gotoPreviousSlide($arrowClicked) {
					$arrowClicked.nextAll('.doubleFaceSlider').data('royalSlider').prev();
				}

				return {
					initDoubleFaceSlidersMobileVersion: initDoubleFaceSlidersMobileVersion,
					initDoubleFaceSliderDesktopVersion: initDoubleFaceSliderDesktopVersion,
					setTheDoubleFaceSliderDesktopVersionImagesToCover: setTheDoubleFaceSliderDesktopVersionImagesToCover,
					$doubleFaceSliderDesktopVersion: $doubleFaceSliderDesktopVersion,
					$doubleFaceSliderMobileVersion: $doubleFaceSliderMobileVersion
				};
			})();

			var imageToCoverModule = (function () {
				function imageToCover(parentOfImage, childImage) {
				    var $parent = parentOfImage,
				        $child  = childImage;
				    if(jswindow.innerWidth >= 1000) {
				        var parentHeight = $parent.innerHeight(),
				            parentWidth  = $parent.innerWidth(),
				            imageHeight  = $child.innerHeight(),
				            imageWidth   = $child.innerWidth();
				            
				            if(imageWidth/imageHeight >= parentWidth/parentHeight) { //landscape mode 
				                $child.css({height: '100%', width: 'auto'});
				            } else { //portrait mode
				                $child.css({width: '100%', height: 'auto'});
				            }
				        var newImageHeight   = $child.innerHeight(),
				            newImageWidth    = $child.innerWidth(),
				            widthDifference  = (newImageWidth  - parentWidth) / 2,
				            heightDifference = (newImageHeight - parentHeight) / 2;

				            $child.css({marginLeft: -widthDifference + 'px', marginTop:  -heightDifference + 'px'});
				    }
				    else {
				        $child.css({marginLeft: 0 + 'px', marginTop:  0 + 'px', width: '100%'});
				    }
				}

				return {
				    	imageToCover: imageToCover
				    };
			})()

			var windowLoadListenersModule = (function () {
				// attach Listeners
				_attachListeners();

				function _attachListeners() {
					$window.on('load', _activateDoubleFaceSliders);
				}

				function _activateDoubleFaceSliders() {
					doubleFaceSliderModule.setTheDoubleFaceSliderDesktopVersionImagesToCover();
					doubleFaceSliderModule.initDoubleFaceSliderDesktopVersion();
					doubleFaceSliderModule.initDoubleFaceSlidersMobileVersion();
					doubleFaceSliderModule.$doubleFaceSliderDesktopVersion.animate({opacity: 1}, 1000);
					doubleFaceSliderModule.$doubleFaceSliderMobileVersion.animate({opacity: 1}, 1000);
				}
			})();

			var windowResizeModule = (function (timer) {
				// attach listeners
				_attachListeners();

				function _attachListeners() {
					$window.on('resize', _resizeListeners);
				}

				function _resizeListeners() {
					_runAfterResize();
				}

				function _runAfterResize() {
					clearTimeout(timer);
					timer = setTimeout(_runAfterResizeListeners, 100);
				}

				function _runAfterResizeListeners() {
					doubleFaceSliderModule.setTheDoubleFaceSliderDesktopVersionImagesToCover();
				}
			})(undefined);
		})($(window), window);
	</script>
</body>
</html>